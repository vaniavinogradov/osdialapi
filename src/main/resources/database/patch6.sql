ALTER TABLE osdial_campaign_agents MODIFY COLUMN campaign_id BIGINT(14) NOT NULL;
ALTER TABLE osdial_campaign_agents ADD FOREIGN KEY (campaign_id) REFERENCES osdial_campaigns(campaign_id) ON DELETE CASCADE ;

DELETE FROM osdial_live_agents;
ALTER TABLE osdial_live_agents MODIFY COLUMN campaign_id BIGINT(14) NOT NULL;
ALTER TABLE osdial_live_agents ADD FOREIGN KEY (campaign_id) REFERENCES osdial_campaigns(campaign_id) ON DELETE CASCADE ;

ALTER TABLE osdial_campaign_statuses MODIFY COLUMN campaign_id BIGINT(14) NOT NULL;
ALTER TABLE osdial_campaign_statuses ADD FOREIGN KEY (campaign_id) REFERENCES osdial_campaigns(campaign_id) ON DELETE CASCADE ;

ALTER TABLE osdial_campaign_hotkeys MODIFY COLUMN campaign_id BIGINT(14) NOT NULL;
ALTER TABLE osdial_campaign_hotkeys ADD FOREIGN KEY (campaign_id) REFERENCES osdial_campaigns(campaign_id) ON DELETE CASCADE ;

ALTER TABLE osdial_callbacks MODIFY COLUMN campaign_id BIGINT(14) NOT NULL;
ALTER TABLE osdial_callbacks ADD FOREIGN KEY (campaign_id) REFERENCES osdial_campaigns(campaign_id) ON DELETE CASCADE ;

DELETE FROM osdial_campaign_stats;
ALTER TABLE osdial_campaign_stats MODIFY COLUMN campaign_id BIGINT(14) NOT NULL;
ALTER TABLE osdial_campaign_stats ADD FOREIGN KEY (campaign_id) REFERENCES osdial_campaigns(campaign_id) ON DELETE CASCADE ;

ALTER TABLE osdial_lead_recycle MODIFY COLUMN campaign_id BIGINT(14) NOT NULL;
ALTER TABLE osdial_lead_recycle ADD FOREIGN KEY (campaign_id) REFERENCES osdial_campaigns(campaign_id) ON DELETE CASCADE ;

ALTER TABLE osdial_campaign_server_stats MODIFY COLUMN campaign_id BIGINT(14) NOT NULL;
ALTER TABLE osdial_campaign_server_stats ADD FOREIGN KEY (campaign_id) REFERENCES osdial_campaigns(campaign_id) ON DELETE CASCADE ;

ALTER TABLE osdial_server_trunks MODIFY COLUMN campaign_id BIGINT(14) NOT NULL;
ALTER TABLE osdial_server_trunks ADD FOREIGN KEY (campaign_id) REFERENCES osdial_campaigns(campaign_id) ON DELETE CASCADE ;

ALTER TABLE osdial_pause_codes MODIFY COLUMN campaign_id BIGINT(14) NOT NULL;
ALTER TABLE osdial_pause_codes ADD FOREIGN KEY (campaign_id) REFERENCES osdial_campaigns(campaign_id) ON DELETE CASCADE ;

ALTER TABLE osdial_campaigns_list_mix MODIFY COLUMN campaign_id BIGINT(14) NOT NULL;
ALTER TABLE osdial_campaigns_list_mix ADD FOREIGN KEY (campaign_id) REFERENCES osdial_campaigns(campaign_id) ON DELETE CASCADE ;

ALTER TABLE osdial_agent_log MODIFY COLUMN campaign_id BIGINT(14) NOT NULL;
ALTER TABLE osdial_agent_log ADD FOREIGN KEY (campaign_id) REFERENCES osdial_campaigns(campaign_id) ON DELETE CASCADE ;



