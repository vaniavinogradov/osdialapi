/*ALTER TABLE osdial_users MODIFY COLUMN user_id INT(9) NOT NULL ;*/

CREATE TABLE access_token (
  access_token_id INT NOT NULL AUTO_INCREMENT,
  token VARCHAR(1024),
  expire TIMESTAMP,
  user_id INT(9),
  CONSTRAINT access_token_pk PRIMARY KEY (access_token_id)
 /* FOREIGN KEY (user_id) REFERENCES osdial_users(user_id) ON DELETE CASCADE*/
);
