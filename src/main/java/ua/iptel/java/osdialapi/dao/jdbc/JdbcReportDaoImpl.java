package ua.iptel.java.osdialapi.dao.jdbc;

import com.sun.rowset.internal.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ua.iptel.java.osdialapi.dao.osdial.ReportDao;
import ua.iptel.java.osdialapi.model.CallLog;
import ua.iptel.java.osdialapi.model.Recording;
import ua.iptel.java.osdialapi.model.dto.*;
import ua.iptel.java.osdialapi.model.report.agent.AgentCallsDetailedReport;
import ua.iptel.java.osdialapi.model.report.agent.AgentCallsStatistic;
import ua.iptel.java.osdialapi.model.report.agent.AgentStatusesDetailedReport;
import ua.iptel.java.osdialapi.model.report.pause.AgentPausesReport;
import ua.iptel.java.osdialapi.model.report.pause.CampaignPausesReport;
import ua.iptel.java.osdialapi.model.report.pause.DetailedPausesReport;
import ua.iptel.java.osdialapi.model.report.pause.GeneralPausesReport;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class JdbcReportDaoImpl implements ReportDao {
    @Autowired
    @Qualifier("jdbcTemplateOsdialDb")
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<GeneralPausesReport> getGeneralPausesReport(LocalDateTime from, LocalDateTime to, String agent, String campaigns) {
        StringBuilder sql = new StringBuilder("select sub_status AS pause_code, count(*) AS count, SUM(pause_sec) as total_seconds FROM osdial_agent_log");

        sql.append(" WHERE event_time BETWEEN '" + Timestamp.valueOf(from) + "' AND '" + Timestamp.valueOf(to) + "'");
        if(!agent.isEmpty()) {
            sql.append(" AND user = '" + agent + "'");
        }
        if(!campaigns.isEmpty() && !campaigns.equals("-ALL-")){
            sql.append(campaignCondition(campaigns));
        }

        return jdbcTemplate.query(sql.toString(), new GeneralPausesReportRowMapper());

    }

    @Override
    public List<CampaignPausesReport> getCampaignPausesReport(LocalDateTime from, LocalDateTime to, String campaigns) {
        StringBuilder sql = new StringBuilder("select osdial_campaigns.campaign_id, osdial_campaigns.campaign_name," +
                " sub_status AS pause_code,count(*) AS count, SUM(pause_sec) AS total_seconds" +
                " FROM osdial_agent_log " +
                "LEFT JOIN osdial_campaigns ON osdial_campaigns.campaign_id = osdial_agent_log.campaign_id " +
                "WHERE pause_sec > 0 ");

        sql.append("AND event_time BETWEEN '" + Timestamp.valueOf(from) + "' AND '" + Timestamp.valueOf(to) + "'");
        if(!campaigns.isEmpty() && !campaigns.equals("-ALL-")){
            sql.append(campaignCondition(campaigns));
        }

        sql.append(" GROUP BY campaign_id, pause_code");
        return jdbcTemplate.query(sql.toString(), new CampaignPausesReportRowMapper());
    }

    @Override
    public List<AgentPausesReport> getAgentPausesReport(LocalDateTime from, LocalDateTime to, String agent, String campaigns) {
        StringBuilder sql = new StringBuilder("select osdial_campaigns.campaign_id, osdial_campaigns.campaign_name, user," +
                " sub_status AS pause_code,count(*) AS count, SUM(pause_sec) AS total_seconds" +
                " FROM osdial_agent_log " +
                "LEFT JOIN osdial_campaigns ON osdial_campaigns.campaign_id = osdial_agent_log.campaign_id " +
                "WHERE pause_sec > 0 ");

        sql.append("AND event_time BETWEEN '" + Timestamp.valueOf(from) + "' AND '" + Timestamp.valueOf(to) + "'");
        if(!agent.isEmpty()) {
            sql.append(" AND user = '" + agent + "'");
        }
        if(!campaigns.isEmpty() && !campaigns.equals("-ALL-")){
            sql.append(campaignCondition(campaigns));
        }

        sql.append(" GROUP BY campaign_id, user, pause_code");
        return jdbcTemplate.query(sql.toString(), new AgentPausesReportRowMapper());
    }

    @Override
    public List<DetailedPausesReport> getDetailedPausesReport(LocalDateTime from, LocalDateTime to, String agent, String campaigns) {
        StringBuilder sql = new StringBuilder("select osdial_campaigns.campaign_id, osdial_campaigns.campaign_name,user,sub_status AS pause_code,event_time AS pause_start," +
                "  DATE_ADD(event_time,INTERVAL pause_sec SECOND) AS pause_end,pause_sec FROM osdial_agent_log" +
                "  LEFT JOIN osdial_campaigns ON osdial_campaigns.campaign_id = osdial_agent_log.campaign_id ");

        sql.append("WHERE event_time BETWEEN '" + Timestamp.valueOf(from) + "' AND '" + Timestamp.valueOf(to) + "'");
        if(!agent.isEmpty()) {
            sql.append(" AND user = '" + agent + "'");
        }
        if(!campaigns.isEmpty() && !campaigns.equals("-ALL-")){
            sql.append(campaignCondition(campaigns));
        }

        sql.append(" GROUP BY campaign_id, user, pause_code");
        return jdbcTemplate.query(sql.toString(), new DetailedReportRowMapper());
    }

    @Override
    public List<AgentCallsDetailedReport> getAgentCallsDetailedReport(LocalDateTime from, LocalDateTime to, String agent, String campaign, String userGroup) {
        StringBuilder sql = new StringBuilder("select full_name,  osdial_users.user, count(*) as count_calls, sum(talk_sec) as sum_talk_sec," +
                " sum(pause_sec) as sum_pause_sec, sum(wait_sec) as sum_wait_sec, sum(dispo_sec) as sum_dispo_sec," +
                " sum( if(lead_called_count='1', 1, 0) ) as sum_lead_called_count" +
                " FROM osdial_users, osdial_agent_log, osdial_user_groups");

        sql.append(" WHERE osdial_users.user = osdial_agent_log.user and event_time BETWEEN '" + Timestamp.valueOf(from) + "' AND '" + Timestamp.valueOf(to) + "'");
        if(!agent.isEmpty()) {
            sql.append(" AND osdial_agent_log.user = '" + agent + "'");
        }
        if(!userGroup.isEmpty() && !userGroup.equals("-ALL-")){
            sql.append(" AND osdial_user_groups.id = '" + userGroup + "'" );
        }
        if(!campaign.isEmpty() && !campaign.equals("-ALL-")){
            sql.append(" AND osdial_agent_log.campaign_id = '" + campaign + "'" );
        }
        sql.append("and osdial_agent_log.lead_id != 0 and pause_sec < 36000 and wait_sec < 36000 and talk_sec < 36000 and dispo_sec < 36000 GROUP BY full_name LIMIT 100000;");
        return jdbcTemplate.query(sql.toString(), new AgentCallsDetailedReportRowMapper());
    }

    @Override
    public List<AgentStatusesDetailedReport> getAgentStatusesDetailedReport(LocalDateTime from, LocalDateTime to, String agent, String campaign, String userGroup) {
        StringBuilder sql = new StringBuilder("select full_name,  osdial_users.user, sum( if(lead_called_count>='1', 1, 0) ) as sum_lead_called_count," +
                " status FROM osdial_users, osdial_agent_log, osdial_user_groups ");
        sql.append(" WHERE osdial_users.user = osdial_agent_log.user and event_time BETWEEN '" + Timestamp.valueOf(from) + "' AND '" + Timestamp.valueOf(to) + "'");
        if(!agent.isEmpty() && !agent.equals("-ALL-")) {
            sql.append(" AND osdial_agent_log.user = '" + agent + "'");
        }
        if(!userGroup.isEmpty() && !userGroup.equals("-ALL-")){
            sql.append(" AND osdial_user_groups.id = '" + userGroup + "'" );
        }
        if(!campaign.isEmpty() && !campaign.equals("-ALL-")){
            sql.append(" AND osdial_agent_log.campaign_id = '" + campaign + "'" );
        }
        sql.append(" AND wait_sec < 36000 and talk_sec < 36000 and dispo_sec < 36000 GROUP BY full_name, status LIMIT 100000;");
        return jdbcTemplate.query(sql.toString(), new AgentStatusesDetailedReportRowMapper());
    }

    @Override
    public List<AgentCallsStatistic> getAgentCallsStatistic(LocalDateTime from, LocalDateTime to, String agent, String type) {
        StringBuilder sql = new StringBuilder("SELECT DISTINCT" +
                " osdial_agent_log.event_time, osdial_agent_log.wait_sec, osdial_agent_log.dispo_sec, osdial_agent_log.pause_sec," +
                " osdial_agent_log.status, osdial_list.phone_number, osdial_agent_log.user_group, osdial_agent_log.campaign_id," +
                " osdial_campaigns.campaign_name, osdial_list.list_id, osdial_agent_log.lead_id " +
                "FROM" +
                " osdial_agent_log JOIN osdial_log ON osdial_agent_log.uniqueid = osdial_log.uniqueid" +
                " JOIN osdial_list ON osdial_agent_log.lead_id = osdial_list.lead_id" +
                " LEFT JOIN osdial_campaigns ON osdial_campaigns.campaign_id = osdial_agent_log.campaign_id ");

        sql.append("WHERE event_time BETWEEN '" + Timestamp.valueOf(from) +"' AND '" + Timestamp.valueOf(to) +"'");
        sql.append(" AND osdial_agent_log.user = '" + agent + "'");
        sql.append(" ORDER BY osdial_agent_log.event_time DESC");
        return jdbcTemplate.query(sql.toString(), new AgentCallsStatisticRowMapper());
    }

    @Override
    public List<Recording> getAllRecordings() {
        String sql = "select * from recording_log";
        return jdbcTemplate.query(sql.toString(), new RecordingRowMapper());
    }

    @Override
    public List<Recording> getRecordingByLeadId(Long leadId) {
        String sql = "select * from recording_log where lead_id = " + leadId;
        return jdbcTemplate.query("SELECT * FROM recording_log WHERE lead_id = ?", new RecordingRowMapper(), leadId);
    }

    @Override
    public List<Recording> getRecordingsByPeriod(LocalDateTime from, LocalDateTime to) {
        StringBuilder sql = new StringBuilder("select * from recording_log ");
        sql.append("WHERE end_time BETWEEN '" + Timestamp.valueOf(from) +"' AND '" + Timestamp.valueOf(to) +"'");
        return jdbcTemplate.query(sql.toString(), new RecordingRowMapper());
    }

    @Override
    public List<Recording> getRecordingsByPeriodAndUser(LocalDateTime from, LocalDateTime to, String user) {
        StringBuilder sql = new StringBuilder("select * from recording_log ");
        sql.append("WHERE end_time BETWEEN '" + Timestamp.valueOf(from) +"' AND '" + Timestamp.valueOf(to) +"'");
        sql.append(" AND user = '" + user + "'" );
        return jdbcTemplate.query(sql.toString(), new RecordingRowMapper());
    }

    @Override
    public List<CallLog> getCallsLog() {
        StringBuilder sql = new StringBuilder("select * from osdial_log");
        //sql.append("WHERE end_time BETWEEN '" + Timestamp.valueOf(from) +"' AND '" + Timestamp.valueOf(to) +"'");
        return jdbcTemplate.query(sql.toString(), new CallLogRowMapper());
    }

    @Override
    public List<CallLogExtended> getCallsLogWitchLeadName() {
        String sql = "select osdial_log.id as id," +
                " osdial_log.uniqueid as uniqueid," +
                " osdial_log.lead_id as lead_id," +
                " CONCAT(osdial_list.first_name," +
                " ' ', osdial_list.last_name ) as name," +
                " osdial_log.list_id as list_id," +
                " osdial_lists.list_name as list_name," +
                " osdial_log.campaign_id as campaign_id," +
                " osdial_log.call_date as call_date," +
                " osdial_log.start_epoch as start_epoch," +
                " osdial_log.end_epoch as end_epoch," +
                " osdial_log.length_in_sec as length_in_sec," +
                " osdial_log.status as status," +
                " osdial_log.phone_code as phone_code," +
                " osdial_log.phone_number as phone_number," +
                " osdial_log.user as user," +
                " osdial_log.comments as comments," +
                " osdial_log.processed as processed," +
                " osdial_log.user_group as user_group," +
                " osdial_log.term_reason as term_reason," +
                " osdial_log.server_ip as server_ip," +
                " osdial_log.callerid as callerid from osdial_log" +
                " LEFT JOIN osdial_list ON osdial_log.lead_id=osdial_list.lead_id " +
                "LEFT JOIN osdial_lists ON osdial_log.list_id=osdial_lists.list_id;";
        return jdbcTemplate.query(sql, new CallLogRowWithNameMapper());
    }

    @Override
    public List<CallLog> getCallsLogByLeadId(Long leadId) {
        StringBuilder sql = new StringBuilder("select * from osdial_log WHERE lead_id='" + leadId + "'");
        //sql.append("WHERE end_time BETWEEN '" + Timestamp.valueOf(from) +"' AND '" + Timestamp.valueOf(to) +"'");
        return jdbcTemplate.query(sql.toString(), new CallLogRowMapper());
    }

    @Override
    public List<CallLogExtended> getCallsLogByLeadIdWithLeadName(Long listId) {
        String sql = "select osdial_log.id as id," +
                " osdial_log.uniqueid as uniqueid," +
                " osdial_log.lead_id as lead_id," +
                " CONCAT(osdial_list.first_name, ' ', osdial_list.last_name ) as name," +
                " osdial_log.list_id as list_id," +
                " osdial_lists.list_name as list_name, " +
                "osdial_log.campaign_id as campaign_id," +
                " osdial_log.call_date as call_date," +
                " osdial_log.start_epoch as start_epoch," +
                " osdial_log.end_epoch as end_epoch," +
                " osdial_log.length_in_sec as length_in_sec," +
                " osdial_log.status as status," +
                " osdial_log.phone_code as phone_code," +
                " osdial_log.phone_number as phone_number," +
                " osdial_log.user as user," +
                " osdial_log.comments as comments," +
                " osdial_log.processed as processed," +
                " osdial_log.user_group as user_group," +
                " osdial_log.term_reason as term_reason, " +
                "osdial_log.server_ip as server_ip," +
                " osdial_log.callerid as callerid from osdial_log " +
                " LEFT JOIN osdial_list ON osdial_log.lead_id=osdial_list.lead_id " +
                " LEFT JOIN osdial_lists ON osdial_log.list_id=osdial_lists.list_id " +
                " where osdial_log.list_id=?";
        return jdbcTemplate.query(sql.toString(), new CallLogRowWithNameMapper(), listId);
    }

    @Override
    public List<CallLog> getCallLogByCampaignAndDateTimeRange(LocalDateTime from, LocalDateTime to, Integer campaignId) {
        StringBuilder sql = new StringBuilder("select * from osdial_log WHERE ");
        sql.append("call_date BETWEEN '" + Timestamp.valueOf(from) +"' AND '" + Timestamp.valueOf(to) +"' ");
        if(campaignId != 0) {
            sql.append("AND campaign_id=" + campaignId);
        }
        sql.append(" ORDER BY call_date DESC");

        return jdbcTemplate.query(sql.toString(), new CallLogRowMapper());
    }

    @Override
    public List<CallLog> getCallLogByListId(Integer listId) {
        String sql = "select * from osdial_log WHERE list_id = ?";
        return jdbcTemplate.query(sql, new CallLogRowMapper(), listId);
    }

    private class CallLogRowMapper implements RowMapper {
        @Override
        public Object mapRow(ResultSet rs, int i) throws SQLException {
            CallLog callLog = new CallLog();
            callLog.setId(rs.getLong("id"));
            callLog.setUniqueId(rs.getString("uniqueid"));
            callLog.setLeadId(rs.getLong("lead_id"));
            callLog.setListId(rs.getLong("list_id"));
            callLog.setCampaingId(rs.getLong("campaign_id"));
            callLog.setCallDateTime(rs.getTimestamp("call_date").toLocalDateTime());
            callLog.setLength(rs.getInt("length_in_sec"));
            callLog.setStatus(rs.getString("status"));
            callLog.setPhoneCode(rs.getString("phone_code"));
            callLog.setPhoneNumber(rs.getString("phone_number"));
            callLog.setUser(rs.getString("user"));
            callLog.setComments(rs.getString("comments"));
            callLog.setUserGroup(rs.getString("user_group"));
            callLog.setTermReason(rs.getString("term_reason"));
            callLog.setCallerId(rs.getString("callerid"));
            return callLog;
        }
    }

    private class CallLogRowWithNameMapper implements RowMapper {
        @Override
        public Object mapRow(ResultSet rs, int i) throws SQLException {
            CallLogExtended callLog = new CallLogExtended();
            callLog.setId(rs.getLong("id"));
            callLog.setUniqueId(rs.getString("uniqueid"));
            callLog.setLeadId(rs.getLong("lead_id"));
            callLog.setListId(rs.getLong("list_id"));
            callLog.setCampaingId(rs.getLong("campaign_id"));
            callLog.setCallDateTime(rs.getTimestamp("call_date").toLocalDateTime());
            callLog.setLength(rs.getInt("length_in_sec"));
            callLog.setStatus(rs.getString("status"));
            callLog.setPhoneCode(rs.getString("phone_code"));
            callLog.setPhoneNumber(rs.getString("phone_number"));
            callLog.setUser(rs.getString("user"));
            callLog.setComments(rs.getString("comments"));
            callLog.setUserGroup(rs.getString("user_group"));
            callLog.setTermReason(rs.getString("term_reason"));
            callLog.setCallerId(rs.getString("callerid"));
            String leadName = rs.getString("name");
            if(leadName != null){
                callLog.setLeadName(leadName);
            }else {
                callLog.setLeadName("");
            }
            String listName = rs.getString("list_name");
            if(listName != null){
                callLog.setListName(listName);
            }else {
                callLog.setListName("");
            }

            return callLog;
        }
    }

    private class GeneralPausesReportRowMapper implements RowMapper {
        @Override
        public Object mapRow(ResultSet rs, int i) throws SQLException {
            GeneralPausesReport generalPausesReport = new GeneralPausesReport();
            generalPausesReport.setCodePause(rs.getString("pause_code"));
            generalPausesReport.setCount(rs.getInt("count"));
            generalPausesReport.setTotalSeconds(rs.getInt("total_seconds"));
            if(generalPausesReport.getCount() == 0){
                generalPausesReport.setTotalTimeAverage(0);
            }else {
                generalPausesReport.setTotalTimeAverage(generalPausesReport.getTotalSeconds()/generalPausesReport.getCount());
            }

            return generalPausesReport;
        }
    }

    private class CampaignPausesReportRowMapper implements RowMapper {
        @Override
        public Object mapRow(ResultSet rs, int i) throws SQLException {
            CampaignPausesReport campaignPausesReport = new CampaignPausesReport();
            CampaignDto campaign = new CampaignDto();
            campaign.setId(rs.getLong("campaign_id"));
            campaign.setName(rs.getString("campaign_name"));
            campaignPausesReport.setCampaign(campaign);

            campaignPausesReport.setPauseCode(rs.getString("pause_code"));
            campaignPausesReport.setCount(rs.getInt("count"));
            campaignPausesReport.setTotalTime(rs.getInt("total_seconds"));
            if(campaignPausesReport.getCount() == 0){
                campaignPausesReport.setTotalTimeAverage(0);
            }else {
                campaignPausesReport.setTotalTimeAverage(campaignPausesReport.getTotalTime()/campaignPausesReport.getCount());
            }
            return campaignPausesReport;
        }
    }

    private class AgentPausesReportRowMapper implements RowMapper {
        @Override
        public Object mapRow(ResultSet rs, int i) throws SQLException {
            AgentPausesReport agentPausesReport = new AgentPausesReport();
            CampaignDto campaign = new CampaignDto();
            campaign.setId(rs.getLong("campaign_id"));
            campaign.setName(rs.getString("campaign_name"));
            agentPausesReport.setCampaign(campaign);

            UserDto userDto = new UserDto();
            userDto.setUsername(rs.getString("user"));

            agentPausesReport.setPauseCode(rs.getString("pause_code"));
            agentPausesReport.setCount(rs.getInt("count"));
            agentPausesReport.setTotalTime(rs.getInt("total_seconds"));
            if(agentPausesReport.getCount() == 0){
                agentPausesReport.setTotalTimeAverage(0);
            }else {
                agentPausesReport.setTotalTimeAverage(agentPausesReport.getTotalTime()/agentPausesReport.getCount());
            }
            return agentPausesReport;
        }
    }

    private class DetailedReportRowMapper implements RowMapper {
        @Override
        public Object mapRow(ResultSet rs, int i) throws SQLException {
            DetailedPausesReport detailedPausesReport = new DetailedPausesReport();
            CampaignDto campaign = new CampaignDto();
            campaign.setId(rs.getLong("campaign_id"));
            campaign.setName(rs.getString("campaign_name"));
            detailedPausesReport.setCampaign(campaign);

            UserDto userDto = new UserDto();
            userDto.setUsername(rs.getString("user"));

            detailedPausesReport.setPauseCode(rs.getString("pause_code"));

            detailedPausesReport.setStart(rs.getTimestamp("pause_start").toLocalDateTime());
            detailedPausesReport.setFinish(rs.getTimestamp("pause_end").toLocalDateTime());
            detailedPausesReport.setTotalTime(rs.getInt("pause_sec"));

            return detailedPausesReport;
        }
    }

    private class AgentCallsDetailedReportRowMapper implements RowMapper {
        @Override
        public Object mapRow(ResultSet rs, int i) throws SQLException {
            AgentCallsDetailedReport agentCallsDetailedReport = new AgentCallsDetailedReport();

           UserDto userDto = new UserDto();
           userDto.setFullName(rs.getString("full_name"));
           userDto.setUsername(rs.getString("user"));

           agentCallsDetailedReport.setUser(userDto);
           int callsCount = rs.getInt("count_calls");
           agentCallsDetailedReport.setCallsCount(callsCount);
           int totalTalkTime = rs.getInt("sum_talk_sec");
           agentCallsDetailedReport.setTotalTime(totalTalkTime);
           int pauseTotalTime = rs.getInt("sum_pause_sec");
           agentCallsDetailedReport.setPauseTotalTime(pauseTotalTime);
           int waitTotalTime = rs.getInt("sum_wait_sec");
           agentCallsDetailedReport.setWaitTotalTime(waitTotalTime);
           int dispoTotalTime = rs.getInt("sum_dispo_sec");
           agentCallsDetailedReport.setDispoTime(dispoTotalTime);

           agentCallsDetailedReport.setCallDurationAverageTime(totalTalkTime/callsCount);
           agentCallsDetailedReport.setPauseAverageTime(pauseTotalTime/callsCount);
           agentCallsDetailedReport.setAverageWaitTime(waitTotalTime/callsCount);
           agentCallsDetailedReport.setAverageDispoTime(dispoTotalTime/callsCount);

            return agentCallsDetailedReport;
        }
    }

    private class AgentCallsStatisticRowMapper implements RowMapper {
        @Override
        public Object mapRow(ResultSet rs, int i) throws SQLException {
           AgentCallsStatistic agentCallsStatistic = new AgentCallsStatistic();
            agentCallsStatistic.setCallDateTime(rs.getTimestamp("event_time").toLocalDateTime());
            agentCallsStatistic.setWaitTime(rs.getInt("wait_sec"));
            agentCallsStatistic.setCallDurationTime(rs.getInt("dispo_sec"));
            agentCallsStatistic.setDispoTime(rs.getInt("dispo_sec"));
            agentCallsStatistic.setPauseTime(rs.getInt("pause_sec"));
            agentCallsStatistic.setStatus(rs.getString("status"));
            agentCallsStatistic.setPhoneNumber(rs.getString("phone_number"));

            CampaignDto campaignDto = new CampaignDto();
            campaignDto.setId(rs.getLong("campaign_id"));
            campaignDto.setName(rs.getString("campaign_name"));
            agentCallsStatistic.setCampaign(campaignDto);

            ListDto listDto = new ListDto();
            listDto.setId(rs.getLong("list_id"));
            agentCallsStatistic.setList(listDto);

            LeadDto leadDto = new LeadDto();
            leadDto.setName(rs.getString("lead_id"));
            agentCallsStatistic.setLead(leadDto);
           return agentCallsStatistic;
        }
    }

    private class AgentStatusesDetailedReportRowMapper implements RowMapper {
        @Override
        public Object mapRow(ResultSet rs, int i) throws SQLException {
           AgentStatusesDetailedReport agentStatusesDetailedReport = new AgentStatusesDetailedReport();

           UserDto userDto = new UserDto();
           userDto.setFullName(rs.getString("full_name"));
           userDto.setUsername(rs.getString("user"));
//
           agentStatusesDetailedReport.setUser(userDto);
            Map<String, Integer> statuses = new HashMap<>();
            String status = rs.getString("status");
            // Osdial issue with null status. Need to remove all records from agent_log table with list.
            if(status != null){
                statuses.put(status, rs.getInt("sum_lead_called_count"));
            }

            agentStatusesDetailedReport.setStatuses(statuses);
           return agentStatusesDetailedReport;
        }
    }

    private class RecordingRowMapper implements RowMapper {
        @Override
        public Object mapRow(ResultSet rs, int i) throws SQLException {
            Recording recording = new Recording();
            recording.setId(rs.getLong("recording_id"));
            recording.setLeadId(rs.getLong("lead_id"));
            recording.setStartTime(rs.getTimestamp("start_time").toLocalDateTime());
            Timestamp endTime = rs.getTimestamp("start_time");
            if(endTime != null){
                recording.setEndTime(endTime.toLocalDateTime());
            }

            recording.setLength(rs.getInt("length_in_sec"));
            recording.setFileName(rs.getString("filename"));
            recording.setLocation(rs.getString("location"));
            return recording;
        }
    }

    private String campaignCondition(String campaigns){
        StringBuilder sql = new StringBuilder();
        String[] splittedArray = campaigns.split(",");
        sql.append(" AND osdial_agent_log.campaign_id IN (");
        for (int i = 0; i < splittedArray.length; i++) {
            if(i == (splittedArray.length - 1)){
                sql.append("'" + splittedArray[i] + "'");
            }else{
                sql.append("'" +splittedArray[i] + "'" + ",");
            }

        }
        sql.append(")");
        return sql.toString();
    }



}
