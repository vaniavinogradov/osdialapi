package ua.iptel.java.osdialapi.dao.osdial;

import ua.iptel.java.osdialapi.dao.AbstractDao;
import ua.iptel.java.osdialapi.model.Script;

public interface ScriptDao extends AbstractDao<Script> {
}
