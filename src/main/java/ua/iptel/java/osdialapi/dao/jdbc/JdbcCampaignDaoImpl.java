package ua.iptel.java.osdialapi.dao.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ua.iptel.java.osdialapi.dao.exception.PersistException;
import ua.iptel.java.osdialapi.dao.osdial.CampaignDao;
import ua.iptel.java.osdialapi.model.Campaign;

import java.sql.*;
import java.util.List;

@Repository
public class JdbcCampaignDaoImpl implements CampaignDao {
    @Autowired
    @Qualifier("jdbcTemplateOsdialDb")
    private JdbcTemplate jdbcTemplate;

    @Override
    public Campaign getByPK(Long id) throws PersistException {
        List<Campaign> dialLists = jdbcTemplate.query("SELECT * FROM osdial_campaigns WHERE campaign_id = ?", new CampaignRowMapper() , id);
        return DataAccessUtils.singleResult(dialLists);
    }

    @Override
    public List<Campaign> getAll() throws PersistException {
        return jdbcTemplate.query("SELECT * FROM osdial_campaigns", new CampaignRowMapper());
    }

    @Override
    public List<Campaign> getShort() {
        return jdbcTemplate.query("SELECT campaign_id FROM osdial_campaigns", new ShortCampaignRowMapper());
    }

    @Override
    public Campaign create(Campaign campaign) throws PersistException {
        PreparedStatementCreator psc = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(final Connection connection) throws SQLException {
                final PreparedStatement ps = connection.prepareStatement("INSERT INTO osdial_campaigns" +
                                "(campaign_name, active, dial_status_a, dial_status_b, dial_status_c," +
                                " dial_status_d, dial_status_e, lead_order, park_ext, park_file_name, web_form_address," +
                                " allow_closers, hopper_level, auto_dial_level, next_agent_call, local_call_time," +
                                " voicemail_ext, dial_timeout, dial_prefix, campaign_cid, campaign_vdad_exten," +
                                " campaign_rec_exten, campaign_recording, campaign_rec_filename, campaign_script," +
                                " get_call_launch, am_message_exten, amd_send_to_vmx, xferconf_a_dtmf, xferconf_a_number," +
                                " xferconf_b_dtmf, xferconf_b_number, alt_number_dialing, scheduled_callbacks, lead_filter_id," +
                                " drop_call_seconds, safe_harbor_message, safe_harbor_exten, display_dialable_count, wrapup_seconds," +
                                " wrapup_message, closer_campaigns, use_internal_dnc, allcalls_delay, omit_phone_code, dial_method," +
                                " available_only_ratio_tally, adaptive_dropped_percentage, adaptive_maximum_level," +
                                " adaptive_latest_server_time, adaptive_intensity, adaptive_dl_diff_target, concurrent_transfers, " +
                                "auto_alt_dial, auto_alt_dial_statuses, agent_pause_codes_active, campaign_description," +
                                " campaign_changedate, campaign_stats_refresh, dial_statuses, disable_alter_custdata," +
                                " no_hopper_leads_logins, list_order_mix, campaign_allow_inbound, manual_dial_list_id, default_xfer_group," +
                                " xfer_groups, web_form_address2, allow_tab_switch, answers_per_hour_limit, campaign_call_time," +
                                " preview_force_dial_time, manual_preview_default, web_form_extwindow, web_form2_extwindow," +
                                " submit_method, use_custom2_callerid, campaign_lastcall, campaign_cid_name, xfer_cid_mode," +
                                " use_cid_areacode_map, carrier_id, email_templates, disable_manual_dial, hide_xfer_local_closer," +
                                " hide_xfer_dial_override, hide_xfer_hangup_xfer, hide_xfer_leave_3way, hide_xfer_dial_with," +
                                " hide_xfer_hangup_both, hide_xfer_blind_xfer, hide_xfer_park_dial, hide_xfer_blind_vmail," +
                                " allow_md_hopperlist, ivr_id) " +
                                " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,?, ?, ?, ?, ?, ?, ?, ?, " +
                                         "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,?, ?, ?, ?, ?, ?, ?, ?," +
                                         "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,?, ?, ?, ?, ?, ?, ?, ?," +
                                         "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,?, ?, ?, ?, ?, ?, ?, ?," +
                                         "?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
                                         "?, ? ,?, ?, ?)",
                        Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, campaign.getName());
                ps.setString(2, convertBooleanToEnumString(campaign.isActive()));
                ps.setString(3, campaign.getDialStatusA());
                ps.setString(4, campaign.getDialStatusB());
                ps.setString(5, campaign.getDialStatusC());
                ps.setString(6, campaign.getDialStatusD());
                ps.setString(7, campaign.getDialStatusE());
                ps.setString(8, campaign.getLeadOrder());
                ps.setString(9, campaign.getParkExt());
                ps.setString(10, campaign.getParkFileName());
                ps.setString(11, campaign.getWebFormAddress());
                ps.setString(12, convertBooleanToEnumString(campaign.isAllowClosers()));
                ps.setInt(13, campaign.getHopper_level());
                ps.setString(14, campaign.getAutoDialLevel());
                ps.setString(15, campaign.getNextAgentCall());
                ps.setString(16, campaign.getLocalCallTime());
                ps.setString(17, campaign.getVoiceMailExtension());
                ps.setInt(18, campaign.getDialTimeout());
                ps.setString(19, campaign.getDialPrefix());
                ps.setString(20, campaign.getCid());
                ps.setString(21, campaign.getVdadExtension());
                ps.setString(22, campaign.getRecordingExtension());
                ps.setString(23, campaign.getRecording());
                ps.setString(24, campaign.getRecordingFileName());
                ps.setString(25, campaign.getScript());
                ps.setString(26, campaign.getGetCallLaunch());
                ps.setString(27, campaign.getAmMessageExtension());
                ps.setString(28, campaign.getAmdSendToVmx());
                ps.setString(29, campaign.getXferConfADtmf());
                ps.setString(30, campaign.getXferConfANumber());
                ps.setString(31, campaign.getXferConfBDtmf());
                ps.setString(32, campaign.getXferConfBNumber());
                ps.setString(33, convertBooleanToEnumString(campaign.isAltNumberDialing()));
                ps.setString(34, convertBooleanToEnumString(campaign.isAllowScheduledCallBacks()));
                ps.setString(35, campaign.getLeadFilterId());
                ps.setInt(36, campaign.getDropCallSeconds());
                ps.setString(37, convertBooleanToEnumString(campaign.isAllowSafeHarborMessage()));
                ps.setString(38, convertBooleanToEnumString(campaign.isAllowSafeHarborExtension()));
                ps.setString(39, convertBooleanToEnumString(campaign.isAllowDisplayDialableCount()));
                ps.setInt(40, campaign.getWrapUpSeconds());
                ps.setString(41, campaign.getWrapUpMessage());
                ps.setString(42, campaign.getCloserCampaigns());
                ps.setString(43, convertBooleanToEnumString(campaign.isAllowUseInternalDnc()));
                ps.setInt(44, campaign.getAllCallsDelay());
                ps.setString(45, convertBooleanToEnumString(campaign.isOmitPhoneCode()));
                ps.setString(46, campaign.getDialMethod());
                ps.setString(47, convertBooleanToEnumString(campaign.isAvailableOnlyRatioTally()));
                ps.setInt(48, campaign.getAdaptiveDroppedPercentage());
                ps.setString(49, campaign.getAdaptiveMaximumLevel());
                ps.setString(50, campaign.getAdaptiveLatestServerTime());
                ps.setString(51, campaign.getAdaptiveIntensity());
                ps.setInt(52, campaign.getAdaptiveDlDiffTarget());
                ps.setString(53, campaign.getConcurrentTransfers());
                ps.setString(54, campaign.getAutoAltDial());
                ps.setString(55, campaign.getAutoAltDialStatuses());
                ps.setString(56, convertBooleanToEnumString(campaign.isAgentPauseCodesActive()));
                ps.setString(57, campaign.getDescription());
                ps.setTimestamp(58, Timestamp.valueOf(campaign.getChangeDateTime()));
                ps.setString(59, convertBooleanToEnumString(campaign.isStatsRefresh()));
                //ps.setTimestamp(60, Timestamp.valueOf(campaign.getLoginDateTime()));
                ps.setString(60, campaign.getDialStatuses());
                ps.setString(61, convertBooleanToEnumString(campaign.isDisableAlterCustData()));
                ps.setString(62, convertBooleanToEnumString(campaign.isNoHopperLeadsLogins()));
                ps.setString(63, campaign.getListOrderMix());
                ps.setString(64, convertBooleanToEnumString(campaign.isAllowCampaignInbound()));
                ps.setInt(65, campaign.getManualDialListId());
                ps.setString(66, campaign.getDefaultXferGroup());
                ps.setString(67, campaign.getXferGroups());
                ps.setString(68, campaign.getWebFormAddress2());
                ps.setString(69, convertBooleanToEnumString(campaign.isAllowTabSwitch()));
                ps.setInt(70, campaign.getAnswersPerHourLimit());
                ps.setString(71, campaign.getCampaignCallTime());
                ps.setInt(72, campaign.getPreviewForceDialTime());
                ps.setString(73, convertBooleanToEnumString(campaign.isManualPreviewDefault()));
                ps.setString(74, convertBooleanToEnumString(campaign.isWebFormExternalWindow()));
                ps.setString(75, convertBooleanToEnumString(campaign.isWebForm2ExternalWindow()));
                ps.setString(76, campaign.getSubmitMethod());
                ps.setString(77, convertBooleanToEnumString(campaign.isUseCustom2CallierId()));
                ps.setTimestamp(78, Timestamp.valueOf(campaign.getCampaignLastCall()));
                ps.setString(79, campaign.getCampaignCidName());
                ps.setString(80, campaign.getXferCidMode());
                ps.setString(81, convertBooleanToEnumString(campaign.isUseCidAreaCodeMap()));
                ps.setInt(82, campaign.getCarrierId());
                ps.setString(83, campaign.getEmailTemplate());
                ps.setString(84, convertBooleanToEnumString(campaign.isDisableManualDial()));
                ps.setString(85, convertBooleanToEnumString(campaign.isHideXferLocalCloser()));
                ps.setString(86, convertBooleanToEnumString(campaign.isHideXferDialOverride()));
                ps.setString(87, convertBooleanToEnumString(campaign.isHideXferHangupXfer()));
                ps.setString(88, convertBooleanToEnumString(campaign.isHideXferLeave3way()));
                ps.setString(89, convertBooleanToEnumString(campaign.isHideXferDialWith()));
                ps.setString(90, convertBooleanToEnumString(campaign.isHideXferHangupBoth()));
                ps.setString(91, convertBooleanToEnumString(campaign.isHideXferBlindXfer()));
                ps.setString(92, convertBooleanToEnumString(campaign.isHideXferParkDial()));
                ps.setString(93, convertBooleanToEnumString(campaign.isHideXferBlindVmail()));
                ps.setString(94, convertBooleanToEnumString(campaign.isAllowMdHopperList()));

                if(campaign.getIvrId() == null){
                    ps.setInt(95, 0);
                }else {
                    ps.setInt(95, campaign.getIvrId());
                }

                return ps;
            }
        };
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(psc, keyHolder);

        Integer newId;
        if (keyHolder.getKeys().size() > 1) {
            newId = (Integer)keyHolder.getKeys().get("campaign_id");
        } else {
            newId= keyHolder.getKey().intValue();
        }
        campaign.setId(Long.valueOf(newId));
        return campaign;
    }

    @Override
    public Campaign update(Campaign campaign) throws PersistException {
        String sql = "UPDATE osdial_campaigns SET campaign_name = ?, active = ?, dial_status_a = ?, dial_status_b = ?, dial_status_c  = ?," +
                "dial_status_d = ?, dial_status_e = ?, lead_order = ?, park_ext = ?, park_file_name = ?, web_form_address = ?," +
                "allow_closers = ?, hopper_level = ?, auto_dial_level = ?, next_agent_call = ?, local_call_time = ?," +
                "voicemail_ext = ?, dial_timeout = ?, dial_prefix = ?, campaign_cid = ?, campaign_vdad_exten = ?," +
                "campaign_rec_exten = ?, campaign_recording = ?, campaign_rec_filename = ?, campaign_script = ?," +
                "get_call_launch  = ?, am_message_exten  = ?, amd_send_to_vmx = ?, xferconf_a_dtmf = ?, xferconf_a_number = ?," +
                "xferconf_b_dtmf = ?, xferconf_b_number = ?, alt_number_dialing = ?, scheduled_callbacks = ?, lead_filter_id = ?," +
                "drop_call_seconds = ?, safe_harbor_message = ?, safe_harbor_exten = ?, display_dialable_count = ?, wrapup_seconds = ?," +
                "wrapup_message = ?, closer_campaigns = ?, use_internal_dnc = ?, allcalls_delay = ?, omit_phone_code = ?, dial_method = ?," +
                "available_only_ratio_tally = ?, adaptive_dropped_percentage = ?, adaptive_maximum_level = ?," +
                "adaptive_latest_server_time = ?, adaptive_intensity = ?, adaptive_dl_diff_target = ?, concurrent_transfers = ?," +
                "auto_alt_dial = ?, auto_alt_dial_statuses = ?, agent_pause_codes_active = ?, campaign_description = ?," +
                "campaign_changedate = ?, campaign_stats_refresh = ?, dial_statuses = ?, disable_alter_custdata = ?," +
                "no_hopper_leads_logins = ?, list_order_mix = ?, campaign_allow_inbound = ?, manual_dial_list_id = ?, default_xfer_group = ?," +
                "xfer_groups = ?, web_form_address2 = ?, allow_tab_switch = ?, answers_per_hour_limit = ?, campaign_call_time = ?," +
                "preview_force_dial_time = ?, manual_preview_default = ?, web_form_extwindow = ?, web_form2_extwindow = ?," +
                "submit_method = ?, use_custom2_callerid = ?, campaign_lastcall = ?, campaign_cid_name = ?, xfer_cid_mode = ?," +
                "use_cid_areacode_map = ?, carrier_id = ?, email_templates = ?, disable_manual_dial = ?, hide_xfer_local_closer = ?," +
                "hide_xfer_dial_override = ?, hide_xfer_hangup_xfer = ?, hide_xfer_leave_3way = ?, hide_xfer_dial_with = ?," +
                "hide_xfer_hangup_both = ?, hide_xfer_blind_xfer = ?, hide_xfer_park_dial = ?, hide_xfer_blind_vmail  = ?," +
                "allow_md_hopperlist = ?, ivr_id = ? WHERE campaign_id = ?";
        return (jdbcTemplate.update(sql,
                campaign.getName(),
                convertBooleanToEnumString(campaign.isActive()),
                campaign.getDialStatusA(),
                campaign.getDialStatusB(),
                campaign.getDialStatusC(),
                campaign.getDialStatusD(),
                campaign.getDialStatusE(),
                campaign.getLeadOrder(),
                campaign.getParkExt(),
                campaign.getParkFileName(),
                campaign.getWebFormAddress(),
                convertBooleanToEnumString(campaign.isAllowClosers()),
                campaign.getHopper_level(),
                campaign.getAutoDialLevel(),
                campaign.getNextAgentCall(),
                campaign.getLocalCallTime(),
                campaign.getVoiceMailExtension(),
                campaign.getDialTimeout(),
                campaign.getDialPrefix(),
                campaign.getCid(),
                campaign.getVdadExtension(),
                campaign.getRecordingExtension(),
                campaign.getRecording(),
                campaign.getRecordingFileName(),
                campaign.getScript(),
                campaign.getGetCallLaunch(),
                campaign.getAmMessageExtension(),
                campaign.getAmdSendToVmx(),
                campaign.getXferConfADtmf(),
                campaign.getXferConfANumber(),
                campaign.getXferConfBDtmf(),
                campaign.getXferConfBNumber(),
                convertBooleanToEnumString(campaign.isAltNumberDialing()),
                convertBooleanToEnumString(campaign.isAllowScheduledCallBacks()),
                campaign.getLeadFilterId(),
                campaign.getDropCallSeconds(),
                convertBooleanToEnumString(campaign.isAllowSafeHarborMessage()),
                convertBooleanToEnumString(campaign.isAllowSafeHarborExtension()),
                convertBooleanToEnumString(campaign.isAllowDisplayDialableCount()),
                campaign.getWrapUpSeconds(),
                campaign.getWrapUpMessage(),
                campaign.getCloserCampaigns(),
                convertBooleanToEnumString(campaign.isAllowUseInternalDnc()),
                campaign.getAllCallsDelay(),
                convertBooleanToEnumString(campaign.isOmitPhoneCode()),
                campaign.getDialMethod(),
                convertBooleanToEnumString(campaign.isAvailableOnlyRatioTally()),
                campaign.getAdaptiveDroppedPercentage(),
                campaign.getAdaptiveMaximumLevel(),
                campaign.getAdaptiveLatestServerTime(),
                campaign.getAdaptiveIntensity(),
                campaign.getAdaptiveDlDiffTarget(),
                campaign.getConcurrentTransfers(),
                campaign.getAutoAltDial(),
                campaign.getAutoAltDialStatuses(),
                convertBooleanToEnumString(campaign.isAgentPauseCodesActive()),
                campaign.getDescription(),
                Timestamp.valueOf(campaign.getChangeDateTime()),
                convertBooleanToEnumString(campaign.isStatsRefresh()),
        //ps.setTimestamp(60, Timestamp.valueOf(campaign.getLoginDateTime()));
                campaign.getDialStatuses(),
                convertBooleanToEnumString(campaign.isDisableAlterCustData()),
                convertBooleanToEnumString(campaign.isNoHopperLeadsLogins()),
                campaign.getListOrderMix(),
                convertBooleanToEnumString(campaign.isAllowCampaignInbound()),
                campaign.getManualDialListId(),
                campaign.getDefaultXferGroup(),
                campaign.getXferGroups(),
                campaign.getWebFormAddress2(),
                convertBooleanToEnumString(campaign.isAllowTabSwitch()),
                campaign.getAnswersPerHourLimit(),
                campaign.getCampaignCallTime(),
                campaign.getPreviewForceDialTime(),
                convertBooleanToEnumString(campaign.isManualPreviewDefault()),
                convertBooleanToEnumString(campaign.isWebFormExternalWindow()),
                convertBooleanToEnumString(campaign.isWebForm2ExternalWindow()),
                campaign.getSubmitMethod(),
                convertBooleanToEnumString(campaign.isUseCustom2CallierId()),
                Timestamp.valueOf(campaign.getCampaignLastCall()),
                campaign.getCampaignCidName(),
                campaign.getXferCidMode(),
                convertBooleanToEnumString(campaign.isUseCidAreaCodeMap()),
                campaign.getCarrierId(),
                campaign.getEmailTemplate(),
                convertBooleanToEnumString(campaign.isDisableManualDial()),
                convertBooleanToEnumString(campaign.isHideXferLocalCloser()),
                convertBooleanToEnumString(campaign.isHideXferDialOverride()),
                convertBooleanToEnumString(campaign.isHideXferHangupXfer()),
                convertBooleanToEnumString(campaign.isHideXferLeave3way()),
                convertBooleanToEnumString(campaign.isHideXferDialWith()),
                convertBooleanToEnumString(campaign.isHideXferHangupBoth()),
                convertBooleanToEnumString(campaign.isHideXferBlindXfer()),
                convertBooleanToEnumString(campaign.isHideXferParkDial()),
                convertBooleanToEnumString(campaign.isHideXferBlindVmail()),
                convertBooleanToEnumString(campaign.isAllowMdHopperList()),
                campaign.getIvrId(),
                campaign.getId()) == 0) ? null : campaign;
    }

    @Override
    public Long delete(Long id) throws PersistException {
        return Long.valueOf(jdbcTemplate.update("DELETE FROM osdial_campaigns WHERE campaign_id=?", id));
    }



    private class CampaignRowMapper implements RowMapper {
        @Override
        public Object mapRow(ResultSet rs, int i) throws SQLException {
            Campaign campaign = new Campaign();
            campaign.setId(Long.valueOf(rs.getString("campaign_id")));
            campaign.setName(rs.getString("campaign_name"));
            campaign.setActive(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("active")));
            campaign.setDialStatusA(rs.getString("dial_status_a"));
            campaign.setDialStatusB(rs.getString("dial_status_b"));
            campaign.setDialStatusC(rs.getString("dial_status_c"));
            campaign.setDialStatusD(rs.getString("dial_status_d"));
            campaign.setDialStatusE(rs.getString("dial_status_e"));
            campaign.setLeadOrder(rs.getString("lead_order"));
            campaign.setParkExt(rs.getString("park_ext"));
            campaign.setParkFileName(rs.getString("park_file_name"));
            campaign.setWebFormAddress(rs.getString("web_form_address"));
            campaign.setAllowClosers(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("allow_closers")));
            campaign.setHopper_level(rs.getInt("hopper_level"));
            campaign.setAutoDialLevel(rs.getString("auto_dial_level"));
            campaign.setNextAgentCall(rs.getString("next_agent_call"));
            campaign.setLocalCallTime(rs.getString("local_call_time"));
            campaign.setVoiceMailExtension(rs.getString("voicemail_ext"));
            campaign.setDialTimeout(rs.getInt("dial_timeout"));
            campaign.setDialPrefix(rs.getString("dial_prefix"));
            campaign.setCid(rs.getString("campaign_cid"));
            campaign.setVdadExtension(rs.getString("campaign_vdad_exten"));
            campaign.setRecordingExtension(rs.getString("campaign_rec_exten"));
            campaign.setRecording(rs.getString("campaign_recording"));
            campaign.setRecordingFileName(rs.getString("campaign_recording"));
            campaign.setScript(rs.getString("campaign_script"));
            campaign.setGetCallLaunch(rs.getString("get_call_launch"));
            campaign.setAmMessageExtension(rs.getString("am_message_exten"));
            campaign.setAmdSendToVmx(rs.getString("amd_send_to_vmx"));
            campaign.setXferConfADtmf(rs.getString("xferconf_a_dtmf"));
            campaign.setXferConfANumber(rs.getString("xferconf_a_number"));
            campaign.setXferConfBDtmf(rs.getString("xferconf_b_dtmf"));
            campaign.setXferConfBNumber(rs.getString("xferconf_b_number"));
            campaign.setAltNumberDialing(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("alt_number_dialing")));
            campaign.setAllowScheduledCallBacks(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("scheduled_callbacks")));
            campaign.setLeadFilterId(rs.getString("lead_filter_id"));
            campaign.setDropCallSeconds(rs.getInt("drop_call_seconds"));
            campaign.setAllowSafeHarborMessage(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("safe_harbor_message")));
            campaign.setAllowDisplayDialableCount(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("display_dialable_count")));
            campaign.setWrapUpSeconds(rs.getInt("wrapup_seconds"));
            campaign.setWrapUpMessage(rs.getString("wrapup_message"));
            campaign.setCloserCampaigns(rs.getString("closer_campaigns"));
            campaign.setAllowUseInternalDnc(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("use_internal_dnc")));
            campaign.setAllCallsDelay(rs.getInt("allcalls_delay"));
            campaign.setOmitPhoneCode(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("omit_phone_code")));
            campaign.setDialMethod(rs.getString("dial_method"));
            campaign.setAvailableOnlyRatioTally(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("available_only_ratio_tally")));
            campaign.setAdaptiveDroppedPercentage(rs.getInt("adaptive_dropped_percentage"));
            campaign.setAdaptiveMaximumLevel(rs.getString("adaptive_maximum_level"));
            campaign.setAdaptiveLatestServerTime(rs.getString("adaptive_latest_server_time"));
            campaign.setAdaptiveIntensity(rs.getString("adaptive_intensity"));
            campaign.setAdaptiveDlDiffTarget(rs.getInt("adaptive_dl_diff_target"));
            campaign.setConcurrentTransfers(rs.getString("concurrent_transfers"));
            campaign.setAutoAltDial(rs.getString("auto_alt_dial"));
            campaign.setAutoAltDialStatuses(rs.getString("auto_alt_dial_statuses"));
            campaign.setDescription(rs.getString("campaign_description"));
            campaign.setChangeDateTime(rs.getTimestamp("campaign_changedate").toLocalDateTime());
            campaign.setStatsRefresh(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("campaign_stats_refresh")));
            campaign.setDialStatuses(rs.getString("dial_statuses"));
            campaign.setDisableAlterCustData(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("disable_alter_custdata")));
            campaign.setNoHopperLeadsLogins(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("no_hopper_leads_logins")));
            campaign.setListOrderMix(rs.getString("list_order_mix"));
            campaign.setAllowCampaignInbound(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("campaign_allow_inbound")));
            campaign.setManualDialListId(rs.getInt("manual_dial_list_id"));
            campaign.setDefaultXferGroup(rs.getString("default_xfer_group"));
            campaign.setXferGroups(rs.getString("xfer_groups"));
            campaign.setWebFormAddress2(rs.getString("web_form_address2"));
            campaign.setAllowTabSwitch(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("allow_tab_switch")));
            campaign.setAnswersPerHourLimit(rs.getInt("answers_per_hour_limit"));
            campaign.setPreviewForceDialTime(rs.getInt("preview_force_dial_time"));
            campaign.setManualPreviewDefault(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("manual_preview_default")));
            campaign.setWebFormExternalWindow(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("web_form_extwindow")));
            campaign.setWebForm2ExternalWindow(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("web_form2_extwindow")));
            campaign.setSubmitMethod(rs.getString("submit_method"));
            campaign.setUseCustom2CallierId(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("use_custom2_callerid")));
            campaign.setCampaignLastCall(rs.getTimestamp("campaign_lastcall").toLocalDateTime());
            campaign.setCid(rs.getString("campaign_cid_name"));
            campaign.setUseCidAreaCodeMap(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("use_cid_areacode_map")));
            campaign.setCarrierId(rs.getInt("carrier_id"));
            campaign.setEmailTemplate(rs.getString("email_templates"));
            campaign.setDisableManualDial(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("disable_manual_dial")));
            campaign.setHideXferLocalCloser(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("hide_xfer_local_closer")));
            campaign.setHideXferDialOverride(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("hide_xfer_dial_override")));
            campaign.setHideXferHangupXfer(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("hide_xfer_hangup_xfer")));
            campaign.setHideXferLeave3way(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("hide_xfer_leave_3way")));
            campaign.setHideXferDialWith(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("hide_xfer_dial_with")));
            campaign.setHideXferBlindXfer(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("hide_xfer_blind_xfer")));
            campaign.setHideXferParkDial(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("hide_xfer_park_dial")));
            campaign.setHideXferBlindVmail(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("hide_xfer_blind_vmail")));
            campaign.setAllowMdHopperList(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("allow_md_hopperlist")));
            campaign.setIvrId(rs.getInt("ivr_id"));
            return campaign;
        }
    }

    private class ShortCampaignRowMapper implements RowMapper {
        @Override
        public Object mapRow(ResultSet rs, int i) throws SQLException {
            Campaign campaign = new Campaign();
            campaign.setId(Long.valueOf(rs.getString("campaign_id")));
            return campaign;
        }
    }

    private boolean convertEnumValueFromMysqlOsdialDbToBoolean(String enumValue) {
        if (enumValue == null) return false;
        else if(enumValue.equals("N")) {
            return false;
        } else {
            return true;
        }
    }

    private String convertBooleanToEnumString(Boolean value){
        if(value) {
            return "Y";
        }else {
            return "N";
        }
    }
}
