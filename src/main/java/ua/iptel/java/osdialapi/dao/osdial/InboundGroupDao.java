package ua.iptel.java.osdialapi.dao.osdial;

import ua.iptel.java.osdialapi.dao.AbstractDao;
import ua.iptel.java.osdialapi.model.InboundGroup;

public interface InboundGroupDao extends AbstractDao<InboundGroup> {
}
