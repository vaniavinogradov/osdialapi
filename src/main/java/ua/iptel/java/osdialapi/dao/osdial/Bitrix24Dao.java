package ua.iptel.java.osdialapi.dao.osdial;

import ua.iptel.java.osdialapi.dao.AbstractDao;
import ua.iptel.java.osdialapi.model.dto.Bitrix24Settings;

public interface Bitrix24Dao extends AbstractDao<Bitrix24Settings> {
}
