package ua.iptel.java.osdialapi.dao.osdial;

import ua.iptel.java.osdialapi.dao.AbstractDao;
import ua.iptel.java.osdialapi.model.Campaign;

import java.util.List;

public interface CampaignDao extends AbstractDao<Campaign> {
    List<Campaign> getShort();
}
