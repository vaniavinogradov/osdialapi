package ua.iptel.java.osdialapi.dao.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ua.iptel.java.osdialapi.dao.exception.PersistException;
import ua.iptel.java.osdialapi.dao.osdial.CallBackDao;
import ua.iptel.java.osdialapi.model.CallBack;

import java.sql.*;
import java.util.List;

@Repository
public class JdbcCallBackDao implements CallBackDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;


    @Override
    public CallBack getByPK(Long id) throws PersistException {
        try {
            List<CallBack> callBackList =
                    jdbcTemplate.query("select * from osdial_callbacks where callback_id = ? limit 1",
                    new CallBackRowMapper() , id );
            return DataAccessUtils.singleResult(callBackList);
        }
        catch (Exception ex){
            return null;
        }
    }

    @Override
    public List<CallBack> getAll() throws PersistException {
        try {
            List<CallBack> callBackList = jdbcTemplate.query("select * from osdial_callbacks where status IN ('LIVE', 'ACTIVE')",
                    new CallBackRowMapper());
            return callBackList;
        }
        catch (Exception ex){
            return null;
        }
    }

    @Override
    public CallBack create(CallBack callBack) throws PersistException {
        try{
            String sql = "INSERT INTO osdial_callbacks( " +
                    " lead_id," +
                    " list_id," +
                    " campaign_id," +
                    " status," +
                    " entry_time," +
                    " callback_time," +
                    " modify_date," +
                    " user," +
                    " recipient," +
                    " comments," +
                    " user_group)" +
                    "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatementCreator psc = new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(final Connection connection) throws SQLException {
                    final PreparedStatement ps = connection.prepareStatement(sql,
                            Statement.RETURN_GENERATED_KEYS);
                    ps.setLong(1, callBack.getLeadId());
                    ps.setLong(2, callBack.getListId());
                    ps.setLong(3, callBack.getCampaignId());
                    ps.setString(4, callBack.getStatus());
                    ps.setTimestamp(5, Timestamp.valueOf(callBack.getEntryTime()));
                    ps.setTimestamp(6, Timestamp.valueOf(callBack.getCallBackTime()));
                    ps.setDate(7, Date.valueOf(callBack.getModifyDate()));
                    ps.setString(8, callBack.getUser());
                    ps.setString(9, callBack.getRecipient());
                    ps.setString(10, callBack.getComment());
                    ps.setString(11, callBack.getUserGroup());

                    return ps;
                }
            };
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(psc, keyHolder);

            Long newId;
            if (keyHolder.getKeys().size() > 1) {
                newId = (Long)keyHolder.getKeys().get("callback_id");
            } else {
                newId= keyHolder.getKey().longValue();
            }
            callBack.setId(newId);
            return callBack;
        }catch (Exception ex){
            return null;
        }
    }

    @Override
    public CallBack update(CallBack callBack) throws PersistException {
        try {
            String sql = "UPDATE osdial_callbacks SET " +
                    " lead_id = ?," +
                    " list_id = ?," +
                    " campaign_id = ?," +
                    " status = ?," +
                    " entry_time = ?, " +
                    " callback_time = ?," +
                    " modify_date = ?," +
                    " user = ?, " +
                    " recipient = ?," +
                    " comments = ?," +
                    " user_group = ?" +
                    " WHERE callback_id = ?";
            PreparedStatementCreator psc = new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(final Connection connection) throws SQLException {
                    final PreparedStatement ps = connection.prepareStatement(sql);

                    ps.setLong(1, callBack.getLeadId());
                    ps.setLong(2, callBack.getListId());
                    ps.setLong(3, callBack.getCampaignId());
                    ps.setString(4, callBack.getStatus());
                    ps.setTimestamp(5, Timestamp.valueOf(callBack.getEntryTime()));
                    ps.setTimestamp(6, Timestamp.valueOf(callBack.getCallBackTime()));
                    ps.setDate(7, Date.valueOf(callBack.getModifyDate()));
                    ps.setString(8, callBack.getUser());
                    ps.setString(9, callBack.getRecipient());
                    ps.setString(10, callBack.getComment());
                    ps.setString(11, callBack.getUserGroup());
                    ps.setLong(12, callBack.getId());
                    return ps;
                }
            };
            jdbcTemplate.update(psc);
            return callBack;
        }catch (Exception ex){
            return null;
        }
    }

    @Override
    public Long delete(Long id) throws PersistException {
        try {
            Long result = Long.valueOf(jdbcTemplate.update("DELETE FROM osdial_callbacks WHERE callback_id = ?", id));
            return result;
        }catch (Exception ex){
            return null;
        }
    }

    private class CallBackRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
          CallBack callBack = new CallBack();
          callBack.setId(rs.getLong("callback_id"));
          callBack.setLeadId(rs.getLong("lead_id"));
          callBack.setListId(rs.getLong("list_id"));
          callBack.setCampaignId(rs.getLong("campaign_id"));
          callBack.setStatus(rs.getString("status"));
          callBack.setEntryTime(rs.getTimestamp("entry_time").toLocalDateTime());
          callBack.setCallBackTime(rs.getTimestamp("callback_time").toLocalDateTime());
          callBack.setModifyDate(rs.getDate("modify_date").toLocalDate());
          callBack.setUser(rs.getString("user"));
          callBack.setRecipient(rs.getString("recipient"));
          callBack.setComment(rs.getString("comments"));
          callBack.setUserGroup(rs.getString("user_group"));
          return callBack;
        }

    }
}
