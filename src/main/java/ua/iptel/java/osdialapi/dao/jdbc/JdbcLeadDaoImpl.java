package ua.iptel.java.osdialapi.dao.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ua.iptel.java.osdialapi.dao.exception.PersistException;
import ua.iptel.java.osdialapi.dao.osdial.LeadDao;
import ua.iptel.java.osdialapi.model.Lead;
import ua.iptel.java.osdialapi.model.dto.LeadSimpleDto;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Repository
public class JdbcLeadDaoImpl implements LeadDao {
    @Autowired
    @Qualifier("jdbcTemplateOsdialDb")
    private JdbcTemplate jdbcTemplate;

    @Override
    public Lead getByPK(Long id) throws PersistException {
        List<Lead> leadList = jdbcTemplate.query("SELECT * FROM osdial_list WHERE lead_id = ?", new LeadRowMapper() , id);
        return DataAccessUtils.singleResult(leadList);
    }

    @Override
    public List<Lead> getByListId(Long listId) {
        return jdbcTemplate.query("SELECT * FROM osdial_list WHERE list_id = ?", new LeadRowMapper(), listId);
    }

    @Override
    public List<Lead> getByCampaignId(Long campaignId) {
        return null;
    }

    @Override
    public List<Lead> getByStatuses(List<String> statuses) {
        StringBuilder sql = new StringBuilder("SELECT * FROM osdial_list WHERE status = ");
        List<Lead> result = new LinkedList<>();
        if(statuses.size() == 0){
            return result;
        }else {
            for (int i = 0; i < statuses.size(); i++) {
                sql.append("'" + statuses.get(i) + "'");
                if(i != (statuses.size() - 1)){
                    sql.append(" OR status = ");
                }else {
                    sql.append(";");
                }
            }
            return jdbcTemplate.query(sql.toString(), new LeadRowMapper());
        }

    }

    @Override
    public List<Lead> insertBatchLeads(List<Lead> leads) {
        String sql = "INSERT INTO " +
                "osdial_list(entry_date, modify_date, status, user, vendor_lead_code, source_id," +
                " list_id, gmt_offset_now, called_since_last_reset, phone_code, phone_number, title," +
                " first_name, middle_initial, last_name, address1, address2, address3, city, state," +
                " province, postal_code, country_code, gender, date_of_birth, alt_phone, email, custom1," +
                " comments, called_count, custom2, external_key, cost, post_date," +
                " organization, organization_title, status_extended)" +
                " VALUES " +
                "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
                " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
                " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
                " ?, ?, ?, ?, ?, ?, ?)";

        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {

            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                Lead lead = leads.get(i);
                ps.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
                ps.setTimestamp(2, Timestamp.valueOf(LocalDateTime.now()));
                ps.setString(3, lead.getStatus());
                ps.setString(4, lead.getUser());
                ps.setString(5, lead.getVendorLeadCode());
                ps.setString(6, lead.getSourceId());
                ps.setLong(7, lead.getListId());
                ps.setFloat(8, lead.getGmtOffsetNow());
                ps.setString(9, lead.getCalledSinceLastReset());
                ps.setString(10, lead.getPhoneCode());
                ps.setString(11, lead.getPhoneNumber());
                ps.setString(12, lead.getTitle());
                ps.setString(13, lead.getFirstName());
                ps.setString(14, lead.getMiddleInitial());
                ps.setString(15, lead.getLastName());
                ps.setString(16, lead.getAddress1());
                ps.setString(17, lead.getAddress2());
                ps.setString(18, lead.getAddress3());
                ps.setString(19, lead.getCity());
                ps.setString(20, lead.getState());
                ps.setString(21, lead.getProvince());
                ps.setString(22, lead.getPostalCode());
                ps.setString(23, lead.getCountryCode());
                ps.setString(24, lead.getGender());

                LocalDateTime dateOfBirth = lead.getDateOfBirth();
                if (dateOfBirth != null) {
                    ps.setDate(25, Date.valueOf(dateOfBirth.toLocalDate()));
                }else {
                    ps.setDate(25, Date.valueOf("1001-01-01"));
                }

                ps.setString(26, lead.getAltPhone());
                ps.setString(27, lead.getEmail());
                ps.setString(28, lead.getCustom1());
                ps.setString(29, lead.getComments());

                Integer callCount = lead.getCalledCount();
                if(callCount != null){
                    ps.setInt(30, callCount);
                }else {
                    ps.setInt(30, 0);
                }

                ps.setString(31, lead.getCustom2());
                ps.setString(32, String.valueOf(lead.getExternalKey()));

                ps.setFloat(33, lead.getCost());
                ps.setTimestamp(34, Timestamp.valueOf(LocalDateTime.now()));
                ps.setString(35, lead.getOrganization());
                ps.setString(36, lead.getOrganizationTitle());
                ps.setString(37, lead.getStatusExtended());
            }

            @Override
            public int getBatchSize() {
                return leads.size();
            }
        });
        return leads;
    }

    @Override
    public Lead getByExternalKey(Long externalKey) {
        List<Lead> leadList = jdbcTemplate.query("SELECT * FROM osdial_list WHERE external_key = ? LIMIT 1", new LeadRowMapper() , externalKey);
        return DataAccessUtils.singleResult(leadList);
    }

    @Override
    public Integer addOnlyUniqueueLeads(List<LeadSimpleDto> leads) {
        Integer counter = 0;
        for (LeadSimpleDto lead : leads) {
            if(!leadNumberExists(lead) && lead.getListId() != null && !lead.getPhoneNumber().isEmpty()){
                Lead currentLead = convertDtoToLead(lead);
                Lead createdLead = this.create(currentLead);
                if(createdLead != null){
                    counter++;
                }

            }
        }
        return counter;
    }



    @Override
    public List<Lead> getAll() throws PersistException {
        return jdbcTemplate.query("SELECT * FROM osdial_list", new LeadRowMapper());
    }

    @Override
    public Lead create(Lead lead) throws PersistException {
        PreparedStatementCreator psc = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(final Connection connection) throws SQLException {
                final PreparedStatement ps = connection.prepareStatement("INSERT INTO " +
                                "osdial_list(entry_date, modify_date, status, user, vendor_lead_code, source_id," +
                                " list_id, gmt_offset_now, called_since_last_reset, phone_code, phone_number, title," +
                                " first_name, middle_initial, last_name, address1, address2, address3, city, state," +
                                " province, postal_code, country_code, gender, date_of_birth, alt_phone, email, custom1," +
                                " comments, called_count, custom2, external_key, cost, post_date," +
                                " organization, organization_title, status_extended)" +
                                " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
                                        " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
                                        " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
                                        " ?, ?, ?, ?, ?, ?, ?)",
                        Statement.RETURN_GENERATED_KEYS);
                ps.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
                ps.setTimestamp(2, Timestamp.valueOf(LocalDateTime.now()));
                ps.setString(3, lead.getStatus());
                ps.setString(4, lead.getUser());
                ps.setString(5, lead.getVendorLeadCode());
                ps.setString(6, lead.getSourceId());
                ps.setLong(7, lead.getListId());
                ps.setFloat(8, lead.getGmtOffsetNow());
                ps.setString(9, lead.getCalledSinceLastReset());
                ps.setString(10, lead.getPhoneCode());
                ps.setString(11, lead.getPhoneNumber());
                ps.setString(12, lead.getTitle());
                ps.setString(13, lead.getFirstName());
                ps.setString(14, lead.getMiddleInitial());
                ps.setString(15, lead.getLastName());
                ps.setString(16, lead.getAddress1());
                ps.setString(17, lead.getAddress2());
                ps.setString(18, lead.getAddress3());
                ps.setString(19, lead.getCity());
                ps.setString(20, lead.getState());
                ps.setString(21, lead.getProvince());
                ps.setString(22, lead.getPostalCode());
                ps.setString(23, lead.getCountryCode());
                ps.setString(24, lead.getGender());

                LocalDateTime dateOfBirth = lead.getDateOfBirth();
                if (dateOfBirth != null) {
                    ps.setDate(25, Date.valueOf(dateOfBirth.toLocalDate()));
                }else {
                    ps.setDate(25, Date.valueOf("1001-01-01"));
                }

                ps.setString(26, lead.getAltPhone());
                ps.setString(27, lead.getEmail());
                ps.setString(28, lead.getCustom1());
                ps.setString(29, lead.getComments());

                Integer callCount = lead.getCalledCount();
                if(callCount != null){
                    ps.setInt(30, callCount);
                }else {
                    ps.setInt(30, 0);
                }

                ps.setString(31, lead.getCustom2());
                ps.setString(32, String.valueOf(lead.getExternalKey()));

                ps.setFloat(33, lead.getCost());
                ps.setTimestamp(34, Timestamp.valueOf(LocalDateTime.now()));
                ps.setString(35, lead.getOrganization());
                ps.setString(36, lead.getOrganizationTitle());
                ps.setString(37, lead.getStatusExtended());
                return ps;
            }
        };
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(psc, keyHolder);

        Integer newId;
        if (keyHolder.getKeys().size() > 1) {
            newId = (Integer)keyHolder.getKeys().get("list_id");
        } else {
            newId= keyHolder.getKey().intValue();
        }
        lead.setId(Long.valueOf(newId));
        return lead;
    }

    @Override
    public Lead update(Lead lead) throws PersistException {
        return (jdbcTemplate.update("UPDATE osdial_list SET modify_date = ?, status = ?, user = ?, vendor_lead_code = ?," +
                        " source_id = ?, list_id = ?, gmt_offset_now = ?, called_since_last_reset = ?, phone_code = ?," +
                        " phone_number = ?, title = ?, first_name = ?, middle_initial = ?, last_name = ?, address1 = ?," +
                        " address2 = ?, address3 = ?, city = ?, state = ?, province = ?, postal_code = ?, country_code = ?," +
                        " gender = ?, date_of_birth = ?, alt_phone = ?, email = ?, custom1 = ?, comments = ?," +
                        " custom2 = ?, external_key = ?, cost = ?, organization = ?, organization_title = ?, status_extended = ? " +
                        " WHERE lead_id = ?",
                LocalDateTime.now(),
                lead.getStatus(),
                lead.getUser(),
                lead.getVendorLeadCode(),
                lead.getSourceId(),
                lead.getListId(),
                lead.getGmtOffsetNow(),
                lead.getCalledSinceLastReset(),
                lead.getPhoneCode(),
                lead.getPhoneNumber(),
                lead.getTitle(),
                lead.getFirstName(),
                lead.getMiddleInitial(),
                lead.getLastName(),
                lead.getAddress1(),
                lead.getAddress2(),
                lead.getAddress3(),
                lead.getCity(),
                lead.getState(),
                lead.getProvince(),
                lead.getPostalCode(),
                lead.getCountryCode(),
                lead.getGender(),
                Date.valueOf(lead.getDateOfBirth().toLocalDate()),
                lead.getAltPhone(),
                lead.getEmail(),
                lead.getCustom1(),
                lead.getComments(),
                lead.getCustom2(),
                (lead.getExternalKey() == null ? "": lead.getExternalKey()),
                lead.getCost(),
                lead.getOrganization(),
                lead.getOrganizationTitle(),
                lead.getStatusExtended(),
                lead.getId()) == 0) ? null : lead;
    }

    @Override
    public Long delete(Long id) throws PersistException {
        return Long.valueOf(jdbcTemplate.update("DELETE FROM osdial_list WHERE lead_id=?", id));
    }

    @Override
    public void deleteLeadsFromPoolByListId(Long listId) {
        jdbcTemplate.update("DELETE FROM osdial_hopper WHERE list_id=?", listId);
    }

    @Override
    public void deleteLeadsFromPoolByCampaignId(Long campaignId) {
        jdbcTemplate.update("DELETE FROM osdial_hopper WHERE campaign_id=?", campaignId);
    }



    private class LeadRowMapper implements RowMapper {
        @Override
        public Object mapRow(ResultSet rs, int i) throws SQLException {
              Lead lead = new Lead();
              lead.setId(rs.getLong("lead_id"));
              Timestamp entryDate = rs.getTimestamp("entry_date");
              if(entryDate != null){
                  lead.setEntryDate(entryDate.toLocalDateTime());
              }

            Timestamp modifyDate = rs.getTimestamp("modify_date");
            if(modifyDate != null){
                lead.setModifyDateTime(modifyDate.toLocalDateTime());
            }

            lead.setStatus(rs.getString("status"));
            lead.setUser(rs.getString("user"));
            lead.setVendorLeadCode(rs.getString("vendor_lead_code"));
            lead.setSourceId(rs.getString("source_id"));
            lead.setListId(rs.getLong("list_id"));
            lead.setGmtOffsetNow(rs.getFloat("gmt_offset_now"));
            lead.setCalledSinceLastReset(rs.getString("called_since_last_reset"));
            lead.setPhoneCode(rs.getString("phone_code"));
            lead.setPhoneNumber(rs.getString("phone_number"));
            lead.setTitle(rs.getString("title"));
            lead.setFirstName(rs.getString("first_name"));
            lead.setMiddleInitial(rs.getString("middle_initial"));
            lead.setLastName(rs.getString("last_name"));
            lead.setAddress1(rs.getString("address1"));
            lead.setAddress2(rs.getString("address2"));
            lead.setAddress3(rs.getString("address3"));
            lead.setCity(rs.getString("city"));
            lead.setState(rs.getString("state"));
            lead.setProvince(rs.getString("province"));
            lead.setPostalCode(rs.getString("postal_code"));
            lead.setCountryCode(rs.getString("country_code"));
            lead.setGender(rs.getString("gender"));
            lead.setDateOfBirth(rs.getTimestamp("date_of_birth").toLocalDateTime());
            lead.setAltPhone(rs.getString("alt_phone"));
            lead.setEmail(rs.getString("email"));
            lead.setCustom1(rs.getString("custom1"));
            lead.setCustom2(rs.getString("custom2"));

            String externalKey = rs.getString("external_key");
            if(!externalKey.isEmpty() && externalKey != null && !externalKey.equals("null")){
                lead.setExternalKey(Long.valueOf(externalKey));
            }


            lead.setLastLocalCallTime(rs.getTimestamp("last_local_call_time").toLocalDateTime());
            lead.setCost(rs.getFloat("cost"));

            Timestamp postDate = rs.getTimestamp("post_date");
            if(postDate != null){
                lead.setPostDateTime(postDate.toLocalDateTime());
            }
            lead.setOrganization(rs.getString("organization"));
            lead.setOrganizationTitle(rs.getString("organization_title"));
            lead.setStatusExtended(rs.getString("status_extended"));
          return lead;
        }
    }

    private boolean leadNumberExists(LeadSimpleDto leadSimpleDto) {
        boolean isLeadNumberExistsInCampaing = false;
        String sql = "select count(*) from osdial_list where list_id in (select list_id from osdial_lists " +
                "where campaign_id = (select campaign_id from osdial_lists where list_id=?)) and phone_number = ?";
        int count = jdbcTemplate.queryForObject(sql, new Object[] { leadSimpleDto.getListId(), leadSimpleDto.getPhoneNumber() }, Integer.class);

        if(count > 0){
            isLeadNumberExistsInCampaing = true;
        }
        return isLeadNumberExistsInCampaing;
    }

    private Lead convertDtoToLead(LeadSimpleDto leadSimpleDto){
        Lead lead = new Lead();
        lead.setEntryDate(LocalDateTime.now());
        lead.setModifyDateTime(LocalDateTime.now());
        lead.setStatus("NEW");
        lead.setUser("");
        lead.setVendorLeadCode("");
        lead.setSourceId("");
        lead.setListId(leadSimpleDto.getListId());
        lead.setGmtOffsetNow(2.0f);
        lead.setCalledSinceLastReset("N");
        lead.setPhoneCode(leadSimpleDto.getPhoneCode());
        lead.setPhoneNumber(leadSimpleDto.getPhoneNumber());
        lead.setTitle("");
        lead.setFirstName(leadSimpleDto.getFirstName());
        lead.setMiddleInitial("");
        lead.setLastName(leadSimpleDto.getLastName());
        lead.setAddress1(leadSimpleDto.getAddress1());
        lead.setAddress2("");
        lead.setAddress3("");
        lead.setCity(leadSimpleDto.getCity());
        lead.setState("");
        lead.setProvince("");
        lead.setCountryCode(leadSimpleDto.getCountryCode());
        lead.setGender("");
        lead.setAltPhone(leadSimpleDto.getAltPhone());
        lead.setEmail(leadSimpleDto.getEmail());
        lead.setCustom1(leadSimpleDto.getCustom1());
        lead.setCustom2(leadSimpleDto.getCustom2());
        lead.setComments(leadSimpleDto.getComments());
        lead.setExternalKey(leadSimpleDto.getExternalKey());
        lead.setOrganization(leadSimpleDto.getOrganization());
        lead.setCost(0.0f);
        lead.setPostalCode("");
        lead.setOrganizationTitle("");
        lead.setStatusExtended("");
        return lead;
    }


}
