package ua.iptel.java.osdialapi.dao.osdial;

import ua.iptel.java.osdialapi.dao.exception.PersistException;
import ua.iptel.java.osdialapi.model.Company;

public interface CompanyDao {
    Company get() throws PersistException;
}
