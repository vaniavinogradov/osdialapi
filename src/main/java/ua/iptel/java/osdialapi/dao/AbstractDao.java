package ua.iptel.java.osdialapi.dao;

import ua.iptel.java.osdialapi.dao.exception.PersistException;

import java.util.List;

/**
 * Created by ipvinner on 12.10.2016.
 */
public interface AbstractDao<T> {
//    T persist(T object) throws PersistException;
    T getByPK(Long id) throws PersistException;
    List<T> getAll() throws PersistException;
    T create(T object) throws PersistException;
    T update(T object) throws PersistException;
    Long delete(Long id) throws PersistException;
}
