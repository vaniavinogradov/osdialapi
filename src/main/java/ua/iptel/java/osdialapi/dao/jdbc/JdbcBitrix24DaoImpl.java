package ua.iptel.java.osdialapi.dao.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.iptel.java.osdialapi.dao.exception.PersistException;
import ua.iptel.java.osdialapi.dao.osdial.Bitrix24Dao;
import ua.iptel.java.osdialapi.model.MediaFile;
import ua.iptel.java.osdialapi.model.dto.Bitrix24Settings;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
@Transactional
public class JdbcBitrix24DaoImpl implements Bitrix24Dao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Bitrix24Settings getByPK(Long id) throws PersistException {
        String sql = "select * from bx24_settings limit 1";
        return null;
    }

    @Override
    public List<Bitrix24Settings> getAll() throws PersistException {
        return null;
    }

    @Override
    public Bitrix24Settings create(Bitrix24Settings object) throws PersistException {
        return null;
    }

    @Override
    public Bitrix24Settings update(Bitrix24Settings bitrix24Settings) throws PersistException {
        return (jdbcTemplate.update("UPDATE bx24_settings SET bx24_incoming_webhook = ?",
                bitrix24Settings.getWebHookUrl()) == 0) ? null : bitrix24Settings;
    }

    @Override
    public Long delete(Long id) throws PersistException {
        return null;
    }

    private class Bitrix24SettingsRowMapper implements RowMapper {
        @Override
        public Object mapRow(ResultSet rs, int i) throws SQLException {
            Bitrix24Settings bitrix24Settings = new Bitrix24Settings();
            bitrix24Settings.setId(rs.getLong("id"));
            bitrix24Settings.setWebHookUrl("bx24_incoming_webhook");
            return bitrix24Settings;
        }
    }
}
