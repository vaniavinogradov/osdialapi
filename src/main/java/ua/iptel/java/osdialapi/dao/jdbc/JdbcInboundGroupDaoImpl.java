package ua.iptel.java.osdialapi.dao.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ua.iptel.java.osdialapi.dao.exception.PersistException;
import ua.iptel.java.osdialapi.dao.osdial.InboundGroupDao;
import ua.iptel.java.osdialapi.model.InboundGroup;

import java.sql.*;
import java.util.List;

@Repository
public class JdbcInboundGroupDaoImpl implements InboundGroupDao {
    @Autowired
    @Qualifier("jdbcTemplateOsdialDb")
    private JdbcTemplate jdbcTemplate;

    @Override
    public InboundGroup getByPK(Long id) throws PersistException {
        List<InboundGroup> inboundGroupList = jdbcTemplate.query("SELECT * FROM osdial_inbound_groups WHERE group_id = ?", new InboundGroupRowMapper() , id);
        return DataAccessUtils.singleResult(inboundGroupList);
    }

    @Override
    public List<InboundGroup> getAll() throws PersistException {
        return jdbcTemplate.query("SELECT * FROM osdial_inbound_groups", new InboundGroupRowMapper());
    }

    @Override
    public InboundGroup create(InboundGroup inboundGroup) throws PersistException {
        PreparedStatementCreator psc = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(final Connection connection) throws SQLException {
                final PreparedStatement ps = connection.prepareStatement("INSERT INTO " +
                                "osdial_inbound_groups(group_name, group_color, active, web_form_address, voicemail_ext," +
                                " next_agent_call, fronter_display, ingroup_script, get_call_launch, xferconf_a_dtmf," +
                                " xferconf_a_number, xferconf_b_dtmf, xferconf_b_number, drop_call_seconds, drop_action," +
                                " drop_exten, call_time_id, after_hours_action, after_hours_message_filename, after_hours_exten," +
                                " after_hours_voicemail, welcome_message_filename, moh_context, onhold_prompt_filename, prompt_interval," +
                                " agent_alert_exten, agent_alert_delay, default_xfer_group, web_form_address2, allow_tab_switch," +
                                " web_form_extwindow, web_form2_extwindow, drop_trigger, allow_multicall, placement_interval," +
                                " placement_max_repeat, queuetime_interval, queuetime_max_repeat, background_music_filename, drop_message_filename," +
                                " callback_interval, callback_interrupt_key, onhold_startdelay, callback_startdelay, placement_startdelay," +
                                " queuetime_startdelay, prompt_language, welcome_message_min_playtime)" +
                                " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
                                         "?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
                                         "?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
                                         "?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
                                         "?, ?, ?, ?, ?, ?, ?, ? )",
                        Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, inboundGroup.getName());
                ps.setString(2, inboundGroup.getColor());
                ps.setString(3, convertBooleanToEnumString(inboundGroup.isActive()));
                ps.setString(4, inboundGroup.getWebFormAddress());
                ps.setString(5, inboundGroup.getVoicemailExtension());
                ps.setString(6, inboundGroup.getNextAgentCall());
                ps.setString(7, convertBooleanToEnumString(inboundGroup.isAllowFronterDisplay()));
                ps.setString(8, inboundGroup.getInboundScript());
                ps.setString(9, inboundGroup.getGetCallLaunch());
                ps.setString(10, inboundGroup.getXferConfADtmf());
                ps.setString(11, inboundGroup.getXferConfANumber());
                ps.setString(12, inboundGroup.getXferConfBDtmf());
                ps.setString(13, inboundGroup.getXferConfBNumber());
                ps.setInt(14, inboundGroup.getDropCallSeconds());
                ps.setString(15, inboundGroup.getDropAction());
                ps.setString(16, inboundGroup.getDropExtension());
                ps.setLong(17, inboundGroup.getCallTimeId());
                ps.setString(18, inboundGroup.getAfterHoursAction());
                ps.setString(19, inboundGroup.getAfterHoursMessageFilename());
                ps.setString(20, inboundGroup.getAfterHoursExtension());
                ps.setString(21, inboundGroup.getAfterHoursVoiceMail());
                ps.setString(22, inboundGroup.getWelcomeMessageFilename());
                ps.setString(23, inboundGroup.getMohContext());
                ps.setString(24, inboundGroup.getOnHoldPromptFilename());
                ps.setInt(25, inboundGroup.getPromptInterval());
                ps.setString(26, inboundGroup.getAgentAlertExtension());
                ps.setInt(27, inboundGroup.getAgentAlertDelay());
                ps.setString(28, inboundGroup.getDefaultXferGroup());
                ps.setString(29, inboundGroup.getWebFormAddress2());
                ps.setString(30, convertBooleanToEnumString(inboundGroup.isAllowTabSwitch()));
                ps.setString(31, convertBooleanToEnumString(inboundGroup.isAllowWebFormExternalWindow()));
                ps.setString(32, convertBooleanToEnumString(inboundGroup.isAllowWebForm2ExternalWindow()));
                ps.setString(33, inboundGroup.getDropTrigger());
                ps.setString(34, convertBooleanToEnumString(inboundGroup.isAllowMultiCall()));
                ps.setInt(35, inboundGroup.getPlacementInterval());
                ps.setInt(36, inboundGroup.getPlacementMaxRepeat());
                ps.setInt(37, inboundGroup.getQueueTimeInterval());
                ps.setInt(38, inboundGroup.getQueueMaxRepeat());
                ps.setString(39, inboundGroup.getBackgroundMusicFilename());
                ps.setString(40, inboundGroup.getDropMessageFilename());
                ps.setInt(41, inboundGroup.getCallbackInterval());
                ps.setString(42, inboundGroup.getCallbackIntervalKey());
                ps.setInt(43, inboundGroup.getOnHoldStartDelay());
                ps.setInt(44, inboundGroup.getCallbackStartDelay());
                ps.setInt(45, inboundGroup.getPlacementStartDelay());
                ps.setInt(46, inboundGroup.getQueueTimeStartDelay());
                ps.setString(47, inboundGroup.getPromptLanguage());
                ps.setString(48, inboundGroup.getWelcomeMessageMinPlayTime());
                return ps;
            }
        };
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(psc, keyHolder);

        Integer newId;
        if (keyHolder.getKeys().size() > 1) {
            newId = (Integer)keyHolder.getKeys().get("group_id");
        } else {
            newId= keyHolder.getKey().intValue();
        }
        inboundGroup.setId(Long.valueOf(newId));
        return inboundGroup;
    }

    @Override
    public InboundGroup update(InboundGroup inboundGroup) throws PersistException {
        return (jdbcTemplate.update("UPDATE osdial_inbound_groups SET group_name = ?, group_color = ?, active = ?," +
                        " web_form_address = ?, voicemail_ext = ?, next_agent_call = ?, fronter_display = ?, ingroup_script = ?," +
                        " get_call_launch = ?, xferconf_a_dtmf = ?, xferconf_a_number = ?, xferconf_b_dtmf = ?, xferconf_b_number =?," +
                        " drop_call_seconds = ?, drop_action = ?, drop_exten = ?, call_time_id = ?, after_hours_action = ?, " +
                        " after_hours_message_filename = ?, after_hours_exten  = ?, after_hours_voicemail = ?, " +
                        " welcome_message_filename = ?, moh_context = ?, onhold_prompt_filename = ?, prompt_interval = ?," +
                        " agent_alert_exten = ?, agent_alert_delay = ?, default_xfer_group = ?, web_form_address2 = ?, allow_tab_switch = ?, " +
                        " web_form_extwindow = ?, web_form2_extwindow = ?," +
                        " drop_trigger = ?, allow_multicall = ?, placement_interval = ?, placement_max_repeat = ?, " +
                        " queuetime_interval = ?, queuetime_max_repeat = ?, background_music_filename = ?," +
                        " drop_message_filename = ?, callback_interval = ?, callback_interrupt_key = ?, onhold_startdelay = ?," +
                        " callback_startdelay = ?, placement_startdelay = ?, queuetime_startdelay = ?, prompt_language = ?, welcome_message_min_playtime = ?" +
                        " WHERE group_id =?",
                inboundGroup.getName(),
                inboundGroup.getColor(),
                convertBooleanToEnumString(inboundGroup.isActive()),
                inboundGroup.getWebFormAddress(),
                inboundGroup.getVoicemailExtension(),
                inboundGroup.getNextAgentCall(),
                convertBooleanToEnumString(inboundGroup.isAllowFronterDisplay()),
                inboundGroup.getInboundScript(),
                inboundGroup.getGetCallLaunch(),
                inboundGroup.getXferConfADtmf(),
                inboundGroup.getXferConfANumber(),
                inboundGroup.getXferConfBDtmf(),
                inboundGroup.getXferConfBNumber(),
                inboundGroup.getDropCallSeconds(),
                inboundGroup.getDropAction(),
                inboundGroup.getDropExtension(),
                inboundGroup.getCallTimeId(),
                inboundGroup.getAfterHoursAction(),
                inboundGroup.getAfterHoursMessageFilename(),
                inboundGroup.getAfterHoursExtension(),
                inboundGroup.getAfterHoursVoiceMail(),
                inboundGroup.getWelcomeMessageFilename(),
                inboundGroup.getMohContext(),
                inboundGroup.getOnHoldPromptFilename(),
                inboundGroup.getPromptInterval(),
                inboundGroup.getAgentAlertExtension(),
                inboundGroup.getAgentAlertDelay(),
                inboundGroup.getDefaultXferGroup(),
                inboundGroup.getWebFormAddress2(),
                convertBooleanToEnumString(inboundGroup.isAllowTabSwitch()),
                convertBooleanToEnumString(inboundGroup.isAllowWebFormExternalWindow()),
                convertBooleanToEnumString(inboundGroup.isAllowWebForm2ExternalWindow()),
                inboundGroup.getDropTrigger(),
                convertBooleanToEnumString(inboundGroup.isAllowMultiCall()),
                inboundGroup.getPlacementInterval(),
                inboundGroup.getPlacementMaxRepeat(),
                inboundGroup.getQueueTimeInterval(),
                inboundGroup.getQueueMaxRepeat(),
                inboundGroup.getBackgroundMusicFilename(),
                inboundGroup.getDropMessageFilename(),
                inboundGroup.getCallbackInterval(),
                inboundGroup.getCallbackIntervalKey(),
                inboundGroup.getOnHoldStartDelay(),
                inboundGroup.getCallbackStartDelay(),
                inboundGroup.getPlacementStartDelay(),
                inboundGroup.getQueueTimeStartDelay(),
                inboundGroup.getPromptLanguage(),
                inboundGroup.getWelcomeMessageMinPlayTime(),
                inboundGroup.getId()) == 0) ? null : inboundGroup;
    }

    @Override
    public Long delete(Long id) throws PersistException {
        return Long.valueOf(jdbcTemplate.update("DELETE FROM osdial_inbound_groups WHERE id=?", id));
    }

    private class InboundGroupRowMapper implements RowMapper {
        @Override
        public Object mapRow(ResultSet rs, int i) throws SQLException {
            InboundGroup inboundGroup = new InboundGroup();
            inboundGroup.setName(rs.getString("group_name"));
            inboundGroup.setColor(rs.getString("group_color"));
            inboundGroup.setActive(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("active")));
            inboundGroup.setWebFormAddress(rs.getString("web_form_address"));
            inboundGroup.setVoicemailExtension(rs.getString("voicemail_ext"));
            inboundGroup.setNextAgentCall(rs.getString("next_agent_call"));
            inboundGroup.setAllowFronterDisplay(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("fronter_display")));
            inboundGroup.setInboundScript(rs.getString("ingroup_script"));
            inboundGroup.setGetCallLaunch(rs.getString("get_call_launch"));
            inboundGroup.setXferConfADtmf(rs.getString("xferconf_a_dtmf"));
            inboundGroup.setXferConfANumber(rs.getString("xferconf_a_number"));
            inboundGroup.setXferConfBDtmf(rs.getString("xferconf_b_dtmf"));
            inboundGroup.setXferConfBNumber(rs.getString("xferconf_b_number"));
            inboundGroup.setDropCallSeconds(rs.getInt("drop_call_seconds"));
            inboundGroup.setDropAction(rs.getString("drop_action"));
            inboundGroup.setDropExtension(rs.getString("drop_exten"));
            inboundGroup.setCallTimeId(rs.getLong("call_time_id"));
            inboundGroup.setAfterHoursAction(rs.getString("after_hours_action"));
            inboundGroup.setAfterHoursMessageFilename(rs.getString("after_hours_message_filename"));
            inboundGroup.setAfterHoursExtension(rs.getString("after_hours_exten"));
            inboundGroup.setAfterHoursVoiceMail(rs.getString("after_hours_voicemail"));
            inboundGroup.setWelcomeMessageFilename(rs.getString("welcome_message_filename"));
            inboundGroup.setMohContext(rs.getString("moh_context"));
            inboundGroup.setOnHoldPromptFilename(rs.getString("onhold_prompt_filename"));
            inboundGroup.setPromptInterval(rs.getInt("prompt_interval"));
            inboundGroup.setAgentAlertExtension(rs.getString("agent_alert_exten"));
            inboundGroup.setAgentAlertDelay(rs.getInt("agent_alert_delay"));
            inboundGroup.setDefaultXferGroup(rs.getString("default_xfer_group"));
            inboundGroup.setWebFormAddress2(rs.getString("web_form_address2"));
            inboundGroup.setAllowTabSwitch(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("allow_tab_switch")));
            inboundGroup.setAllowWebFormExternalWindow(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("web_form_extwindow")));
            inboundGroup.setAllowWebForm2ExternalWindow(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("web_form2_extwindow")));
            inboundGroup.setDropTrigger(rs.getString("drop_trigger"));
            inboundGroup.setAllowMultiCall(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("allow_multicall")));
            inboundGroup.setPlacementInterval(rs.getInt("placement_interval"));
            inboundGroup.setPlacementMaxRepeat(rs.getInt("placement_max_repeat"));
            inboundGroup.setQueueTimeInterval(rs.getInt("queuetime_interval"));
            inboundGroup.setQueueMaxRepeat(rs.getInt("queuetime_max_repeat"));
            inboundGroup.setBackgroundMusicFilename(rs.getString("background_music_filename"));
            inboundGroup.setDropMessageFilename(rs.getString("drop_message_filename"));
            inboundGroup.setCallbackInterval(rs.getInt("callback_interval"));
            inboundGroup.setCallbackIntervalKey(rs.getString("callback_interrupt_key"));
            inboundGroup.setOnHoldStartDelay(rs.getInt("onhold_startdelay"));
            inboundGroup.setCallbackStartDelay(rs.getInt("callback_startdelay"));
            inboundGroup.setPlacementStartDelay(rs.getInt("placement_startdelay"));
            inboundGroup.setQueueTimeStartDelay(rs.getInt("queuetime_startdelay"));
            inboundGroup.setPromptLanguage(rs.getString("prompt_language"));
            inboundGroup.setWelcomeMessageMinPlayTime(rs.getString("welcome_message_min_playtime"));
            return inboundGroup;
        }
    }

    private boolean convertEnumValueFromMysqlOsdialDbToBoolean(String enumValue) {
        if (enumValue == null){
            return false;
        }else if(enumValue.equals("N")) {
            return false;
        } else {
            return true;
        }
    }

    private String convertBooleanToEnumString(Boolean value){
        if(value) {
            return "Y";
        }else {
            return "N";
        }
    }
}
