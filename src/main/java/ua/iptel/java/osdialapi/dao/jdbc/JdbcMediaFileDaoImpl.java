package ua.iptel.java.osdialapi.dao.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ua.iptel.java.osdialapi.dao.exception.PersistException;
import ua.iptel.java.osdialapi.dao.osdial.MediaFileDao;
import ua.iptel.java.osdialapi.model.MediaFile;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public class JdbcMediaFileDaoImpl implements MediaFileDao {

    @Autowired
    @Qualifier("jdbcTemplateOsdialDb")
    private JdbcTemplate jdbcTemplate;

    @Override
    public MediaFile getByPK(Long id) throws PersistException {
        List<MediaFile> mediaFileList = jdbcTemplate.query("SELECT * FROM osdial_media WHERE id = ?", new MediaFileRowMapper() , id);
        return DataAccessUtils.singleResult(mediaFileList);
    }

    @Override
    public List<MediaFile> getAll() throws PersistException {
        return jdbcTemplate.query("SELECT * FROM osdial_media", new MediaFileRowMapper());
    }

    @Override
    public MediaFile create(MediaFile mediaFile) throws PersistException {
        PreparedStatementCreator psc = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(final Connection connection) throws SQLException {
                final PreparedStatement ps = connection.prepareStatement("INSERT INTO " +
                                "osdial_media(filename, mimetype, description, extension, created)" +
                                " VALUES (?, ?, ?, ?, ?)",
                        Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, mediaFile.getFileName());
                ps.setString(2, mediaFile.getMimeType());
                ps.setString(3, mediaFile.getDescription());
                ps.setString(4, mediaFile.getExtension());
                ps.setTimestamp(5, Timestamp.valueOf(LocalDateTime.now()));
                return ps;
            }
        };
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(psc, keyHolder);

        Integer newId;
        if (keyHolder.getKeys().size() > 1) {
            newId = (Integer)keyHolder.getKeys().get("id");
        } else {
            newId= keyHolder.getKey().intValue();
        }
        mediaFile.setId(Long.valueOf(newId));
        return mediaFile;
    }

    @Override
    public MediaFile update(MediaFile mediaFile) throws PersistException {
        return (jdbcTemplate.update("UPDATE osdial_media SET filename = ?, mimetype = ?, description = ?," +
                        " extension = ? WHERE id = ?",
                mediaFile.getFileName(),
                mediaFile.getMimeType(),
                mediaFile.getDescription(),
                mediaFile.getExtension(),
                mediaFile.getId()) == 0) ? null : mediaFile;
    }

    @Override
    public Long delete(Long id) throws PersistException {
        return Long.valueOf(jdbcTemplate.update("DELETE FROM osdial_media WHERE id=?", id));
    }

    private class MediaFileRowMapper implements RowMapper {
        @Override
        public Object mapRow(ResultSet rs, int i) throws SQLException {
            MediaFile mediaFile = new MediaFile();
            mediaFile.setId(rs.getLong("id"));
            mediaFile.setFileName(rs.getString("filename"));
            mediaFile.setMimeType(rs.getString("mimetype"));
            mediaFile.setDescription(rs.getString("description"));
            mediaFile.setExtension(rs.getString("extension"));
            mediaFile.setCreated(rs.getTimestamp("created").toLocalDateTime());
            return mediaFile;
        }
    }
}
