package ua.iptel.java.osdialapi.dao.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ua.iptel.java.osdialapi.dao.exception.PersistException;
import ua.iptel.java.osdialapi.dao.osdial.ScriptDao;
import ua.iptel.java.osdialapi.model.Script;

import java.sql.*;
import java.util.List;

@Repository
public class JdbcScriptDaoImpl implements ScriptDao {
    @Autowired
    @Qualifier("jdbcTemplateOsdialDb")
    private JdbcTemplate jdbcTemplate;

    @Override
    public Script getByPK(Long id) throws PersistException {
        List<Script> scriptList = jdbcTemplate.query("SELECT * FROM osdial_scripts WHERE script_id = ?", new ScriptRowMapper() , id);
        return DataAccessUtils.singleResult(scriptList);
    }

    @Override
    public List<Script> getAll() throws PersistException {
        return jdbcTemplate.query("SELECT * FROM osdial_scripts", new ScriptRowMapper());
    }

    @Override
    public Script create(Script script) throws PersistException {
        PreparedStatementCreator psc = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(final Connection connection) throws SQLException {
                final PreparedStatement ps = connection.prepareStatement("INSERT INTO " +
                                "osdial_scripts(script_name, script_comments, script_text, active)" +
                                " VALUES (?, ?, ?, ?)",
                        Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, script.getName());
                ps.setString(2, script.getComments());
                ps.setString(3, script.getScriptText());
                ps.setString(4, convertBooleanToEnumString(script.isActive()));
                return ps;
            }
        };
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(psc, keyHolder);

        Integer newId;
        if (keyHolder.getKeys().size() > 1) {
            newId = (Integer)keyHolder.getKeys().get("script_id");
        } else {
            newId= keyHolder.getKey().intValue();
        }
        script.setId(Long.valueOf(newId));
        return script;
    }

    @Override
    public Script update(Script script) throws PersistException {
        return (jdbcTemplate.update("UPDATE osdial_scripts SET script_name = ?, script_comments = ?, script_text = ?," +
                        " active = ? WHERE script_id = ?",
                script.getName(),
                script.getComments(),
                script.getScriptText(),
                convertBooleanToEnumString(script.isActive()),
                script.getId()) == 0) ? null : script;
    }

    @Override
    public Long delete(Long id) throws PersistException {
        return Long.valueOf(jdbcTemplate.update("DELETE FROM osdial_scripts WHERE script_id=?", id));
    }

    private class ScriptRowMapper implements RowMapper {
        @Override
        public Object mapRow(ResultSet rs, int i) throws SQLException {
            Script script = new Script();
            script.setId(rs.getLong("script_id"));
            script.setName(rs.getString("script_name"));
            script.setComments(rs.getString("script_comments"));
            script.setScriptText(rs.getString("script_text"));
            script.setActive(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("active")));
            return script;
        }
    }

    private boolean convertEnumValueFromMysqlOsdialDbToBoolean(String enumValue) {
        if (enumValue == null) return false;
        else if(enumValue.equals("N")) {
            return false;
        } else {
            return true;
        }
    }

    private String convertBooleanToEnumString(Boolean value){
        if(value) {
            return "Y";
        }else {
            return "N";
        }
    }
}
