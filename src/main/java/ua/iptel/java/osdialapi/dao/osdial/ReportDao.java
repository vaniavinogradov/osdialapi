package ua.iptel.java.osdialapi.dao.osdial;

import ua.iptel.java.osdialapi.model.CallLog;
import ua.iptel.java.osdialapi.model.Recording;
import ua.iptel.java.osdialapi.model.dto.CallLogExtended;
import ua.iptel.java.osdialapi.model.report.agent.AgentCallsDetailedReport;
import ua.iptel.java.osdialapi.model.report.agent.AgentCallsStatistic;
import ua.iptel.java.osdialapi.model.report.agent.AgentStatusesDetailedReport;
import ua.iptel.java.osdialapi.model.report.pause.AgentPausesReport;
import ua.iptel.java.osdialapi.model.report.pause.CampaignPausesReport;
import ua.iptel.java.osdialapi.model.report.pause.DetailedPausesReport;
import ua.iptel.java.osdialapi.model.report.pause.GeneralPausesReport;

import java.time.LocalDateTime;
import java.util.List;

public interface ReportDao {
    List<GeneralPausesReport> getGeneralPausesReport(LocalDateTime from, LocalDateTime to, String agent, String campaigns);
    List<CampaignPausesReport> getCampaignPausesReport(LocalDateTime from, LocalDateTime to, String campaigns);
    List<AgentPausesReport> getAgentPausesReport(LocalDateTime from, LocalDateTime to, String agent, String campaigns);
    List<DetailedPausesReport> getDetailedPausesReport(LocalDateTime from, LocalDateTime to, String agent, String campaigns);

    List<AgentCallsDetailedReport> getAgentCallsDetailedReport(LocalDateTime from, LocalDateTime to, String agent, String campaign, String userGroup);
    List<AgentStatusesDetailedReport> getAgentStatusesDetailedReport(LocalDateTime from, LocalDateTime to, String agent, String campaign, String userGroup);

    List<AgentCallsStatistic> getAgentCallsStatistic(LocalDateTime from, LocalDateTime to, String agent, String type);

    List<Recording> getAllRecordings();

    List<Recording> getRecordingByLeadId(Long leadId);
    List<Recording> getRecordingsByPeriod(LocalDateTime from, LocalDateTime to);
    List<Recording> getRecordingsByPeriodAndUser(LocalDateTime from, LocalDateTime to, String user);

    List<CallLog> getCallsLog();
    List<CallLogExtended> getCallsLogWitchLeadName();
    List<CallLog> getCallsLogByLeadId(Long leadId);
    List<CallLogExtended> getCallsLogByLeadIdWithLeadName(Long leadId);
    List<CallLog> getCallLogByCampaignAndDateTimeRange(LocalDateTime from, LocalDateTime to, Integer campaignId);
    List<CallLog> getCallLogByListId(Integer listId);
}
