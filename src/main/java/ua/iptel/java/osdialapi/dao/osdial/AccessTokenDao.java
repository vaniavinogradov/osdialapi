package ua.iptel.java.osdialapi.dao.osdial;

import ua.iptel.java.osdialapi.model.AccessToken;

public interface AccessTokenDao {
    AccessToken findByToken(String accessTokenString);

    AccessToken findByUserId(Long id);

    AccessToken create(AccessToken accessToken);

    AccessToken update(AccessToken accessToken);
}
