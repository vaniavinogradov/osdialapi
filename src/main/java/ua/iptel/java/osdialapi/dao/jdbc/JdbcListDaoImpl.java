package ua.iptel.java.osdialapi.dao.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ua.iptel.java.osdialapi.dao.exception.PersistException;
import ua.iptel.java.osdialapi.dao.osdial.ListDao;
import ua.iptel.java.osdialapi.dao.osdial.UserGroupDao;
import ua.iptel.java.osdialapi.model.DialList;
import ua.iptel.java.osdialapi.model.UserGroup;
import ua.iptel.java.osdialapi.model.dto.CallsCountResponse;
import ua.iptel.java.osdialapi.service.UserGroupService;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@Repository
public class JdbcListDaoImpl implements ListDao {
    @Autowired
    @Qualifier("jdbcTemplateOsdialDb")
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private UserGroupService userGroupService;

    @Override
    public DialList getByPK(Long id) throws PersistException {
        List<DialList> dialLists = jdbcTemplate.query("SELECT * FROM osdial_lists WHERE (list_id = ? AND list_id >= 1001)", new ListRowMapper() , id);
        return DataAccessUtils.singleResult(dialLists);
    }

    @Override
    public List<DialList> getAllByUserGroup(String userGroup) {
        UserGroup userGroup1 = userGroupService.getByName(userGroup);
        userGroup1.getAllowedCampaigns();
        String allowedConverted = convertStrangeAllowedCampaingToListOfCampaignId(userGroup1.getAllowedCampaigns());
        if(allowedConverted.equals("all")){
            return this.getAll();
        }else {
            StringBuilder sql = new StringBuilder("SELECT * FROM osdial_lists WHERE ");
            sql.append("campaign_id IN (" + allowedConverted + ")");
            sql.append(" ORDER BY list_id desc");
            List<DialList> dialLists = jdbcTemplate.query(sql.toString(), new ListRowMapper());
            return dialLists;
        }
    }

    @Override
    public List<CallsCountResponse> getCallsCount() {
        String sql = "SELECT IF (osdial_statuses.category = 'IVR', 'Success', 'Not Success') AS call_status," +
                " sum(IF (called_since_last_reset LIKE \"Y%\", 1, 0)) AS called," +
                " sum(IF (called_since_last_reset LIKE \"Y%\", 0, 1)) AS not_called " +
                " FROM osdial_list LEFT JOIN osdial_statuses " +
                " ON osdial_list.status=osdial_statuses.status GROUP BY call_status;";
        List<CallsCountResponse> list = jdbcTemplate.query(sql, new CallsCountRowMapper());
        return list;
    }

    @Override
    public List<CallsCountResponse> getCallsCountByListId(Long listId) {
        String sql = "SELECT IF (osdial_statuses.category = 'IVR', 'Success', 'Not Success') AS call_status," +
                " sum(IF (called_since_last_reset LIKE \"Y%\", 1, 0)) AS called," +
                " sum(IF (called_since_last_reset LIKE \"Y%\", 0, 1)) AS not_called " +
                " FROM osdial_list LEFT JOIN osdial_statuses " +
                " ON osdial_list.status=osdial_statuses.status WHERE list_id=? GROUP BY call_status;";
        List<CallsCountResponse> list = jdbcTemplate.query(sql, new CallsCountRowMapper(), listId);
        return list;
    }

    @Override
    public List<DialList> getAll() throws PersistException {
        return jdbcTemplate.query("SELECT * FROM osdial_lists WHERE list_id >= 1001", new ListRowMapper());
    }

    @Override
    public DialList create(DialList dialList) throws PersistException {
        PreparedStatementCreator psc = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(final Connection connection) throws SQLException {
                final PreparedStatement ps = connection.prepareStatement("INSERT INTO " +
                                "osdial_lists(list_name, campaign_id, active, list_description," +
                                " list_changedate, scrub_dnc, scrub_info, cost, web_form_address," +
                                " web_form_address2, list_script, lead_transfer_id)" +
                                " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                        Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, dialList.getName());
                ps.setString(2, String.valueOf(dialList.getCampaignId()));
                ps.setString(3, convertBooleanToEnumString(dialList.isActive()));
                ps.setString(4, dialList.getDescription());
                ps.setTimestamp(5, Timestamp.valueOf(LocalDateTime.now()));
                ps.setString(6, convertBooleanToEnumString(dialList.isScrubDNC()));
                ps.setString(7, dialList.getScrubInfo());
                ps.setFloat(8, dialList.getCost());
                ps.setString(9, dialList.getWebFormAddress());
                ps.setString(10, dialList.getWebFormAddress2());
                ps.setString(11, dialList.getListScript());
                ps.setString(12, dialList.getLeadTransferId());
                return ps;
            }
        };
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(psc, keyHolder);

        Integer newId;
        if (keyHolder.getKeys().size() > 1) {
            newId = (Integer)keyHolder.getKeys().get("list_id");
        } else {
            newId= keyHolder.getKey().intValue();
        }
        dialList.setId(Long.valueOf(newId));
        return dialList;
    }

    @Override
    public DialList update(DialList dialList) throws PersistException {
        return (jdbcTemplate.update("UPDATE osdial_lists SET list_name = ?, campaign_id = ?, active = ?," +
                        " list_description = ?, list_changedate = ?, scrub_dnc = ?, scrub_info = ?, cost = ?, " +
                        " web_form_address = ?, web_form_address2 = ?, list_script = ?, lead_transfer_id = ?" +
                        " WHERE list_id = ?",
                dialList.getName(),
                dialList.getCampaignId(),
                convertBooleanToEnumString(dialList.isActive()),
                dialList.getDescription(),
                Timestamp.valueOf(LocalDateTime.now()),
                convertBooleanToEnumString(dialList.isScrubDNC()),
                dialList.getScrubInfo(),
                dialList.getCost(),
                dialList.getWebFormAddress(),
                dialList.getWebFormAddress2(),
                dialList.getListScript(),
                dialList.getLeadTransferId(),
                dialList.getId()) == 0) ? null : dialList;
    }

    @Override
    public Long delete(Long id) throws PersistException {
        return Long.valueOf(jdbcTemplate.update("DELETE FROM osdial_lists WHERE list_id=?", id));
    }



    private class ListRowMapper implements RowMapper {
        @Override
        public Object mapRow(ResultSet rs, int i) throws SQLException {
            DialList list = new DialList();
            list.setId(rs.getLong("list_id"));
            list.setName(rs.getString("list_name"));
            list.setCampaignId(Long.valueOf(rs.getString("campaign_id")));
            list.setActive(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("active")));
            list.setDescription(rs.getString("list_description"));

            Timestamp listChangeDate = rs.getTimestamp("list_changedate");
            if(listChangeDate != null) {
                list.setChangeDateTime(listChangeDate.toLocalDateTime());
            }

            Timestamp lastCallDate = rs.getTimestamp("list_lastcalldate");
            if(lastCallDate != null){
                list.setLastCallDate(lastCallDate.toLocalDateTime());
            }

            list.setScrubDNC(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("scrub_dnc")));
            list.setScrubInfo(rs.getString("scrub_info"));
            list.setCost(rs.getFloat("cost"));
            list.setWebFormAddress(rs.getString("web_form_address"));
            list.setWebFormAddress2(rs.getString("web_form_address2"));
            list.setListScript(rs.getString("list_script"));
            list.setLeadTransferId(rs.getString("lead_transfer_id"));
            return  list;
        }
    }

    private class CallsCountRowMapper implements RowMapper {
        @Override
        public Object mapRow(ResultSet rs, int i) throws SQLException {
            CallsCountResponse callsCountResponse = new CallsCountResponse();
            callsCountResponse.setCallStatus(rs.getString("call_status"));
            callsCountResponse.setCalledCallsCount(rs.getInt("called"));
            callsCountResponse.setNotCalledCallsCount(rs.getInt("not_called"));
            return callsCountResponse;
        }
    }

    private boolean convertEnumValueFromMysqlOsdialDbToBoolean(String enumValue) {
        if (enumValue == null){
            return false;
        }else if(enumValue.equals("N")) {
            return false;
        } else {
            return true;
        }
    }

    private String convertBooleanToEnumString(Boolean value){
        if(value) {
            return "Y";
        }else {
            return "N";
        }
    }

    private String convertStrangeAllowedCampaingToListOfCampaignId(String allowedCampaigns){
        String result = "";
        if(allowedCampaigns.startsWith(" -")){
            result = "all";
        }else {
            String temp = allowedCampaigns.substring(1);
            String temp2 = temp.substring(0, temp.length() - 2);
            List<String> list = Arrays.asList(temp2.split(" "));
            String temp3 = String.join(",", list);
            result = temp3;
        }
        return result;
    }
}
