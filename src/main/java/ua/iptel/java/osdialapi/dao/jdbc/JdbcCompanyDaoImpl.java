package ua.iptel.java.osdialapi.dao.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ua.iptel.java.osdialapi.dao.exception.PersistException;
import ua.iptel.java.osdialapi.dao.osdial.CompanyDao;
import ua.iptel.java.osdialapi.model.Company;
import ua.iptel.java.osdialapi.model.TariffPlan;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class JdbcCompanyDaoImpl implements CompanyDao {
    @Autowired
    @Qualifier("jdbcTemplateOsdialDb")
    private JdbcTemplate jdbcTemplate;

    @Override
    public Company get() throws PersistException {
        String sql = "SELECT company_name, tariff_id, tariff_plan_name, agents_limit, virtual_agents_limit, leads_limit, started, default_language " +
                "from system_settings " +
                "left outer join osdial_tariff_settings on system_settings.tariff_id = osdial_tariff_settings.id;";
        List<Company> contactList = jdbcTemplate.query(sql, new CompanyRowMapper());
        return DataAccessUtils.singleResult(contactList);
    }

    private class CompanyRowMapper implements RowMapper {
        @Override
        public Object mapRow(ResultSet rs, int i) throws SQLException {
            Company company = new Company();
            company.setName(rs.getString("company_name"));
            TariffPlan tariffPlan = new TariffPlan();
            tariffPlan.setId(rs.getInt("tariff_id"));
            tariffPlan.setName(rs.getString("tariff_plan_name"));
            tariffPlan.setAgentsLimit(rs.getInt("agents_limit"));
            tariffPlan.setVirtualAgentsLimit(rs.getInt("virtual_agents_limit"));
            tariffPlan.setLeadsLimit(rs.getInt("leads_limit"));
            tariffPlan.setDefaultLanguage(rs.getString("default_language"));
            tariffPlan.setPaymentDateTime(rs.getTimestamp("started").toLocalDateTime());
            company.setTariffPlan(tariffPlan);
            return company;
        }
    }
}
