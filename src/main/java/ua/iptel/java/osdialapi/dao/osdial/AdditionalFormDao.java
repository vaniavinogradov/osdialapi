package ua.iptel.java.osdialapi.dao.osdial;

import ua.iptel.java.osdialapi.dao.AbstractDao;
import ua.iptel.java.osdialapi.model.AdditionalForm;

public interface AdditionalFormDao extends AbstractDao<AdditionalForm>{
}
