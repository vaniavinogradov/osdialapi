package ua.iptel.java.osdialapi.dao.osdial;

import ua.iptel.java.osdialapi.dao.AbstractDao;
import ua.iptel.java.osdialapi.dao.exception.PersistException;
import ua.iptel.java.osdialapi.model.DialList;
import ua.iptel.java.osdialapi.model.dto.CallsCountResponse;

import java.util.List;


public interface ListDao extends AbstractDao<DialList> {
    // This methods is required for backward compatibility with old scructure of osdial_lists table
    // for now changed to Primary key, but some of lists can be Long: example 20180219122052
    DialList getByPK(Long id) throws PersistException;
    List<DialList> getAllByUserGroup(String userGroup);
    List<CallsCountResponse> getCallsCount();
    List<CallsCountResponse> getCallsCountByListId(Long listId);
}
