package ua.iptel.java.osdialapi.dao.osdial;

import ua.iptel.java.osdialapi.dao.AbstractDao;
import ua.iptel.java.osdialapi.model.User;

public interface UserDao extends AbstractDao<User> {
    User getByUserName(String username);
}