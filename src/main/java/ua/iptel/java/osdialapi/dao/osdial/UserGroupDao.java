package ua.iptel.java.osdialapi.dao.osdial;

import ua.iptel.java.osdialapi.dao.AbstractDao;
import ua.iptel.java.osdialapi.model.UserGroup;

public interface UserGroupDao extends AbstractDao<UserGroup> {
    UserGroup getByName(String userGroupName);
}
