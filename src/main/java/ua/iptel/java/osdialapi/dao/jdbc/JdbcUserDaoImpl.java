package ua.iptel.java.osdialapi.dao.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ua.iptel.java.osdialapi.dao.exception.PersistException;
import ua.iptel.java.osdialapi.dao.osdial.UserDao;
import ua.iptel.java.osdialapi.model.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

@Repository
public class JdbcUserDaoImpl implements UserDao {

    @Autowired
    @Qualifier("jdbcTemplateOsdialDb")
    private JdbcTemplate jdbcTemplate;

    @Override
    public User getByPK(Long id) throws PersistException {
        List<User> userList = jdbcTemplate.query("SELECT * FROM osdial_users WHERE user_id = ?", new UserRowMapper(), id);
        return DataAccessUtils.singleResult(userList);
    }

    @Override
    public User getByUserName(String username) {
        List<User> userList = jdbcTemplate.query("SELECT * FROM osdial_users WHERE user = ?", new UserRowMapper(), username);
        return DataAccessUtils.singleResult(userList);
    }

    @Override
    public List<User> getAll() throws PersistException {
        return jdbcTemplate.query("SELECT * FROM osdial_users", new UserRowMapper());
    }

    @Override
    public User create(User user) throws PersistException {
        jdbcTemplate.update("INSERT INTO osdial_users (user, pass, full_name, user_level, user_group," +
                        " phone_login, phone_pass, delete_users, delete_user_groups, delete_lists, delete_campaigns," +
                        " delete_ingroups, delete_remote_agents, load_leads, campaign_detail, ast_admin_access," +
                        " ast_delete_phones, delete_scripts, modify_leads, hotkeys_active, change_agent_campaign," +
                        " agent_choose_ingroups, closer_campaigns, scheduled_callbacks, agentonly_callbacks," +
                        " agentcall_manual, osdial_recording, osdial_transfers, delete_filters, alter_agent_interface_options," +
                        " closer_default_blended, delete_call_times, modify_call_times, modify_users, modify_campaigns," +
                        " modify_lists, modify_scripts, modify_filters, modify_ingroups, modify_usergroups, modify_remoteagents," +
                        " modify_servers, view_reports, osdial_recording_override, alter_custdata_override, manual_dial_new_limit, " +
                        " manual_dial_allow_skip, export_leads, admin_api_access, agent_api_access, xfer_agent2agent, script_override," +
                        " load_dnc, export_dnc, delete_dnc, access_code, modify_phones, modify_trunks, modify_ext," +
                        " modify_statuses, modify_conferensions, perm_administration_menu, password_md5)" +
                        " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
                                " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
                                " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
                                " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
                                " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
                                " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
                                " ?, ?, ?)",
                user.getUsername(),
                user.getPassword(),
                user.getFullName(),
                user.getUserLevel(),
                user.getUserGroup(),
                user.getPhoneLogin(),
                user.getPhonePassword(),
                (user.isAllowDeleteUsers() ? "1" : "0"),
                (user.isAllowDeleteUserGroups() ? "1" : "0"),
                (user.isAllowDeleteLists() ? "1" : "0"),
                (user.isAllowDeleteCampaigns() ? "1" : "0"),
                (user.isAllowDeleteIngroups() ? "1" : "0"),
                (user.isAllowDeleteRemoteAgents() ? "1" : "0"),
                (user.isAllowLoadLeads() ? "1" : "0"),
                (user.isAllowShowCampaignDetail() ? "1" : "0"),
                (user.isAstAdminAccess() ? "1" : "0"),
                (user.isAstDeletePhones() ? "1" : "0"),
                (user.isAllowDeleteScripts() ? "1" : "0"),
                (user.isAllowModifyLeads() ? "1" : "0"),
                (user.isHotKeyActive() ? "1" : "0"),
                (user.isAllowChangeAgentCampaign() ? "1" : "0"),
                (user.isAllowAgentChooseInGroups() ? "1" : "0"),
                user.getCloserCampaign(),
                (user.isAllowScheduledCallbacks() ? "1" : "0"),
                (user.isAllowAgentOnlyCallbacks() ? "1" : "0"),
                (user.isAllowAgentCallManual() ? "1" : "0"),
                (user.isOsdialRecording() ? "1" : "0"),
                (user.isOsdialTransfer() ? "1" : "0"),
                (user.isAllowDeleteFilters() ? "1" : "0"),
                (user.isAllowModifyAgentInterfaceOptions() ? "1" : "0"),
                (user.isCloserDefaultBlended() ? "1" : "0"),
                (user.isAllowDeleteCallTimes() ? "1" : "0"),
                (user.isAllowModifyCallTimes() ? "1" : "0"),
                (user.isAllowModifyUsers() ? "1" : "0"),
                (user.isAllowModifyCampaigns() ? "1" : "0"),
                (user.isAllowModifyLists() ? "1" : "0"),
                (user.isAllowModifyScripts() ? "1" : "0"),
                (user.isAllowModifyFilters() ? "1" : "0"),
                (user.isAllowModifyInGroups() ? "1" : "0"),
                (user.isAllowModifyUserGroups() ? "1" : "0"),
                (user.isAllowModifyRemoteAgents() ? "1" : "0"),
                (user.isAllowModifyServers() ? "1" : "0"),
                (user.isAllowViewReports() ? "1" : "0"),
                user.isAllowOsdialRecordingOverride(),
                user.isAllowAlterCustDataOverride(),
                user.getManualDialNewLimit(),
                (user.isAllowManualDialSkip() ? "1" : "0"),
                (user.isAllowExportLeads() ? "1" : "0"),
                (user.isAdminAPIAccess() ? "1" : "0"),
                (user.isAgentAPIAccess() ? "1" : "0"),
                (user.isAllowXferAgent2Agent() ? "1" : "0"),
                user.getScriptOverride(),
                (user.isAllowLoadDnc() ? "1" : "0"),
                (user.isAllowExportDnc() ? "1" : "0"),
                (user.isAllowDeleteDnc() ? "1" : "0"),
                user.getAccessCode(),
                (user.isAllowModifyPhones() ? "1" : "0"),
                (user.isAllowModifyTrunks() ? "1" : "0"),
                (user.isAllowModifyExtensions() ? "1" : "0"),
                (user.isAllowModifyStatuses() ? "1" : "0"),
                (user.isAllowModifyConferensions() ? "1" : "0"),
                (user.isPermAdministrationMenu() ? "1" : "0"),
                user.getPasswordMd5());
        Long insertedId = this.getByUserName(user.getUsername()).getId();
        user.setId(insertedId);
        return user;
    }

    @Override
    public User update(User user) throws PersistException {
        return (jdbcTemplate.update("UPDATE osdial_users SET user = ?, pass = ? , full_name = ?, user_level = ?," +
                        " user_group = ?, phone_login = ?, phone_pass = ?, delete_users = ?, delete_user_groups = ?," +
                        " delete_lists = ?, delete_campaigns = ?, delete_ingroups = ?, delete_remote_agents = ?, load_leads = ?," +
                        " campaign_detail = ?, ast_admin_access = ?, ast_delete_phones= ?, delete_scripts = ?, modify_leads = ?," +
                        " hotkeys_active = ?, change_agent_campaign = ?, agent_choose_ingroups = ?, closer_campaigns = ?," +
                        " scheduled_callbacks = ?, agentonly_callbacks = ?, agentcall_manual = ?, osdial_recording = ?," +
                        " osdial_transfers = ?, delete_filters = ?, alter_agent_interface_options = ?, closer_default_blended = ?," +
                        " delete_call_times = ?, modify_call_times = ?, modify_users = ?, modify_campaigns = ?, modify_lists = ?, modify_scripts = ?," +
                        " modify_filters = ?, modify_ingroups = ?, modify_usergroups = ?, modify_remoteagents = ?," +
                        " modify_servers = ?, view_reports = ?, osdial_recording_override = ?, alter_custdata_override = ?," +
                        " manual_dial_new_limit = ?, manual_dial_allow_skip = ?, export_leads = ?, admin_api_access = ?, " +
                        " agent_api_access = ?, xfer_agent2agent = ?, script_override = ?, load_dnc = ?, export_dnc = ?, delete_dnc = ?," +
                        " access_code = ?, access_timeout = ?, modify_phones = ?, modify_trunks = ?, modify_ext = ?," +
                        " modify_statuses = ?, modify_conferensions = ?, perm_administration_menu = ?" +
                        " WHERE user_id = ?",
                user.getUsername(),
                user.getPassword(),
                user.getFullName(),
                user.getUserLevel(),
                user.getUserGroup(),
                user.getPhoneLogin(),
                user.getPhonePassword(),
                (user.isAllowDeleteUsers() ? "1" : "0"),
                (user.isAllowDeleteUserGroups() ? "1" : "0"),
                (user.isAllowDeleteLists() ? "1" : "0"),
                (user.isAllowDeleteCampaigns() ? "1" : "0"),
                (user.isAllowDeleteIngroups() ? "1" : "0"),
                (user.isAllowDeleteRemoteAgents() ? "1" : "0"),
                (user.isAllowLoadLeads() ? "1" : "0"),
                (user.isAllowShowCampaignDetail() ? "1" : "0"),
                (user.isAstAdminAccess() ? "1" : "0"),
                (user.isAstDeletePhones() ? "1" : "0"),
                (user.isAllowDeleteScripts() ? "1" : "0"),
                (user.isAllowModifyLeads() ? "1" : "0"),
                (user.isHotKeyActive() ? "1" : "0"),
                (user.isAllowChangeAgentCampaign() ? "1" : "0"),
                (user.isAllowAgentChooseInGroups() ? "1" : "0"),
                user.getCloserCampaign(),
                (user.isAllowScheduledCallbacks() ? "1" : "0"),
                (user.isAllowAgentOnlyCallbacks() ? "1" : "0"),
                (user.isAllowAgentCallManual() ? "1" : "0"),
                (user.isOsdialRecording() ? "1" : "0"),
                (user.isOsdialTransfer() ? "1" : "0"),
                (user.isAllowDeleteFilters() ? "1" : "0"),
                (user.isAllowModifyAgentInterfaceOptions() ? "1" : "0"),
                (user.isCloserDefaultBlended() ? "1" : "0"),
                (user.isAllowDeleteCallTimes() ? "1" : "0"),
                (user.isAllowModifyCallTimes() ? "1" : "0"),
                (user.isAllowModifyUsers() ? "1" : "0"),
                (user.isAllowModifyCampaigns() ? "1" : "0"),
                (user.isAllowModifyLists() ? "1" : "0"),
                (user.isAllowModifyScripts() ? "1" : "0"),
                (user.isAllowModifyFilters() ? "1" : "0"),
                (user.isAllowModifyInGroups() ? "1" : "0"),
                (user.isAllowModifyUserGroups() ? "1" : "0"),
                (user.isAllowModifyRemoteAgents() ? "1" : "0"),
                (user.isAllowModifyServers() ? "1" : "0"),
                (user.isAllowViewReports() ? "1" : "0"),
                user.isAllowOsdialRecordingOverride(),
                user.isAllowAlterCustDataOverride(),
                user.getManualDialNewLimit(),
                (user.isAllowManualDialSkip() ? "1" : "0"),
                (user.isAllowExportLeads() ? "1" : "0"),
                (user.isAdminAPIAccess() ? "1" : "0"),
                (user.isAgentAPIAccess() ? "1" : "0"),
                (user.isAllowXferAgent2Agent() ? "1" : "0"),
                user.getScriptOverride(),
                (user.isAllowLoadDnc() ? "1" : "0"),
                (user.isAllowExportDnc() ? "1" : "0"),
                (user.isAllowDeleteDnc() ? "1" : "0"),
                user.getAccessCode(),
                Timestamp.valueOf(user.getTokenExpire()),
                (user.isAllowModifyPhones() ? "1" : "0"),
                (user.isAllowModifyTrunks() ? "1" : "0"),
                (user.isAllowModifyExtensions() ? "1" : "0"),
                (user.isAllowModifyStatuses() ? "1" : "0"),
                (user.isAllowModifyConferensions() ? "1" : "0"),
                (user.isPermAdministrationMenu() ? "1" : "0"),
                user.getId()) == 0) ? null : user;
    }

    @Override
    public Long delete(Long id) throws PersistException {
        return Long.valueOf(jdbcTemplate.update("DELETE FROM osdial_users WHERE user_id=?", id));
    }




    private class UserRowMapper implements RowMapper  {
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            User user = new User();
            user.setId(rs.getLong("user_id"));
            user.setUsername(rs.getString("user"));
            user.setPasswordMd5(rs.getString("password_md5"));
            user.setFullName(rs.getString("full_name"));
            user.setUserLevel(rs.getInt("user_level"));
            user.setUserGroup(rs.getString("user_group"));
            user.setPhoneLogin(rs.getString("phone_login"));
            user.setPhonePassword(rs.getString("phone_pass"));
            user.setAllowDeleteUsers(rs.getBoolean("delete_users"));
            user.setAllowDeleteUserGroups(rs.getBoolean("delete_user_groups"));
            user.setAllowDeleteLists(rs.getBoolean("delete_lists"));
            user.setAllowDeleteCampaigns(rs.getBoolean("delete_campaigns"));
            user.setAllowDeleteIngroups(rs.getBoolean("delete_ingroups"));
            user.setAllowDeleteRemoteAgents(rs.getBoolean("delete_remote_agents"));
            user.setAllowLoadLeads(rs.getBoolean("load_leads"));
            user.setAllowShowCampaignDetail(rs.getBoolean("campaign_detail"));
            user.setAstAdminAccess(rs.getBoolean("ast_admin_access"));
            user.setAstDeletePhones(rs.getBoolean("ast_delete_phones"));
            user.setAllowDeleteScripts(rs.getBoolean("delete_scripts"));
            user.setAllowModifyLeads(rs.getBoolean("modify_leads"));
            user.setHotKeyActive(rs.getBoolean("hotkeys_active"));
            user.setAllowChangeAgentCampaign(rs.getBoolean("change_agent_campaign"));
            user.setCloserCampaign(rs.getString("closer_campaigns"));
            user.setAllowScheduledCallbacks(rs.getBoolean("scheduled_callbacks"));
            user.setAllowAgentOnlyCallbacks(rs.getBoolean("agentonly_callbacks"));
            user.setAllowAgentCallManual(rs.getBoolean("agentcall_manual"));
            user.setOsdialRecording(rs.getBoolean("osdial_recording"));
            user.setOsdialTransfer(rs.getBoolean("osdial_transfers"));
            user.setAllowDeleteFilters(rs.getBoolean("delete_filters"));
            user.setAllowModifyAgentInterfaceOptions(rs.getBoolean("alter_agent_interface_options"));
            user.setCloserDefaultBlended(rs.getBoolean("closer_default_blended"));
            user.setAllowDeleteCallTimes(rs.getBoolean("delete_call_times"));
            user.setAllowModifyCallTimes(rs.getBoolean("modify_call_times"));
            user.setAllowModifyUsers(rs.getBoolean("modify_users"));
            user.setAllowModifyCampaigns(rs.getBoolean("modify_campaigns"));
            user.setAllowModifyLists(rs.getBoolean("modify_lists"));
            user.setAllowModifyScripts(rs.getBoolean("modify_scripts"));
            user.setAllowModifyFilters(rs.getBoolean("modify_filters"));
            user.setAllowModifyInGroups(rs.getBoolean("modify_ingroups"));
            user.setAllowModifyUserGroups(rs.getBoolean("modify_usergroups"));
            user.setAllowModifyRemoteAgents(rs.getBoolean("modify_remoteagents"));
            user.setAllowModifyServers(rs.getBoolean("modify_servers"));
            user.setAllowOsdialRecordingOverride(rs.getString("osdial_recording_override"));
            user.setAllowAlterCustDataOverride(rs.getString("alter_custdata_override"));
            user.setManualDialNewLimit(rs.getInt("manual_dial_new_limit"));
            user.setAllowManualDialSkip(rs.getBoolean("manual_dial_allow_skip"));
            user.setAllowExportLeads(rs.getBoolean("export_leads"));
            user.setAdminAPIAccess(rs.getBoolean("admin_api_access"));
            user.setAgentAPIAccess(rs.getBoolean("agent_api_access"));
            user.setAllowXferAgent2Agent(rs.getBoolean("xfer_agent2agent"));
            user.setScriptOverride(rs.getString("script_override"));
            user.setAllowLoadDnc(rs.getBoolean("load_dnc"));
            user.setAllowDeleteDnc(rs.getBoolean("delete_dnc"));
            user.setAccessCode(rs.getString("access_code"));

            user.setAllowViewReports(rs.getBoolean("view_reports"));

            Timestamp accessTimeout = rs.getTimestamp("access_timeout");
            if(accessTimeout != null) {
                user.setTokenExpire(accessTimeout.toLocalDateTime());
            }

            user.setAllowModifyPhones(rs.getBoolean("modify_phones"));
            user.setAllowModifyTrunks(rs.getBoolean("modify_trunks"));
            user.setAllowModifyExtensions(rs.getBoolean("modify_ext"));
            user.setAllowModifyStatuses(rs.getBoolean("modify_statuses"));
            user.setAllowModifyConferensions(rs.getBoolean("modify_conferensions"));
            user.setPermAdministrationMenu(rs.getBoolean("perm_administration_menu"));
            return user;
        }

    }


}
