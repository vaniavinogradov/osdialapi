package ua.iptel.java.osdialapi.dao.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ua.iptel.java.osdialapi.dao.CustomDao;
import ua.iptel.java.osdialapi.dao.exception.PersistException;
import ua.iptel.java.osdialapi.model.dto.CustomNumberResponse;

@Repository
public class JdbcCustomDaoImpl implements CustomDao {
  @Autowired
  JdbcTemplate jdbcTemplate;

  @Override
  public CustomNumberResponse getAgentCurrentConnectedPhoneNumber(Long agentInternalPhoneNumber)
      throws PersistException {

    List<CustomNumberResponse> customNumberResponses =
        jdbcTemplate.query("select osdial_list.phone_number from osdial_live_agents "
                + "LEFT JOIN osdial_list ON osdial_live_agents.lead_id = osdial_list.lead_id "
                + "WHERE osdial_live_agents.user = ? LIMIT 1",
            new CustomResponseRowMapper() , agentInternalPhoneNumber );
    return DataAccessUtils.singleResult(customNumberResponses);
  }

  private class CustomResponseRowMapper implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      CustomNumberResponse customNumberResponse = new CustomNumberResponse();
      customNumberResponse.setPhoneNumber(rs.getString("phone_number"));
      customNumberResponse.setStatusCode("OK");
      return  customNumberResponse;
    }

  }
}
