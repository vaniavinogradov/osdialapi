package ua.iptel.java.osdialapi.dao.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ua.iptel.java.osdialapi.dao.exception.PersistException;
import ua.iptel.java.osdialapi.dao.osdial.IvrDao;
import ua.iptel.java.osdialapi.model.Ivr;
import ua.iptel.java.osdialapi.model.IvrOption;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public class JdbcIvrDaoImpl implements IvrDao {
    @Autowired
    @Qualifier("jdbcTemplateOsdialDb")
    private JdbcTemplate jdbcTemplate;

    @Override
    public Ivr getByPK(Long id) throws PersistException {
        List<Ivr> ivrList = jdbcTemplate.query("SELECT * FROM osdial_ivr WHERE id = ?", new IvrRowMapper() , id);
        return DataAccessUtils.singleResult(ivrList);
    }

    @Override
    public List<Ivr> getAll() throws PersistException {
        return jdbcTemplate.query("SELECT * FROM osdial_ivr", new IvrRowMapper());
    }

    @Override
    public Ivr create(Ivr ivr) throws PersistException {
        PreparedStatementCreator psc = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(final Connection connection) throws SQLException {
                final PreparedStatement ps = connection.prepareStatement("INSERT INTO " +
                                "osdial_ivr(name, announcement, repeat_loops, wait_loops, wait_timeout, " +
                                "answered_status, virtual_agents, status, timeout_action, reserve_agents," +
                                " allow_inbound, allow_agent_extensions)" +
                                " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                        Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, ivr.getName());
                ps.setString(2, ivr.getAnnouncement());
                ps.setInt(3, ivr.getRepeatLoops());
                ps.setInt(4, ivr.getWaitLoops());
                ps.setInt(5, ivr.getWaitTimeout());
                ps.setString(6, ivr.getAnsweredStatus());
                ps.setInt(7, ivr.getVirtualAgents());
                ps.setString(8, ivr.getStatus());
                ps.setString(9, ivr.getTimeoutAction());
                ps.setInt(10, ivr.getReserveAgents());
                ps.setString(11, convertBooleanToEnumString(ivr.isAllowInbound()));
                ps.setString(12, convertBooleanToEnumString(ivr.isAllowAgentExtensions()));
                return ps;
            }
        };
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(psc, keyHolder);

        Integer newId;
        if (keyHolder.getKeys().size() > 1) {
            newId = (Integer)keyHolder.getKeys().get("id");
        } else {
            newId= keyHolder.getKey().intValue();
        }
        ivr.setId(Long.valueOf(newId));
        return ivr;
    }

    @Override
    public Ivr update(Ivr ivr) throws PersistException {
        return (jdbcTemplate.update("UPDATE osdial_ivr SET name = ?, announcement = ?, repeat_loops = ?," +
                        "wait_loops = ?, wait_timeout = ?, answered_status = ?, virtual_agents = ?, status = ?," +
                        "timeout_action = ?, reserve_agents = ?, allow_inbound = ?, allow_agent_extensions = ? WHERE id = ?",
                ivr.getName(),
                ivr.getAnnouncement(),
                ivr.getRepeatLoops(),
                ivr.getWaitLoops(),
                ivr.getWaitTimeout(),
                ivr.getAnsweredStatus(),
                ivr.getVirtualAgents(),
                ivr.getStatus(),
                ivr.getTimeoutAction(),
                ivr.getReserveAgents(),
                convertBooleanToEnumString(ivr.isAllowInbound()),
                convertBooleanToEnumString(ivr.isAllowAgentExtensions()),
                ivr.getId()) == 0) ? null : ivr;
    }

    @Override
    public Long delete(Long id) throws PersistException {
        return Long.valueOf(jdbcTemplate.update("DELETE FROM osdial_ivr WHERE id=?", id));
    }




    @Override
    public List<IvrOption> getAllIvrOptions() {
        return jdbcTemplate.query("SELECT * FROM osdial_ivr_options", new IvrOptionRowMapper());
    }

    @Override
    public IvrOption getIvrOptionByPk(Long id) {
        List<IvrOption> ivrOptionsList = jdbcTemplate.query("SELECT * FROM osdial_ivr_options WHERE id = ?", new IvrOptionRowMapper() , id);
        return DataAccessUtils.singleResult(ivrOptionsList);
    }

    @Override
    public IvrOption createIvrOption(IvrOption ivrOption) {
        PreparedStatementCreator psc = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(final Connection connection) throws SQLException {
                final PreparedStatement ps = connection.prepareStatement("INSERT INTO " +
                                "osdial_ivr_options(ivr_id, parent_id, keypress, action, action_data, last_state)" +
                                " VALUES (?, ?, ?, ?, ?, ?)",
                        Statement.RETURN_GENERATED_KEYS);
                ps.setLong(1, ivrOption.getIvrId());
                ps.setLong(2, ivrOption.getParentId());
                ps.setString(3, ivrOption.getKeyPress());
                ps.setString(4, ivrOption.getAction());
                ps.setString(5, ivrOption.getActionData());
                ps.setString(6, ivrOption.getLastState());
                return ps;
            }
        };
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(psc, keyHolder);

        Integer newId;
        if (keyHolder.getKeys().size() > 1) {
            newId = (Integer)keyHolder.getKeys().get("id");
        } else {
            newId= keyHolder.getKey().intValue();
        }
        ivrOption.setId(Long.valueOf(newId));
        return ivrOption;
    }

    @Override
    public IvrOption updateIvrOption(IvrOption ivrOption) {
        return (jdbcTemplate.update("UPDATE osdial_ivr_options SET ivr_id = ?, parent_id = ?," +
                        " keypress = ?, action = ?, action_data = ?, last_state = ? WHERE id = ?",
                ivrOption.getIvrId(),
                ivrOption.getParentId(),
                ivrOption.getKeyPress(),
                ivrOption.getAction(),
                ivrOption.getActionData(),
                ivrOption.getLastState(),
                ivrOption.getId()) == 0) ? null : ivrOption;
    }

    @Override
    public Long deleteIvrOption(Long id) {
        return Long.valueOf(jdbcTemplate.update("DELETE FROM osdial_ivr_options WHERE id=?", id));
    }

    private class IvrRowMapper implements RowMapper {
        @Override
        public Object mapRow(ResultSet rs, int i) throws SQLException {
            Ivr ivr = new Ivr();
            ivr.setId(rs.getLong("id"));
            ivr.setName(rs.getString("name"));
            ivr.setAnnouncement(rs.getString("announcement"));
            ivr.setRepeatLoops(rs.getInt("repeat_loops"));
            ivr.setWaitLoops(rs.getInt("wait_loops"));
            ivr.setWaitTimeout(rs.getInt("wait_timeout"));
            ivr.setAnsweredStatus(rs.getString("answered_status"));
            ivr.setVirtualAgents(rs.getInt("virtual_agents"));
            ivr.setStatus(rs.getString("status"));
            ivr.setTimeoutAction(rs.getString("timeout_action"));
            ivr.setReserveAgents(rs.getInt("reserve_agents"));
            ivr.setAllowInbound(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("allow_inbound")));
            ivr.setAllowAgentExtensions(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("allow_agent_extensions")));
            return ivr;
        }
    }

    private class IvrOptionRowMapper implements RowMapper {
        @Override
        public Object mapRow(ResultSet rs, int i) throws SQLException {
            IvrOption ivrOption = new IvrOption();
            ivrOption.setId(rs.getLong("id"));
            ivrOption.setIvrId(rs.getLong("ivr_id"));
            ivrOption.setParentId(rs.getLong("parent_id"));
            ivrOption.setKeyPress(rs.getString("keypress"));
            ivrOption.setAction(rs.getString("action"));
            ivrOption.setActionData(rs.getString("action_data"));
            ivrOption.setLastState(rs.getString("last_state"));
            return ivrOption;
        }
    }

    private boolean convertEnumValueFromMysqlOsdialDbToBoolean(String enumValue) {
        if (enumValue == null){
            return false;
        }else if(enumValue.equals("N")) {
            return false;
        } else {
            return true;
        }
    }

    private String convertBooleanToEnumString(Boolean value){
        if(value) {
            return "Y";
        }else {
            return "N";
        }
    }
}
