package ua.iptel.java.osdialapi.dao.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ua.iptel.java.osdialapi.dao.exception.PersistException;
import ua.iptel.java.osdialapi.dao.osdial.AdditionalFormDao;
import ua.iptel.java.osdialapi.dao.osdial.CampaignDao;
import ua.iptel.java.osdialapi.model.AdditionalForm;
import ua.iptel.java.osdialapi.model.Campaign;

import java.sql.*;
import java.util.List;

@Repository
public class JdbcAdditionalFormDaoImpl implements AdditionalFormDao {

    @Autowired
    @Qualifier("jdbcTemplateOsdialDb")
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private CampaignDao campaignDao;

    @Override
    public AdditionalForm getByPK(Long id) throws PersistException {
        List<AdditionalForm> additionalForms = jdbcTemplate.query("SELECT * FROM osdial_campaign_forms WHERE id = ?", new AdditionalFormRowMapper() , id);
        return DataAccessUtils.singleResult(additionalForms);
    }

    @Override
    public List<AdditionalForm> getAll() throws PersistException {
        return jdbcTemplate.query("SELECT * FROM osdial_campaign_forms", new AdditionalFormRowMapper());
    }

    @Override
    public AdditionalForm create(AdditionalForm additionalForm) throws PersistException {
        PreparedStatementCreator psc = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(final Connection connection) throws SQLException {
                final PreparedStatement ps = connection.prepareStatement(
                        "INSERT INTO osdial_campaign_forms(campaigns, name, description, description2, priority, deleted)" +
                                " VALUES (?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, convertArrayOfLongsToStringDb(additionalForm.getCampaignsIds()));
                ps.setString(2, additionalForm.getName());
                if(additionalForm.getDescription() != null){
                    ps.setString(3, additionalForm.getDescription());
                }else {
                    ps.setString(3, "");
                }
                if(additionalForm.getDescription2() != null){
                    ps.setString(4, additionalForm.getDescription2());
                }else {
                    ps.setString(4, "");
                }

                ps.setInt(5, additionalForm.getPriority());
                ps.setInt(6, additionalForm.getDeleted());
                return ps;
            }
        };
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(psc, keyHolder);

        Integer newId;
        if (keyHolder.getKeys().size() > 1) {
            newId = (Integer)keyHolder.getKeys().get("id");
        } else {
            newId= keyHolder.getKey().intValue();
        }
        additionalForm.setId(Long.valueOf(newId));
        return additionalForm;
    }

    @Override
    public AdditionalForm update(AdditionalForm additionalForm) throws PersistException {
        if(additionalForm.getDescription() == null){
            additionalForm.setDescription("");
        }
        if(additionalForm.getDescription2() == null){
            additionalForm.setDescription2("");
        }
        return (jdbcTemplate.update("UPDATE osdial_campaign_forms " +
                        "SET campaigns = ?, name = ?, description = ?, description2 =?, priority = ?, deleted = ?" +
                        " WHERE id = ?",

                convertArrayOfLongsToStringDb(additionalForm.getCampaignsIds()),
                additionalForm.getName(),
                additionalForm.getDescription(),
                additionalForm.getDescription2(),
                additionalForm.getPriority(),
                additionalForm.getDeleted(),
                additionalForm.getId()) == 0) ? null : additionalForm;
    }

    @Override
    public Long delete(Long id) throws PersistException {
        return Long.valueOf(jdbcTemplate.update("DELETE FROM osdial_campaign_forms WHERE id=?", id));
    }

    private class AdditionalFormRowMapper implements RowMapper {
        @Override
        public Object mapRow(ResultSet rs, int i) throws SQLException {
            AdditionalForm additionalForm = new AdditionalForm();
            additionalForm.setId(rs.getLong("id"));
            additionalForm.setCampaignsIds(parseCampaignsIdsStringToLongArray(rs.getString("campaigns")));
            additionalForm.setName(rs.getString("name"));
            additionalForm.setDescription(rs.getString("description"));
            additionalForm.setDescription2(rs.getString("description2"));
            additionalForm.setPriority(rs.getInt("priority"));
            additionalForm.setDeleted(rs.getInt("deleted"));
            return additionalForm;
        }
    }

    private Long[] parseCampaignsIdsStringToLongArray(String dbString){
        Long[] campaignsIds = {};
        if(dbString.equals("-ALL-")){
            List<Campaign> campaigns = campaignDao.getShort();
            campaignsIds = new Long[campaigns.size()];
            for (int i = 0; i < campaigns.size(); i++) {
                campaignsIds[i] = campaigns.get(i).getId();
            }
        }else {
            String[] split = dbString.split(",");
            for (int i = 0; i < split.length; i++) {
                campaignsIds[i] = Long.valueOf(split[i]);
            }
        }

        return campaignsIds;
    }

    private String convertArrayOfLongsToStringDb(Long[] longsArray){
        StringBuilder arrayConvertedToStringDbFormat = new StringBuilder();
        for (int i = 0; i < longsArray.length; i++) {
            if(i != (longsArray.length - 1)){
                arrayConvertedToStringDbFormat.append(longsArray[i]);
                arrayConvertedToStringDbFormat.append(",");
            }else {
                arrayConvertedToStringDbFormat.append(longsArray[i]);
            }
        }
        return arrayConvertedToStringDbFormat.toString();
    }
}
