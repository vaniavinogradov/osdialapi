package ua.iptel.java.osdialapi.dao.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ua.iptel.java.osdialapi.dao.exception.PersistException;
import ua.iptel.java.osdialapi.dao.osdial.CallTimeDao;
import ua.iptel.java.osdialapi.model.CallTime;

import java.sql.*;
import java.util.List;

@Repository
public class JdbcCallTimeDaoImpl implements CallTimeDao {
    @Autowired
    @Qualifier("jdbcTemplateOsdialDb")
    private JdbcTemplate jdbcTemplate;

    @Override
    public CallTime getByPK(Long id) throws PersistException {
        List<CallTime> callTimeList = jdbcTemplate.query("SELECT * FROM osdial_call_times WHERE call_time_id = ?", new CallTimeRowMapper() , id);
        return DataAccessUtils.singleResult(callTimeList);
    }

    @Override
    public List<CallTime> getAll() throws PersistException {
        return jdbcTemplate.query("SELECT * FROM osdial_call_times", new CallTimeRowMapper());
    }

    @Override
    public CallTime create(CallTime callTime) throws PersistException {
        PreparedStatementCreator psc = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(final Connection connection) throws SQLException {
                final PreparedStatement ps = connection.prepareStatement("INSERT INTO " +
                                "osdial_call_times(call_time_name, call_time_comments, ct_default_start," +
                                " ct_default_stop, ct_sunday_start, ct_sunday_stop, ct_monday_start, ct_monday_stop," +
                                " ct_tuesday_start, ct_tuesday_stop, ct_wednesday_start, ct_wednesday_stop," +
                                " ct_thursday_start, ct_thursday_stop, ct_friday_start, ct_friday_stop, ct_saturday_start," +
                                " ct_saturday_stop, ct_state_call_times, use_recycle_gap)" +
                                " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                        Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, callTime.getName());
                ps.setString(2, callTime.getComments());
                ps.setInt(3, callTime.getCtDefaultStart());
                ps.setInt(4, callTime.getCtDefaultStop());
                ps.setInt(5, callTime.getCtSundayStart());
                ps.setInt(6, callTime.getCtSundayStop());
                ps.setInt(7, callTime.getCtMondayStart());
                ps.setInt(8, callTime.getCtMondayStop());
                ps.setInt(9, callTime.getCtTuesdayStart());
                ps.setInt(10, callTime.getCtTuesdayStop());
                ps.setInt(11, callTime.getCtWednesdayStart());
                ps.setInt(12, callTime.getCtWednesdayStop());
                ps.setInt(13, callTime.getCtThursdayStart());
                ps.setInt(14, callTime.getCtTuesdayStop());
                ps.setInt(15, callTime.getCtFridayStart());
                ps.setInt(16, callTime.getCtFridayStop());
                ps.setInt(17, callTime.getCtSaturdayStart());
                ps.setInt(18, callTime.getCtSaturdayStop());
                ps.setString(19, callTime.getCtStateCallTimes());
                ps.setString(20, convertBooleanToEnumString(callTime.isUseRecycleGap()));
                return ps;
            }
        };
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(psc, keyHolder);

        Integer newId;
        if (keyHolder.getKeys().size() > 1) {
            newId = (Integer)keyHolder.getKeys().get("call_time_id");
        } else {
            newId= keyHolder.getKey().intValue();
        }
        callTime.setId(Long.valueOf(newId));
        return callTime;
    }

    @Override
    public CallTime update(CallTime callTime) throws PersistException {
        return (jdbcTemplate.update("UPDATE osdial_call_times SET call_time_name = ?, call_time_comments = ?," +
                        " ct_default_start = ?, ct_default_stop = ?, ct_sunday_start = ?, ct_sunday_stop = ?, " +
                        " ct_monday_start = ?, ct_monday_stop = ?, ct_tuesday_start = ?, ct_tuesday_stop = ?, " +
                        " ct_wednesday_start = ?, ct_wednesday_stop = ?, ct_thursday_start = ?, ct_tuesday_stop = ?," +
                        " ct_friday_start = ?, ct_friday_stop = ?, ct_saturday_start = ?, ct_saturday_stop = ?," +
                        " ct_state_call_times = ?, use_recycle_gap = ?" +
                        " WHERE call_time_id = ?",
                callTime.getName(),
                callTime.getComments(),
                callTime.getCtDefaultStart(),
                callTime.getCtDefaultStop(),
                callTime.getCtSundayStart(),
                callTime.getCtSundayStop(),
                callTime.getCtMondayStart(),
                callTime.getCtMondayStop(),
                callTime.getCtTuesdayStart(),
                callTime.getCtTuesdayStop(),
                callTime.getCtWednesdayStart(),
                callTime.getCtWednesdayStop(),
                callTime.getCtThursdayStart(),
                callTime.getCtThursdayStop(),
                callTime.getCtFridayStart(),
                callTime.getCtFridayStop(),
                callTime.getCtSaturdayStart(),
                callTime.getCtSundayStop(),
                callTime.getCtStateCallTimes(),
                convertBooleanToEnumString(callTime.isUseRecycleGap()),
                callTime.getId()) == 0) ? null : callTime;
    }

    @Override
    public Long delete(Long id) throws PersistException {
        return Long.valueOf(jdbcTemplate.update("DELETE FROM osdial_call_times WHERE call_time_id = ?", id));
    }

    private class CallTimeRowMapper implements RowMapper {
        @Override
        public Object mapRow(ResultSet rs, int i) throws SQLException {
            CallTime callTime = new CallTime();
            callTime.setId(rs.getLong("call_time_id"));
            callTime.setName(rs.getString("call_time_name"));
            callTime.setComments(rs.getString("call_time_comments"));
            callTime.setCtDefaultStart(rs.getInt("ct_default_start"));
            callTime.setCtDefaultStop(rs.getInt("ct_default_stop"));
            callTime.setCtSundayStart(rs.getInt("ct_sunday_start"));
            callTime.setCtSundayStop(rs.getInt("ct_sunday_stop"));
            callTime.setCtMondayStart(rs.getInt("ct_monday_start"));
            callTime.setCtMondayStop(rs.getInt("ct_monday_stop"));
            callTime.setCtTuesdayStart(rs.getInt("ct_tuesday_start"));
            callTime.setCtTuesdayStop(rs.getInt("ct_tuesday_stop"));
            callTime.setCtWednesdayStart(rs.getInt("ct_wednesday_start"));
            callTime.setCtWednesdayStop(rs.getInt("ct_wednesday_stop"));
            callTime.setCtThursdayStart(rs.getInt("ct_thursday_start"));
            callTime.setCtTuesdayStop(rs.getInt("ct_thursday_stop"));
            callTime.setCtFridayStart(rs.getInt("ct_friday_start"));
            callTime.setCtFridayStop(rs.getInt("ct_friday_stop"));
            callTime.setCtSaturdayStart(rs.getInt("ct_saturday_start"));
            callTime.setCtSaturdayStop(rs.getInt("ct_saturday_stop"));
            callTime.setUseRecycleGap(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("use_recycle_gap")));
            return callTime;
        }
    }

    private boolean convertEnumValueFromMysqlOsdialDbToBoolean(String enumValue) {
        if (enumValue == null){
            return false;
        }else if(enumValue.equals("N")) {
            return false;
        } else {
            return true;
        }
    }

    private String convertBooleanToEnumString(Boolean value){
        if(value) {
            return "Y";
        }else {
            return "N";
        }
    }
}
