package ua.iptel.java.osdialapi.dao.osdial;

import ua.iptel.java.osdialapi.dao.AbstractDao;
import ua.iptel.java.osdialapi.model.Ivr;
import ua.iptel.java.osdialapi.model.IvrOption;

import java.util.List;

public interface IvrDao extends AbstractDao<Ivr> {
    List<IvrOption> getAllIvrOptions();
    IvrOption getIvrOptionByPk(Long id);
    IvrOption createIvrOption(IvrOption ivrOption);
    IvrOption updateIvrOption(IvrOption ivrOption);
    Long deleteIvrOption(Long id);
}
