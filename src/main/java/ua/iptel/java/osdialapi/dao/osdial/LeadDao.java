package ua.iptel.java.osdialapi.dao.osdial;

import ua.iptel.java.osdialapi.dao.AbstractDao;
import ua.iptel.java.osdialapi.model.Lead;
import ua.iptel.java.osdialapi.model.dto.LeadSimpleDto;

import java.util.List;

public interface LeadDao extends AbstractDao<Lead> {
    List<Lead> getByListId(Long listId);
    List<Lead> getByCampaignId(Long campaignId);
    List<Lead> getByStatuses(List<String> statuses);
    List<Lead> insertBatchLeads(List<Lead> leads);
    Lead getByExternalKey(Long externalKey);
    Integer addOnlyUniqueueLeads(List<LeadSimpleDto> leads);
    void deleteLeadsFromPoolByListId(Long listId);
    void deleteLeadsFromPoolByCampaignId(Long campaignId);


}
