package ua.iptel.java.osdialapi.dao.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ua.iptel.java.osdialapi.dao.exception.PersistException;
import ua.iptel.java.osdialapi.dao.osdial.UserGroupDao;
import ua.iptel.java.osdialapi.model.UserGroup;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public class JdbcUserGroupDaoImpl implements UserGroupDao {
    @Autowired
    @Qualifier("jdbcTemplateOsdialDb")
    private JdbcTemplate jdbcTemplate;

    @Override
    public UserGroup getByPK(Long id) throws PersistException {
        List<UserGroup> userGroupList = jdbcTemplate.query("SELECT * FROM osdial_user_groups WHERE id = ?", new UserGroupRowMapper() , id);
        return DataAccessUtils.singleResult(userGroupList);
    }

    @Override
    public UserGroup getByName(String userGroupName) {
        List<UserGroup> userGroupList = jdbcTemplate.query("SELECT * FROM osdial_user_groups WHERE user_group = ?",
                new UserGroupRowMapper() , userGroupName);
        return DataAccessUtils.singleResult(userGroupList);
    }

    @Override
    public List<UserGroup> getAll() throws PersistException {
        return jdbcTemplate.query("SELECT * FROM osdial_user_groups", new UserGroupRowMapper());
    }

    @Override
    public UserGroup create(UserGroup userGroup) throws PersistException {
        PreparedStatementCreator psc = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(final Connection connection) throws SQLException {
                final PreparedStatement ps = connection.prepareStatement("INSERT INTO " +
                                "osdial_user_groups(group_name, allowed_campaigns, view_agent_pause_summary," +
                                " export_agent_pause_summary, view_agent_performance_detail, export_agent_performance_detail," +
                                " view_agent_realtime, view_agent_realtime_iax_barge, view_agent_realtime_iax_listen," +
                                " view_agent_realtime_sip_barge, view_agent_realtime_sip_listen, view_agent_realtime_summary," +
                                " view_agent_stats, view_agent_status, view_agent_timesheet, export_agent_timesheet," +
                                " view_campaign_call_report, export_campaign_call_report, view_campaign_recent_outbound_sales," +
                                " export_campaign_recent_outbound_sales, view_ingroup_call_report, export_ingroup_call_report," +
                                " view_lead_performance_campaign, export_lead_performance_campaign, view_lead_performance_list," +
                                " export_lead_performance_list, view_lead_search, view_lead_search_advanced, export_lead_search_advanced," +
                                " view_list_cost_entry, export_list_cost_entry, view_server_performance, view_server_times," +
                                " view_usergroup_hourly_stats, allowed_scripts, allowed_email_templates, allowed_ingroups, agent_message)" +
                                " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
                                         "?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
                                         "?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
                                         "?, ?, ?, ?, ?, ?, ?, ?)",
                        Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, userGroup.getName());
                ps.setString(2, userGroup.getAllowedCampaigns());
                ps.setString(3, convertBooleanToEnumString(userGroup.isAllowViewAgentPauseSummary()));
                ps.setString(4, convertBooleanToEnumString(userGroup.isAllowExportAgentPauseSummary()));
                ps.setString(5, convertBooleanToEnumString(userGroup.isAllowViewAgentPerformanceDetail()));
                ps.setString(6, convertBooleanToEnumString(userGroup.isAllowExportAgentPerformanceDetail()));
                ps.setString(7, convertBooleanToEnumString(userGroup.isAllowViewAgentRealTime()));
                ps.setString(8, convertBooleanToEnumString(userGroup.isAllowViewAgentRealTimeIaxBarge()));
                ps.setString(9, convertBooleanToEnumString(userGroup.isAllowViewAgentRealTimeIaxListen()));
                ps.setString(10, convertBooleanToEnumString(userGroup.isAllowViewAgentRealTimeSipBarge()));
                ps.setString(11, convertBooleanToEnumString(userGroup.isAllowViewAgentRealTimeSipListen()));
                ps.setString(12, convertBooleanToEnumString(userGroup.isAllowViewAgentRealTimeSummary()));
                ps.setString(13, convertBooleanToEnumString(userGroup.isAllowViewAgentStats()));
                ps.setString(14, convertBooleanToEnumString(userGroup.isAllowViewAgentStatus()));
                ps.setString(15, convertBooleanToEnumString(userGroup.isAllowViewAgentTimeSheet()));
                ps.setString(16, convertBooleanToEnumString(userGroup.isAllowExportAgentTimeSheet()));
                ps.setString(17, convertBooleanToEnumString(userGroup.isAllowViewCampaignCallReport()));
                ps.setString(18, convertBooleanToEnumString(userGroup.isAllowExportCampaignCallReport()));
                ps.setString(19, convertBooleanToEnumString(userGroup.isAllowViewCampaignRecentOutboundSales()));
                ps.setString(20, convertBooleanToEnumString(userGroup.isAllowExportCampaignRecentOutboundSales()));
                ps.setString(21, convertBooleanToEnumString(userGroup.isAllowViewIngroupCallReport()));
                ps.setString(22, convertBooleanToEnumString(userGroup.isAllowExportIngroupCallReport()));
                ps.setString(23, convertBooleanToEnumString(userGroup.isAllowViewLeadPerformanceCampaign()));
                ps.setString(24, convertBooleanToEnumString(userGroup.isAllowExportLeadPerformanceCampaign()));
                ps.setString(25, convertBooleanToEnumString(userGroup.isAllowViewLeadPerformanceList()));
                ps.setString(26, convertBooleanToEnumString(userGroup.isAllowExportLeadPerformanceList()));
                ps.setString(27, convertBooleanToEnumString(userGroup.isAllowViewLeadSearch()));
                ps.setString(28, convertBooleanToEnumString(userGroup.isAllowViewLeadSearchAdvanced()));
                ps.setString(29, convertBooleanToEnumString(userGroup.isAllowExportLeadSearchAdvanced()));
                ps.setString(30, convertBooleanToEnumString(userGroup.isAllowViewListCostEntry()));
                ps.setString(31, convertBooleanToEnumString(userGroup.isAllowExportListCostEntry()));
                ps.setString(32, convertBooleanToEnumString(userGroup.isAllowViewServerPerformance()));
                ps.setString(33, convertBooleanToEnumString(userGroup.isAllowViewServerTimes()));
                ps.setString(34, convertBooleanToEnumString(userGroup.isAllowViewUserGroupHourlyStats()));
                ps.setString(35, userGroup.getAllowedScripts());
                ps.setString(36, userGroup.getAllowedEmailTemplates());
                ps.setString(37, userGroup.getAllowedIngroups());
                ps.setString(38, userGroup.getAgentMessage());
                return ps;
            }
        };
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(psc, keyHolder);

        Integer newId;
        if (keyHolder.getKeys().size() > 1) {
            newId = (Integer)keyHolder.getKeys().get("id");
        } else {
            newId= keyHolder.getKey().intValue();
        }
        userGroup.setId(Long.valueOf(newId));
        return userGroup;
    }

    @Override
    public UserGroup update(UserGroup userGroup) throws PersistException {
        return (jdbcTemplate.update("UPDATE osdial_user_groups SET group_name = ?," +
                        " allowed_campaigns  = ?, view_agent_pause_summary = ?, export_agent_pause_summary = ?," +
                        " view_agent_performance_detail = ?, export_agent_performance_detail= ?, view_agent_realtime = ?," +
                        " view_agent_realtime_iax_barge  = ?, view_agent_realtime_iax_listen  = ?, view_agent_realtime_sip_barge = ?," +
                        " view_agent_realtime_sip_listen = ?, view_agent_realtime_summary = ?, view_agent_stats = ?," +
                        " view_agent_status = ?, view_agent_timesheet = ?, export_agent_timesheet = ?, view_campaign_call_report = ?," +
                        " export_campaign_call_report = ?, view_campaign_recent_outbound_sales = ?, export_campaign_recent_outbound_sales = ?," +
                        " view_ingroup_call_report = ?, export_ingroup_call_report = ?, view_lead_performance_campaign = ?," +
                        " export_lead_performance_campaign = ?, view_lead_performance_list = ?, export_lead_performance_list = ?," +
                        " view_lead_search = ?, view_lead_search_advanced = ?, export_lead_search_advanced = ?, view_list_cost_entry = ?," +
                        " export_list_cost_entry = ?, view_server_performance = ?, view_server_times = ?, view_usergroup_hourly_stats = ?," +
                        " allowed_scripts = ?, allowed_email_templates = ?, allowed_ingroups = ?, agent_message = ? WHERE id = ?",
                userGroup.getName(),
                userGroup.getAllowedCampaigns(),
                convertBooleanToEnumString(userGroup.isAllowViewAgentPauseSummary()),
                convertBooleanToEnumString(userGroup.isAllowExportAgentPauseSummary()),
                convertBooleanToEnumString(userGroup.isAllowViewAgentPerformanceDetail()),
                convertBooleanToEnumString(userGroup.isAllowExportAgentPerformanceDetail()),
                convertBooleanToEnumString(userGroup.isAllowViewAgentRealTime()),
                convertBooleanToEnumString(userGroup.isAllowViewAgentRealTimeIaxBarge()),
                convertBooleanToEnumString(userGroup.isAllowViewAgentRealTimeIaxListen()),
                convertBooleanToEnumString(userGroup.isAllowViewAgentRealTimeSipBarge()),
                convertBooleanToEnumString(userGroup.isAllowViewAgentRealTimeSipListen()),
                convertBooleanToEnumString(userGroup.isAllowViewAgentRealTimeSummary()),
                convertBooleanToEnumString(userGroup.isAllowViewAgentStats()),
                convertBooleanToEnumString(userGroup.isAllowViewAgentStatus()),
                convertBooleanToEnumString(userGroup.isAllowViewAgentTimeSheet()),
                convertBooleanToEnumString(userGroup.isAllowExportAgentTimeSheet()),
                convertBooleanToEnumString(userGroup.isAllowViewCampaignCallReport()),
                convertBooleanToEnumString(userGroup.isAllowExportCampaignCallReport()),
                convertBooleanToEnumString(userGroup.isAllowViewCampaignRecentOutboundSales()),
                convertBooleanToEnumString(userGroup.isAllowExportCampaignRecentOutboundSales()),
                convertBooleanToEnumString(userGroup.isAllowViewIngroupCallReport()),
                convertBooleanToEnumString(userGroup.isAllowExportIngroupCallReport()),
                convertBooleanToEnumString(userGroup.isAllowViewLeadPerformanceCampaign()),
                convertBooleanToEnumString(userGroup.isAllowExportLeadPerformanceCampaign()),
                convertBooleanToEnumString(userGroup.isAllowViewLeadPerformanceList()),
                convertBooleanToEnumString(userGroup.isAllowExportLeadPerformanceList()),
                convertBooleanToEnumString(userGroup.isAllowViewLeadSearch()),
                convertBooleanToEnumString(userGroup.isAllowViewLeadSearchAdvanced()),
                convertBooleanToEnumString(userGroup.isAllowExportLeadSearchAdvanced()),
                convertBooleanToEnumString(userGroup.isAllowViewListCostEntry()),
                convertBooleanToEnumString(userGroup.isAllowExportListCostEntry()),
                convertBooleanToEnumString(userGroup.isAllowViewServerPerformance()),
                convertBooleanToEnumString(userGroup.isAllowViewServerTimes()),
                convertBooleanToEnumString(userGroup.isAllowViewUserGroupHourlyStats()),
                userGroup.getAllowedScripts(),
                userGroup.getAllowedEmailTemplates(),
                userGroup.getAllowedIngroups(),
                userGroup.getAgentMessage(),
                userGroup.getId()) == 0) ? null : userGroup;
    }

    @Override
    public Long delete(Long id) throws PersistException {
        return Long.valueOf(jdbcTemplate.update("DELETE FROM osdial_user_groups WHERE id = ?", id));
    }



    private class UserGroupRowMapper implements RowMapper {
        @Override
        public Object mapRow(ResultSet rs, int i) throws SQLException {
            UserGroup userGroup = new UserGroup();
            userGroup.setId(rs.getLong("id"));
            userGroup.setName(rs.getString("group_name"));
            userGroup.setAllowedCampaigns(rs.getString("allowed_campaigns"));
            userGroup.setAllowViewAgentPauseSummary(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("view_agent_pause_summary")));
            userGroup.setAllowExportAgentPauseSummary(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("export_agent_pause_summary")));
            userGroup.setAllowViewAgentPerformanceDetail(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("view_agent_performance_detail")));
            userGroup.setAllowExportAgentPerformanceDetail(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("export_agent_performance_detail")));
            userGroup.setAllowViewAgentRealTime(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("view_agent_realtime")));
            userGroup.setAllowViewAgentRealTimeIaxBarge(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("view_agent_realtime_iax_barge")));
            userGroup.setAllowViewAgentRealTimeIaxListen(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("view_agent_realtime_iax_listen")));
            userGroup.setAllowViewAgentRealTimeSipBarge(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("view_agent_realtime_sip_barge")));
            userGroup.setAllowViewAgentRealTimeSipListen(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("view_agent_realtime_sip_listen")));
            userGroup.setAllowViewAgentRealTimeSummary(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("view_agent_realtime_summary")));
            userGroup.setAllowViewAgentStats(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("view_agent_stats")));
            userGroup.setAllowViewAgentStatus(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("view_agent_status")));
            userGroup.setAllowViewAgentTimeSheet(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("view_agent_timesheet")));
            userGroup.setAllowExportAgentTimeSheet(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("export_agent_timesheet")));
            userGroup.setAllowViewCampaignCallReport(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("view_campaign_call_report")));
            userGroup.setAllowExportCampaignCallReport(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("export_campaign_call_report")));
            userGroup.setAllowViewCampaignRecentOutboundSales(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("view_campaign_recent_outbound_sales")));
            userGroup.setAllowExportCampaignRecentOutboundSales(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("export_campaign_recent_outbound_sales")));
            userGroup.setAllowViewIngroupCallReport(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("view_ingroup_call_report")));
            userGroup.setAllowExportIngroupCallReport(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("export_ingroup_call_report")));
            userGroup.setAllowViewLeadPerformanceCampaign(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("view_lead_performance_campaign")));
            userGroup.setAllowExportLeadPerformanceCampaign(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("export_lead_performance_campaign")));
            userGroup.setAllowViewLeadPerformanceList(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("view_lead_performance_list")));
            userGroup.setAllowExportLeadPerformanceList(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("export_lead_performance_list")));
            userGroup.setAllowViewLeadSearch(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("view_lead_search")));
            userGroup.setAllowViewLeadSearchAdvanced(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("view_lead_search_advanced")));
            userGroup.setAllowExportLeadSearchAdvanced(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("export_lead_search_advanced")));
            userGroup.setAllowViewListCostEntry(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("view_list_cost_entry")));
            userGroup.setAllowExportListCostEntry(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("export_list_cost_entry")));
            userGroup.setAllowViewServerPerformance(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("view_server_performance")));
            userGroup.setAllowViewServerTimes(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("view_server_times")));
            userGroup.setAllowViewUserGroupHourlyStats(convertEnumValueFromMysqlOsdialDbToBoolean(rs.getString("view_usergroup_hourly_stats")));
            userGroup.setAllowedScripts(rs.getString("allowed_scripts"));
            userGroup.setAllowedEmailTemplates(rs.getString("allowed_email_templates"));
            userGroup.setAllowedIngroups(rs.getString("allowed_ingroups"));
            userGroup.setAgentMessage(rs.getString("agent_message"));
            return userGroup;
        }
    }

    private boolean convertEnumValueFromMysqlOsdialDbToBoolean(String enumValue) {
        if (enumValue == null){
            return false;
        }else if(enumValue.equals("N")) {
            return false;
        } else {
            return true;
        }
    }

    private String convertBooleanToEnumString(Boolean value){
        if(value) {
            return "1";
        }else {
            return "0";
        }
    }
}
