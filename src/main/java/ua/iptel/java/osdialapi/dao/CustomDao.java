package ua.iptel.java.osdialapi.dao;

import ua.iptel.java.osdialapi.dao.exception.PersistException;
import ua.iptel.java.osdialapi.model.dto.CustomNumberResponse;

public interface CustomDao {

  CustomNumberResponse getAgentCurrentConnectedPhoneNumber(Long agentInternalPhoneNumber)
      throws PersistException;
}
