package ua.iptel.java.osdialapi.dao.osdial;

import ua.iptel.java.osdialapi.dao.AbstractDao;
import ua.iptel.java.osdialapi.model.Hopper;

public interface HopperDao extends AbstractDao<Hopper> {
    Long deleteByLeadId(Long leadId);
    Long deleteByListId(Long listId);
}
