package ua.iptel.java.osdialapi.dao.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ua.iptel.java.osdialapi.dao.exception.PersistException;
import ua.iptel.java.osdialapi.dao.osdial.HopperDao;
import ua.iptel.java.osdialapi.model.Hopper;

import java.util.List;

@Repository
public class JdbcHopperDaoImpl implements HopperDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;


    @Override
    public Hopper getByPK(Long id) throws PersistException {
        return null;
    }

    @Override
    public List<Hopper> getAll() throws PersistException {
        return null;
    }

    @Override
    public Hopper create(Hopper object) throws PersistException {
        return null;
    }

    @Override
    public Hopper update(Hopper object) throws PersistException {
        return null;
    }

    @Override
    public Long delete(Long id) throws PersistException {
        return null;
    }

    @Override
    public Long deleteByLeadId(Long leadId) {
        return Long.valueOf(jdbcTemplate.update("DELETE FROM osdial_hopper WHERE lead_id=?", leadId));
    }

    @Override
    public Long deleteByListId(Long listId) {
        return null;
    }
}
