package ua.iptel.java.osdialapi.dao.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ua.iptel.java.osdialapi.dao.osdial.AccessTokenDao;
import ua.iptel.java.osdialapi.model.AccessToken;
import ua.iptel.java.osdialapi.model.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public class AccessTokenDaoImpl implements AccessTokenDao {
    @Autowired
    @Qualifier("jdbcTemplateOsdialDb")
    private JdbcTemplate jdbcTemplate;


    @Override
    public AccessToken findByToken(String accessTokenString) {
        List<AccessToken> accessTokensList = jdbcTemplate.query("SELECT * FROM access_token WHERE token = ?", new AccessTokenRowMapper(), accessTokenString);
        return DataAccessUtils.singleResult(accessTokensList);
    }

    @Override
    public AccessToken findByUserId(Long id) {
        String sql = "select *" +
                " from access_token left outer join osdial_users on access_token.user_id = osdial_users.user_id WHERE access_token.user_id = ?";
        List<AccessToken> accessTokensList = jdbcTemplate.query(sql, new AccessTokenRowMapper(), id);
        return DataAccessUtils.singleResult(accessTokensList);
    }

    @Override
    public AccessToken create(AccessToken accessToken) {
        LocalDateTime expire = LocalDateTime.now().plusDays(1);
        jdbcTemplate.update("INSERT INTO access_token(token, expire, user_id) VALUES (?, ?, ?)",
                accessToken.getToken(), Timestamp.valueOf(expire), accessToken.getUser().getId());
        accessToken.setExpire(expire);
        return accessToken;
    }

    @Override
    public AccessToken update(AccessToken accessToken) {
        LocalDateTime updatedExpire = LocalDateTime.now().plusDays(1);
        jdbcTemplate.update("UPDATE access_token SET token = ?, expire = ? WHERE access_token_id = ?",
                accessToken.getToken(), Timestamp.valueOf(updatedExpire), accessToken.getId());
        accessToken.setExpire(updatedExpire);
        return accessToken;
    }

    private class AccessTokenRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            User loggedInUser = new User();
            loggedInUser.setId(rs.getLong("user_id"));
            loggedInUser.setUsername(rs.getString("user"));
            loggedInUser.setFullName(rs.getString("full_name"));
            loggedInUser.setUserLevel(rs.getInt("user_level"));
            loggedInUser.setUserGroup(rs.getString("user_group"));
            loggedInUser.setPhoneLogin(rs.getString("phone_login"));
            loggedInUser.setPhonePassword(rs.getString("phone_pass"));
            loggedInUser.setAllowDeleteUsers(rs.getBoolean("delete_users"));
            loggedInUser.setAllowDeleteUserGroups(rs.getBoolean("delete_user_groups"));
            loggedInUser.setAllowDeleteLists(rs.getBoolean("delete_lists"));
            loggedInUser.setAllowDeleteCampaigns(rs.getBoolean("delete_campaigns"));
            loggedInUser.setAllowDeleteIngroups(rs.getBoolean("delete_ingroups"));
            loggedInUser.setAllowDeleteRemoteAgents(rs.getBoolean("delete_remote_agents"));
            loggedInUser.setAllowLoadLeads(rs.getBoolean("load_leads"));
            loggedInUser.setAllowShowCampaignDetail(rs.getBoolean("campaign_detail"));
            loggedInUser.setAstAdminAccess(rs.getBoolean("ast_admin_access"));
            loggedInUser.setAstDeletePhones(rs.getBoolean("ast_delete_phones"));
            loggedInUser.setAllowDeleteScripts(rs.getBoolean("delete_scripts"));
            loggedInUser.setAllowModifyLeads(rs.getBoolean("modify_leads"));
            loggedInUser.setHotKeyActive(rs.getBoolean("hotkeys_active"));
            loggedInUser.setAllowChangeAgentCampaign(rs.getBoolean("change_agent_campaign"));
            loggedInUser.setCloserCampaign(rs.getString("closer_campaigns"));
            loggedInUser.setAllowScheduledCallbacks(rs.getBoolean("scheduled_callbacks"));
            loggedInUser.setAllowAgentOnlyCallbacks(rs.getBoolean("agentonly_callbacks"));
            loggedInUser.setAllowAgentCallManual(rs.getBoolean("agentcall_manual"));
            loggedInUser.setOsdialRecording(rs.getBoolean("osdial_recording"));
            loggedInUser.setOsdialTransfer(rs.getBoolean("osdial_transfers"));
            loggedInUser.setAllowDeleteFilters(rs.getBoolean("delete_filters"));
            loggedInUser.setAllowModifyAgentInterfaceOptions(rs.getBoolean("alter_agent_interface_options"));
            loggedInUser.setCloserDefaultBlended(rs.getBoolean("closer_default_blended"));
            loggedInUser.setAllowDeleteCallTimes(rs.getBoolean("delete_call_times"));
            loggedInUser.setAllowModifyCallTimes(rs.getBoolean("modify_call_times"));
            loggedInUser.setAllowModifyUsers(rs.getBoolean("modify_users"));
            loggedInUser.setAllowModifyCampaigns(rs.getBoolean("modify_campaigns"));
            loggedInUser.setAllowModifyLists(rs.getBoolean("modify_lists"));
            loggedInUser.setAllowModifyScripts(rs.getBoolean("modify_scripts"));
            loggedInUser.setAllowModifyFilters(rs.getBoolean("modify_filters"));
            loggedInUser.setAllowModifyInGroups(rs.getBoolean("modify_ingroups"));
            loggedInUser.setAllowModifyUserGroups(rs.getBoolean("modify_usergroups"));
            loggedInUser.setAllowModifyRemoteAgents(rs.getBoolean("modify_remoteagents"));
            loggedInUser.setAllowModifyServers(rs.getBoolean("modify_servers"));
            loggedInUser.setAllowOsdialRecordingOverride(rs.getString("osdial_recording_override"));
            loggedInUser.setAllowAlterCustDataOverride(rs.getString("alter_custdata_override"));
            loggedInUser.setManualDialNewLimit(rs.getInt("manual_dial_new_limit"));
            loggedInUser.setAllowManualDialSkip(rs.getBoolean("manual_dial_allow_skip"));
            loggedInUser.setAllowExportLeads(rs.getBoolean("export_leads"));
            loggedInUser.setAdminAPIAccess(rs.getBoolean("admin_api_access"));
            loggedInUser.setAgentAPIAccess(rs.getBoolean("agent_api_access"));
            loggedInUser.setAllowXferAgent2Agent(rs.getBoolean("xfer_agent2agent"));
            loggedInUser.setScriptOverride(rs.getString("script_override"));
            loggedInUser.setAllowLoadDnc(rs.getBoolean("load_dnc"));
            loggedInUser.setAllowDeleteDnc(rs.getBoolean("delete_dnc"));
            loggedInUser.setAccessCode(rs.getString("access_code"));

            Timestamp accessTimeout = rs.getTimestamp("access_timeout");
            if(accessTimeout != null) {
                loggedInUser.setTokenExpire(accessTimeout.toLocalDateTime());
            }

            loggedInUser.setAllowModifyPhones(rs.getBoolean("modify_phones"));
            loggedInUser.setAllowModifyTrunks(rs.getBoolean("modify_trunks"));
            loggedInUser.setAllowModifyExtensions(rs.getBoolean("modify_ext"));
            loggedInUser.setAllowModifyStatuses(rs.getBoolean("modify_statuses"));
            loggedInUser.setAllowModifyConferensions(rs.getBoolean("modify_conferensions"));
            loggedInUser.setPermAdministrationMenu(rs.getBoolean("perm_administration_menu"));


            String token = rs.getString("token");
            LocalDateTime expire = rs.getTimestamp("expire").toLocalDateTime();

            AccessToken accessToken = new AccessToken(loggedInUser, token, expire);
            accessToken.setId(rs.getInt("access_token_id"));
            return accessToken;
        }}
}
