package ua.iptel.java.osdialapi.util;

import io.jsonwebtoken.*;
import ua.iptel.java.osdialapi.model.User;


import java.util.HashMap;
import java.util.Map;

public class JwtUtil {
    public static User parseToken(String token) {
        try {
            Claims body = Jwts.parser()
                    .setSigningKey("abc123")
                    .parseClaimsJws(token)
                    .getBody();

            User user = new User();
            user.setUsername(body.getSubject());
            user.setId(Long.parseLong((String) body.get("userId")));
            user.setUsername((String)body.get("username"));
            user.setUserLevel((Integer)body.get("userLevel"));

            user.setAllowModifyUsers((Boolean)body.get("modifyUsers"));
            user.setAllowDeleteUsers((Boolean)body.get("deleteUsers"));

            user.setAllowModifyCampaigns((Boolean)body.get("modifyCampaigns"));
            user.setAllowDeleteCampaigns((Boolean)body.get("deleteCampaigns"));

            user.setAllowModifyInGroups((Boolean)body.get("modifyInboundGroups"));
            user.setAllowDeleteIngroups((Boolean)body.get("deleteInboundGroups"));


            user.setAllowModifyLeads((Boolean)body.get("modifyLeads"));

            user.setAllowModifyLists((Boolean)body.get("modifyLists"));
            user.setAllowDeleteLists((Boolean)body.get("deleteLists"));

            user.setAllowModifyScripts((Boolean)body.get("modifyScripts"));
            user.setAllowDeleteScripts((Boolean)body.get("deleteScripts"));

            user.setAllowViewReports((Boolean)body.get("viewReports"));
            return user;

        } catch (JwtException | ClassCastException e) {
            return null;
        }
    }


    public static String generateToken(User user){
        Map<String, Object> tokenData = new HashMap<>();
        JwtBuilder jwtBuilder = Jwts.builder();
        String key = "abc123";
        tokenData.put("clientType", "user");
        tokenData.put("userId", user.getId().toString());
        tokenData.put("username", user.getUsername());
        tokenData.put("userLevel", user.getUserLevel());

        tokenData.put("modifyUsers", user.isAllowModifyUsers());
        tokenData.put("deleteUsers", user.isAllowDeleteUsers());

        tokenData.put("modifyCampaigns", user.isAllowModifyCampaigns());
        tokenData.put("deleteCampaigns", user.isAllowDeleteCampaigns());

        tokenData.put("modifyInboundGroups", user.isAllowModifyInGroups());
        tokenData.put("deleteInboundGroups", user.isAllowDeleteIngroups());

        tokenData.put("modifyLeads", user.isAllowModifyLeads());

        tokenData.put("modifyLists", user.isAllowModifyLists());
        tokenData.put("deleteLists", user.isAllowDeleteLists());

        tokenData.put("viewReports", user.isAllowViewReports());

        tokenData.put("modifyScripts", user.isAllowModifyScripts());
        tokenData.put("deleteScripts", user.isAllowDeleteScripts());

        jwtBuilder.setClaims(tokenData);

        String token = jwtBuilder.signWith(SignatureAlgorithm.HS512, key).compact();
        return token;
    }
}
