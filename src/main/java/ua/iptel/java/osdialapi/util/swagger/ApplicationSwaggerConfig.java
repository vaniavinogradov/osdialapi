package ua.iptel.java.osdialapi.util.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class ApplicationSwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("ua.iptel.java.osdialapi.web.controllers"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        ApiInfo apiInfo = new ApiInfo(
                "Osdial RESTFul API",
                "RESTFul API for OSDial dialer system",
                "1.0.1",
                "Terms of service",
                "Ivan Vynogradov",
                "License of API",
                "https://github.com/ipvinner/osdial-restful-API");
        return apiInfo;
    }

    @Bean
    SecurityConfiguration security() {
        return new SecurityConfiguration(null, null, null,
                null, "Bearer access_token", ApiKeyVehicle.HEADER, "Authorization", ",");
    }
}
