package ua.iptel.java.osdialapi.model.report.agent;

import io.swagger.models.auth.In;
import ua.iptel.java.osdialapi.model.dto.UserDto;

import java.util.Map;

public class AgentStatusesDetailedReport {
    private static final long serialVersionUID = 1L;

    private UserDto user;
    private Map<String, Integer> statuses;

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    public Map<String, Integer> getStatuses() {
        return statuses;
    }

    public void setStatuses(Map<String, Integer> statuses) {
        this.statuses = statuses;
    }
}
