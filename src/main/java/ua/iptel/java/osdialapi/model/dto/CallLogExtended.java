package ua.iptel.java.osdialapi.model.dto;

import ua.iptel.java.osdialapi.model.CallLog;

public class CallLogExtended extends CallLog {
    private String leadName;
    private String listName;

    public String getLeadName() {
        return leadName;
    }

    public void setLeadName(String leadName) {
        this.leadName = leadName;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }
}
