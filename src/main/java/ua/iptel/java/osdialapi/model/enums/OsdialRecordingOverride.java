package ua.iptel.java.osdialapi.model.enums;

public enum OsdialRecordingOverride {
    DISABLED,
    NEVER,
    ONDEMEND,
    ALLCALLS,
    ALLFORCE
}
