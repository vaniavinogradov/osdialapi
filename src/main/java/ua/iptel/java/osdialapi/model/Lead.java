package ua.iptel.java.osdialapi.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.time.LocalDateTime;
@JsonIgnoreProperties(ignoreUnknown = true)
public class Lead implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private LocalDateTime entryDate;
    private LocalDateTime modifyDateTime;
    private String status;
    private String user;
    private String vendorLeadCode;
    private String sourceId;
    private Long listId;
    private Float gmtOffsetNow;
    private String calledSinceLastReset;
    private String phoneCode;
    private String phoneNumber;
    private String title;
    private String firstName;
    private String middleInitial;
    private String lastName;
    private String address1;
    private String address2;
    private String address3;
    private String city;
    private String state;
    private String province;
    private String postalCode;
    private String countryCode;
    private String gender;
    private LocalDateTime dateOfBirth;
    private String altPhone;
    private String email;
    private String custom1;
    private String comments;
    private Integer calledCount;
    private String custom2;
    private Long externalKey;
    private LocalDateTime lastLocalCallTime;
    private Float cost;
    private LocalDateTime postDateTime;
    private String organization;
    private String organizationTitle;
    private String statusExtended;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(LocalDateTime entryDate) {
        this.entryDate = entryDate;
    }

    public LocalDateTime getModifyDateTime() {
        return modifyDateTime;
    }

    public void setModifyDateTime(LocalDateTime modifyDateTime) {
        this.modifyDateTime = modifyDateTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getVendorLeadCode() {
        return vendorLeadCode;
    }

    public void setVendorLeadCode(String vendorLeadCode) {
        this.vendorLeadCode = vendorLeadCode;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public Long getListId() {
        return listId;
    }

    public void setListId(Long listId) {
        this.listId = listId;
    }

    public Float getGmtOffsetNow() {
        return gmtOffsetNow;
    }

    public void setGmtOffsetNow(Float gmtOffsetNow) {
        this.gmtOffsetNow = gmtOffsetNow;
    }

    public String getCalledSinceLastReset() {
        return calledSinceLastReset;
    }

    public void setCalledSinceLastReset(String calledSinceLastReset) {
        this.calledSinceLastReset = calledSinceLastReset;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleInitial() {
        return middleInitial;
    }

    public void setMiddleInitial(String middleInitial) {
        this.middleInitial = middleInitial;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public LocalDateTime getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDateTime dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAltPhone() {
        return altPhone;
    }

    public void setAltPhone(String altPhone) {
        this.altPhone = altPhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCustom1() {
        return custom1;
    }

    public void setCustom1(String custom1) {
        this.custom1 = custom1;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Integer getCalledCount() {
        return calledCount;
    }

    public void setCalledCount(Integer calledCount) {
        if(calledCount == null) {
            this.calledCount = 0;
        }else {
            this.calledCount = calledCount;
        }

    }

    public String getCustom2() {
        return custom2;
    }

    public void setCustom2(String custom2) {
        this.custom2 = custom2;
    }

    public Long getExternalKey() {
        return externalKey;
    }

    public void setExternalKey(Long externalKey) {
        this.externalKey = externalKey;
    }

    public LocalDateTime getLastLocalCallTime() {
        return lastLocalCallTime;
    }

    public void setLastLocalCallTime(LocalDateTime lastLocalCallTime) {
        this.lastLocalCallTime = lastLocalCallTime;
    }

    public Float getCost() {
        return cost;
    }

    public void setCost(Float cost) {
        this.cost = cost;
    }

    public LocalDateTime getPostDateTime() {
        return postDateTime;
    }

    public void setPostDateTime(LocalDateTime postDateTime) {
        this.postDateTime = postDateTime;
    }

    public String getOrganizationTitle() {
        return organizationTitle;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public void setOrganizationTitle(String organizationTitle) {
        this.organizationTitle = organizationTitle;
    }

    public String getStatusExtended() {
        return statusExtended;
    }

    public void setStatusExtended(String statusExtended) {
        this.statusExtended = statusExtended;
    }
}
