package ua.iptel.java.osdialapi.model;

import java.io.Serializable;

public class UserGroup implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    private String allowedCampaigns;
    private boolean allowViewAgentPauseSummary;
    private boolean allowExportAgentPauseSummary;
    private boolean allowViewAgentPerformanceDetail;
    private boolean allowExportAgentPerformanceDetail;
    private boolean allowViewAgentRealTime;
    private boolean allowViewAgentRealTimeIaxBarge;
    private boolean allowViewAgentRealTimeIaxListen;
    private boolean allowViewAgentRealTimeSipBarge;
    private boolean allowViewAgentRealTimeSipListen;
    private boolean allowViewAgentRealTimeSummary;
    private boolean allowViewAgentStats;
    private boolean allowViewAgentStatus;
    private boolean allowViewAgentTimeSheet;
    private boolean allowExportAgentTimeSheet;
    private boolean allowViewCampaignCallReport;
    private boolean allowExportCampaignCallReport;
    private boolean allowViewCampaignRecentOutboundSales;
    private boolean allowExportCampaignRecentOutboundSales;
    private boolean allowViewIngroupCallReport;
    private boolean allowExportIngroupCallReport;
    private boolean allowViewLeadPerformanceCampaign;
    private boolean allowExportLeadPerformanceCampaign;
    private boolean allowViewLeadPerformanceList;
    private boolean allowExportLeadPerformanceList;
    private boolean allowViewLeadSearch;
    private boolean allowViewLeadSearchAdvanced;
    private boolean allowExportLeadSearchAdvanced;
    private boolean allowViewListCostEntry;
    private boolean allowExportListCostEntry;
    private boolean allowViewServerPerformance;
    private boolean allowViewServerTimes;
    private boolean allowViewUserGroupHourlyStats;
    private String allowedScripts;
    private String allowedEmailTemplates;
    private String allowedIngroups;
    private String agentMessage;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAllowedCampaigns() {
        return allowedCampaigns;
    }

    public void setAllowedCampaigns(String allowedCampaigns) {
        this.allowedCampaigns = allowedCampaigns;
    }

    public boolean isAllowViewAgentPauseSummary() {
        return allowViewAgentPauseSummary;
    }

    public void setAllowViewAgentPauseSummary(boolean allowViewAgentPauseSummary) {
        this.allowViewAgentPauseSummary = allowViewAgentPauseSummary;
    }

    public boolean isAllowExportAgentPauseSummary() {
        return allowExportAgentPauseSummary;
    }

    public void setAllowExportAgentPauseSummary(boolean allowExportAgentPauseSummary) {
        this.allowExportAgentPauseSummary = allowExportAgentPauseSummary;
    }

    public boolean isAllowViewAgentPerformanceDetail() {
        return allowViewAgentPerformanceDetail;
    }

    public void setAllowViewAgentPerformanceDetail(boolean allowViewAgentPerformanceDetail) {
        this.allowViewAgentPerformanceDetail = allowViewAgentPerformanceDetail;
    }

    public boolean isAllowExportAgentPerformanceDetail() {
        return allowExportAgentPerformanceDetail;
    }

    public void setAllowExportAgentPerformanceDetail(boolean allowExportAgentPerformanceDetail) {
        this.allowExportAgentPerformanceDetail = allowExportAgentPerformanceDetail;
    }

    public boolean isAllowViewAgentRealTime() {
        return allowViewAgentRealTime;
    }

    public void setAllowViewAgentRealTime(boolean allowViewAgentRealTime) {
        this.allowViewAgentRealTime = allowViewAgentRealTime;
    }

    public boolean isAllowViewAgentRealTimeIaxBarge() {
        return allowViewAgentRealTimeIaxBarge;
    }

    public void setAllowViewAgentRealTimeIaxBarge(boolean allowViewAgentRealTimeIaxBarge) {
        this.allowViewAgentRealTimeIaxBarge = allowViewAgentRealTimeIaxBarge;
    }

    public boolean isAllowViewAgentRealTimeIaxListen() {
        return allowViewAgentRealTimeIaxListen;
    }

    public void setAllowViewAgentRealTimeIaxListen(boolean allowViewAgentRealTimeIaxListen) {
        this.allowViewAgentRealTimeIaxListen = allowViewAgentRealTimeIaxListen;
    }

    public boolean isAllowViewAgentRealTimeSipBarge() {
        return allowViewAgentRealTimeSipBarge;
    }

    public void setAllowViewAgentRealTimeSipBarge(boolean allowViewAgentRealTimeSipBarge) {
        this.allowViewAgentRealTimeSipBarge = allowViewAgentRealTimeSipBarge;
    }

    public boolean isAllowViewAgentRealTimeSipListen() {
        return allowViewAgentRealTimeSipListen;
    }

    public void setAllowViewAgentRealTimeSipListen(boolean allowViewAgentRealTimeSipListen) {
        this.allowViewAgentRealTimeSipListen = allowViewAgentRealTimeSipListen;
    }

    public boolean isAllowViewAgentRealTimeSummary() {
        return allowViewAgentRealTimeSummary;
    }

    public void setAllowViewAgentRealTimeSummary(boolean allowViewAgentRealTimeSummary) {
        this.allowViewAgentRealTimeSummary = allowViewAgentRealTimeSummary;
    }

    public boolean isAllowViewAgentStats() {
        return allowViewAgentStats;
    }

    public void setAllowViewAgentStats(boolean allowViewAgentStats) {
        this.allowViewAgentStats = allowViewAgentStats;
    }

    public boolean isAllowViewAgentStatus() {
        return allowViewAgentStatus;
    }

    public void setAllowViewAgentStatus(boolean allowViewAgentStatus) {
        this.allowViewAgentStatus = allowViewAgentStatus;
    }

    public boolean isAllowViewAgentTimeSheet() {
        return allowViewAgentTimeSheet;
    }

    public void setAllowViewAgentTimeSheet(boolean allowViewAgentTimeSheet) {
        this.allowViewAgentTimeSheet = allowViewAgentTimeSheet;
    }

    public boolean isAllowExportAgentTimeSheet() {
        return allowExportAgentTimeSheet;
    }

    public void setAllowExportAgentTimeSheet(boolean allowExportAgentTimeSheet) {
        this.allowExportAgentTimeSheet = allowExportAgentTimeSheet;
    }

    public boolean isAllowViewCampaignCallReport() {
        return allowViewCampaignCallReport;
    }

    public void setAllowViewCampaignCallReport(boolean allowViewCampaignCallReport) {
        this.allowViewCampaignCallReport = allowViewCampaignCallReport;
    }

    public boolean isAllowExportCampaignCallReport() {
        return allowExportCampaignCallReport;
    }

    public void setAllowExportCampaignCallReport(boolean allowExportCampaignCallReport) {
        this.allowExportCampaignCallReport = allowExportCampaignCallReport;
    }

    public boolean isAllowViewCampaignRecentOutboundSales() {
        return allowViewCampaignRecentOutboundSales;
    }

    public void setAllowViewCampaignRecentOutboundSales(boolean allowViewCampaignRecentOutboundSales) {
        this.allowViewCampaignRecentOutboundSales = allowViewCampaignRecentOutboundSales;
    }

    public boolean isAllowExportCampaignRecentOutboundSales() {
        return allowExportCampaignRecentOutboundSales;
    }

    public void setAllowExportCampaignRecentOutboundSales(boolean allowExportCampaignRecentOutboundSales) {
        this.allowExportCampaignRecentOutboundSales = allowExportCampaignRecentOutboundSales;
    }

    public boolean isAllowViewIngroupCallReport() {
        return allowViewIngroupCallReport;
    }

    public void setAllowViewIngroupCallReport(boolean allowViewIngroupCallReport) {
        this.allowViewIngroupCallReport = allowViewIngroupCallReport;
    }

    public boolean isAllowExportIngroupCallReport() {
        return allowExportIngroupCallReport;
    }

    public void setAllowExportIngroupCallReport(boolean allowExportIngroupCallReport) {
        this.allowExportIngroupCallReport = allowExportIngroupCallReport;
    }

    public boolean isAllowViewLeadPerformanceCampaign() {
        return allowViewLeadPerformanceCampaign;
    }

    public void setAllowViewLeadPerformanceCampaign(boolean allowViewLeadPerformanceCampaign) {
        this.allowViewLeadPerformanceCampaign = allowViewLeadPerformanceCampaign;
    }

    public boolean isAllowExportLeadPerformanceCampaign() {
        return allowExportLeadPerformanceCampaign;
    }

    public void setAllowExportLeadPerformanceCampaign(boolean allowExportLeadPerformanceCampaign) {
        this.allowExportLeadPerformanceCampaign = allowExportLeadPerformanceCampaign;
    }

    public boolean isAllowViewLeadPerformanceList() {
        return allowViewLeadPerformanceList;
    }

    public void setAllowViewLeadPerformanceList(boolean allowViewLeadPerformanceList) {
        this.allowViewLeadPerformanceList = allowViewLeadPerformanceList;
    }

    public boolean isAllowExportLeadPerformanceList() {
        return allowExportLeadPerformanceList;
    }

    public void setAllowExportLeadPerformanceList(boolean allowExportLeadPerformanceList) {
        this.allowExportLeadPerformanceList = allowExportLeadPerformanceList;
    }

    public boolean isAllowViewLeadSearch() {
        return allowViewLeadSearch;
    }

    public void setAllowViewLeadSearch(boolean allowViewLeadSearch) {
        this.allowViewLeadSearch = allowViewLeadSearch;
    }

    public boolean isAllowViewLeadSearchAdvanced() {
        return allowViewLeadSearchAdvanced;
    }

    public void setAllowViewLeadSearchAdvanced(boolean allowViewLeadSearchAdvanced) {
        this.allowViewLeadSearchAdvanced = allowViewLeadSearchAdvanced;
    }

    public boolean isAllowExportLeadSearchAdvanced() {
        return allowExportLeadSearchAdvanced;
    }

    public void setAllowExportLeadSearchAdvanced(boolean allowExportLeadSearchAdvanced) {
        this.allowExportLeadSearchAdvanced = allowExportLeadSearchAdvanced;
    }

    public boolean isAllowViewListCostEntry() {
        return allowViewListCostEntry;
    }

    public void setAllowViewListCostEntry(boolean allowViewListCostEntry) {
        this.allowViewListCostEntry = allowViewListCostEntry;
    }

    public boolean isAllowExportListCostEntry() {
        return allowExportListCostEntry;
    }

    public void setAllowExportListCostEntry(boolean allowExportListCostEntry) {
        this.allowExportListCostEntry = allowExportListCostEntry;
    }

    public boolean isAllowViewServerPerformance() {
        return allowViewServerPerformance;
    }

    public void setAllowViewServerPerformance(boolean allowViewServerPerformance) {
        this.allowViewServerPerformance = allowViewServerPerformance;
    }

    public boolean isAllowViewServerTimes() {
        return allowViewServerTimes;
    }

    public void setAllowViewServerTimes(boolean allowViewServerTimes) {
        this.allowViewServerTimes = allowViewServerTimes;
    }

    public boolean isAllowViewUserGroupHourlyStats() {
        return allowViewUserGroupHourlyStats;
    }

    public void setAllowViewUserGroupHourlyStats(boolean allowViewUserGroupHourlyStats) {
        this.allowViewUserGroupHourlyStats = allowViewUserGroupHourlyStats;
    }

    public String getAllowedScripts() {
        return allowedScripts;
    }

    public void setAllowedScripts(String allowedScripts) {
        this.allowedScripts = allowedScripts;
    }

    public String getAllowedEmailTemplates() {
        return allowedEmailTemplates;
    }

    public void setAllowedEmailTemplates(String allowedEmailTemplates) {
        this.allowedEmailTemplates = allowedEmailTemplates;
    }

    public String getAllowedIngroups() {
        return allowedIngroups;
    }

    public void setAllowedIngroups(String allowedIngroups) {
        this.allowedIngroups = allowedIngroups;
    }

    public String getAgentMessage() {
        return agentMessage;
    }

    public void setAgentMessage(String agentMessage) {
        this.agentMessage = agentMessage;
    }
}
