package ua.iptel.java.osdialapi.model;

import io.swagger.models.auth.In;

import java.io.Serializable;
import java.time.LocalDateTime;

public class AdditionalForm implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private Long[] campaignsIds;
    private String name;
    private String description;
    private String description2;
    private Integer priority;
    private Integer deleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long[] getCampaignsIds() {
        return campaignsIds;
    }

    public void setCampaignsIds(Long[] campaignsIds) {
        this.campaignsIds = campaignsIds;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription2() {
        return description2;
    }

    public void setDescription2(String description2) {
        this.description2 = description2;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }
}
