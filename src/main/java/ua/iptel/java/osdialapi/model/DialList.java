package ua.iptel.java.osdialapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

public class DialList implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    @NotNull
    @NotBlank(message = "list name can not be empty!")
    private String name;
    @NotNull
    private Long campaignId;
    @NotNull
    private Boolean active;
    private String description;
    private LocalDateTime changeDateTime;
    private LocalDateTime lastCallDate;
    private boolean scrubDNC;
    private LocalDateTime scrubLast;
    private String scrubInfo;
    private float cost;
    private String webFormAddress;
    private String webFormAddress2;
    private String listScript;
    private String leadTransferId;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Long campaignId) {
        this.campaignId = campaignId;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getChangeDateTime() {
        return changeDateTime;
    }

    public void setChangeDateTime(LocalDateTime changeDateTime) {
        this.changeDateTime = changeDateTime;
    }

    public LocalDateTime getLastCallDate() {
        return lastCallDate;
    }

    public void setLastCallDate(LocalDateTime lastCallDate) {
        this.lastCallDate = lastCallDate;
    }

    public boolean isScrubDNC() {
        return scrubDNC;
    }

    public void setScrubDNC(boolean scrubDNC) {
        this.scrubDNC = scrubDNC;
    }

    public LocalDateTime getScrubLast() {
        return scrubLast;
    }

    public void setScrubLast(LocalDateTime scrubLast) {
        this.scrubLast = scrubLast;
    }

    public String getScrubInfo() {
        return scrubInfo;
    }

    public void setScrubInfo(String scrubInfo) {
        this.scrubInfo = scrubInfo;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public String getWebFormAddress() {
        return webFormAddress;
    }

    public void setWebFormAddress(String webFormAddress) {
        this.webFormAddress = webFormAddress;
    }

    public String getWebFormAddress2() {
        return webFormAddress2;
    }

    public void setWebFormAddress2(String webFormAddress2) {
        this.webFormAddress2 = webFormAddress2;
    }

    public String getListScript() {
        return listScript;
    }

    public void setListScript(String listScript) {
        this.listScript = listScript;
    }

    public String getLeadTransferId() {
        return leadTransferId;
    }

    public void setLeadTransferId(String leadTransferId) {
        this.leadTransferId = leadTransferId;
    }
}
