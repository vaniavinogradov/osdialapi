package ua.iptel.java.osdialapi.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;

public class User implements Serializable, UserDetails {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String username;
    private String password;
    private String passwordMd5;

    private String fullName;
    private Integer userLevel;
    private String userGroup;
    private String phoneLogin;
    private String phonePassword;

    private boolean allowDeleteUsers;
    private boolean allowDeleteUserGroups;
    private boolean allowDeleteLists;
    private boolean allowDeleteCampaigns;
    private boolean allowDeleteIngroups;
    private boolean allowDeleteRemoteAgents;
    private boolean allowDeleteScripts;
    private boolean allowDeleteFilters;
    private boolean allowDeleteCallTimes;

    private boolean allowLoadLeads;
    private boolean allowShowCampaignDetail;
    private boolean astAdminAccess;
    private boolean astDeletePhones;

    private boolean hotKeyActive;
    private boolean allowChangeAgentCampaign;
    private boolean allowAgentChooseInGroups;
    private String closerCampaign;
    private boolean allowScheduledCallbacks;
    private boolean allowAgentOnlyCallbacks;
    private boolean allowAgentCallManual;
    private boolean osdialRecording;
    private boolean osdialTransfer;
    private boolean allowModifyAgentInterfaceOptions;
    private boolean closerDefaultBlended;


    private boolean allowModifyCallTimes;
    private boolean allowModifyUsers;
    private boolean allowModifyCampaigns;
    private boolean allowModifyLists;
    private boolean allowModifyLeads;
    private boolean allowModifyScripts;
    private boolean allowModifyFilters;
    private boolean allowModifyInGroups;
    private boolean allowModifyUserGroups;
    private boolean allowModifyRemoteAgents;
    private boolean allowModifyServers;
    private boolean allowModifyPhones;
    private boolean allowModifyTrunks;
    private boolean allowModifyExtensions;
    private boolean allowModifyStatuses;
    private boolean allowModifyConferensions;

    private boolean allowViewReports;
    private String allowOsdialRecordingOverride;
    private String allowAlterCustDataOverride;

    private int manualDialNewLimit;
    private boolean allowManualDialSkip;
    private boolean allowExportLeads;
    private boolean adminAPIAccess;
    private boolean agentAPIAccess;
    private boolean allowXferAgent2Agent;
    private String scriptOverride;
    private boolean allowLoadDnc;
    private boolean allowExportDnc;
    private boolean allowDeleteDnc;
    private String accessCode;
    private LocalDateTime tokenExpire;
    private boolean permAdministrationMenu;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Integer getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(Integer userLevel) {
        this.userLevel = userLevel;
    }

    public String getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(String userGroup) {
        this.userGroup = userGroup;
    }

    public String getPhoneLogin() {
        return phoneLogin;
    }

    public void setPhoneLogin(String phoneLogin) {
        this.phoneLogin = phoneLogin;
    }

    public String getPhonePassword() {
        return phonePassword;
    }

    public void setPhonePassword(String phonePassword) {
        this.phonePassword = phonePassword;
    }

    public Boolean isAllowDeleteUsers() {
        return allowDeleteUsers;
    }

    public void setAllowDeleteUsers(boolean allowDeleteUsers) {
        this.allowDeleteUsers = allowDeleteUsers;
    }

    public boolean isAllowDeleteUserGroups() {
        return allowDeleteUserGroups;
    }

    public void setAllowDeleteUserGroups(boolean allowDeleteUserGroups) {
        this.allowDeleteUserGroups = allowDeleteUserGroups;
    }

    public boolean isAllowDeleteLists() {
        return allowDeleteLists;
    }

    public void setAllowDeleteLists(boolean allowDeleteLists) {
        this.allowDeleteLists = allowDeleteLists;
    }

    public boolean isAllowDeleteCampaigns() {
        return allowDeleteCampaigns;
    }

    public void setAllowDeleteCampaigns(boolean allowDeleteCampaigns) {
        this.allowDeleteCampaigns = allowDeleteCampaigns;
    }

    public boolean isAllowDeleteRemoteAgents() {
        return allowDeleteRemoteAgents;
    }

    public void setAllowDeleteRemoteAgents(boolean allowDeleteRemoteAgents) {
        this.allowDeleteRemoteAgents = allowDeleteRemoteAgents;
    }

    public boolean isAllowDeleteScripts() {
        return allowDeleteScripts;
    }

    public void setAllowDeleteScripts(boolean allowDeleteScripts) {
        this.allowDeleteScripts = allowDeleteScripts;
    }

    public boolean isAllowDeleteFilters() {
        return allowDeleteFilters;
    }

    public void setAllowDeleteFilters(boolean allowDeleteFilters) {
        this.allowDeleteFilters = allowDeleteFilters;
    }

    public boolean isAllowDeleteCallTimes() {
        return allowDeleteCallTimes;
    }

    public void setAllowDeleteCallTimes(boolean allowDeleteCallTimes) {
        this.allowDeleteCallTimes = allowDeleteCallTimes;
    }

    public boolean isAllowLoadLeads() {
        return allowLoadLeads;
    }

    public void setAllowLoadLeads(boolean allowLoadLeads) {
        this.allowLoadLeads = allowLoadLeads;
    }

    public boolean isAllowShowCampaignDetail() {
        return allowShowCampaignDetail;
    }

    public void setAllowShowCampaignDetail(boolean allowShowCampaignDetail) {
        this.allowShowCampaignDetail = allowShowCampaignDetail;
    }

    public boolean isAstAdminAccess() {
        return astAdminAccess;
    }

    public void setAstAdminAccess(boolean astAdminAccess) {
        this.astAdminAccess = astAdminAccess;
    }

    public boolean isAstDeletePhones() {
        return astDeletePhones;
    }

    public void setAstDeletePhones(boolean astDeletePhones) {
        this.astDeletePhones = astDeletePhones;
    }

    public boolean isHotKeyActive() {
        return hotKeyActive;
    }

    public void setHotKeyActive(boolean hotKeyActive) {
        this.hotKeyActive = hotKeyActive;
    }

    public boolean isAllowChangeAgentCampaign() {
        return allowChangeAgentCampaign;
    }

    public void setAllowChangeAgentCampaign(boolean allowChangeAgentCampaign) {
        this.allowChangeAgentCampaign = allowChangeAgentCampaign;
    }

    public boolean isAllowAgentChooseInGroups() {
        return allowAgentChooseInGroups;
    }

    public void setAllowAgentChooseInGroups(boolean allowAgentChooseInGroups) {
        this.allowAgentChooseInGroups = allowAgentChooseInGroups;
    }

    public String getCloserCampaign() {
        return closerCampaign;
    }

    public void setCloserCampaign(String closerCampaign) {
        this.closerCampaign = closerCampaign;
    }

    public boolean isAllowScheduledCallbacks() {
        return allowScheduledCallbacks;
    }

    public void setAllowScheduledCallbacks(boolean allowScheduledCallbacks) {
        this.allowScheduledCallbacks = allowScheduledCallbacks;
    }

    public boolean isAllowAgentOnlyCallbacks() {
        return allowAgentOnlyCallbacks;
    }

    public void setAllowAgentOnlyCallbacks(boolean allowAgentOnlyCallbacks) {
        this.allowAgentOnlyCallbacks = allowAgentOnlyCallbacks;
    }

    public boolean isAllowAgentCallManual() {
        return allowAgentCallManual;
    }

    public void setAllowAgentCallManual(boolean allowAgentCallManual) {
        this.allowAgentCallManual = allowAgentCallManual;
    }

    public boolean isOsdialRecording() {
        return osdialRecording;
    }

    public void setOsdialRecording(boolean osdialRecording) {
        this.osdialRecording = osdialRecording;
    }

    public boolean isOsdialTransfer() {
        return osdialTransfer;
    }

    public void setOsdialTransfer(boolean osdialTransfer) {
        this.osdialTransfer = osdialTransfer;
    }

    public boolean isAllowModifyAgentInterfaceOptions() {
        return allowModifyAgentInterfaceOptions;
    }

    public void setAllowModifyAgentInterfaceOptions(boolean allowModifyAgentInterfaceOptions) {
        this.allowModifyAgentInterfaceOptions = allowModifyAgentInterfaceOptions;
    }

    public boolean isCloserDefaultBlended() {
        return closerDefaultBlended;
    }

    public void setCloserDefaultBlended(boolean closerDefaultBlended) {
        this.closerDefaultBlended = closerDefaultBlended;
    }

    public boolean isAllowModifyCallTimes() {
        return allowModifyCallTimes;
    }

    public void setAllowModifyCallTimes(boolean allowModifyCallTimes) {
        this.allowModifyCallTimes = allowModifyCallTimes;
    }

    public boolean isAllowModifyUsers() {
        return allowModifyUsers;
    }

    public void setAllowModifyUsers(boolean allowModifyUsers) {
        this.allowModifyUsers = allowModifyUsers;
    }

    public boolean isAllowModifyCampaigns() {
        return allowModifyCampaigns;
    }

    public void setAllowModifyCampaigns(boolean allowModifyCampaigns) {
        this.allowModifyCampaigns = allowModifyCampaigns;
    }

    public boolean isAllowModifyLists() {
        return allowModifyLists;
    }

    public void setAllowModifyLists(boolean allowModifyLists) {
        this.allowModifyLists = allowModifyLists;
    }

    public boolean isAllowModifyLeads() {
        return allowModifyLeads;
    }

    public void setAllowModifyLeads(boolean allowModifyLeads) {
        this.allowModifyLeads = allowModifyLeads;
    }

    public boolean isAllowModifyScripts() {
        return allowModifyScripts;
    }

    public void setAllowModifyScripts(boolean allowModifyScripts) {
        this.allowModifyScripts = allowModifyScripts;
    }

    public boolean isAllowModifyFilters() {
        return allowModifyFilters;
    }

    public void setAllowModifyFilters(boolean allowModifyFilters) {
        this.allowModifyFilters = allowModifyFilters;
    }

    public boolean isAllowModifyInGroups() {
        return allowModifyInGroups;
    }

    public void setAllowModifyInGroups(boolean allowModifyInGroups) {
        this.allowModifyInGroups = allowModifyInGroups;
    }

    public boolean isAllowModifyUserGroups() {
        return allowModifyUserGroups;
    }

    public void setAllowModifyUserGroups(boolean allowModifyUserGroups) {
        this.allowModifyUserGroups = allowModifyUserGroups;
    }

    public boolean isAllowModifyRemoteAgents() {
        return allowModifyRemoteAgents;
    }

    public void setAllowModifyRemoteAgents(boolean allowModifyRemoteAgents) {
        this.allowModifyRemoteAgents = allowModifyRemoteAgents;
    }

    public boolean isAllowModifyServers() {
        return allowModifyServers;
    }

    public void setAllowModifyServers(boolean allowModifyServers) {
        this.allowModifyServers = allowModifyServers;
    }

    public boolean isAllowModifyPhones() {
        return allowModifyPhones;
    }

    public void setAllowModifyPhones(boolean allowModifyPhones) {
        this.allowModifyPhones = allowModifyPhones;
    }

    public boolean isAllowModifyTrunks() {
        return allowModifyTrunks;
    }

    public void setAllowModifyTrunks(boolean allowModifyTrunks) {
        this.allowModifyTrunks = allowModifyTrunks;
    }

    public boolean isAllowModifyExtensions() {
        return allowModifyExtensions;
    }

    public void setAllowModifyExtensions(boolean allowModifyExtensions) {
        this.allowModifyExtensions = allowModifyExtensions;
    }

    public boolean isAllowModifyStatuses() {
        return allowModifyStatuses;
    }

    public void setAllowModifyStatuses(boolean allowModifyStatuses) {
        this.allowModifyStatuses = allowModifyStatuses;
    }

    public boolean isAllowModifyConferensions() {
        return allowModifyConferensions;
    }

    public void setAllowModifyConferensions(boolean allowModifyConferensions) {
        this.allowModifyConferensions = allowModifyConferensions;
    }

    public boolean isAllowViewReports() {
        return allowViewReports;
    }

    public void setAllowViewReports(boolean allowViewReports) {
        this.allowViewReports = allowViewReports;
    }

    public String isAllowOsdialRecordingOverride() {
        return allowOsdialRecordingOverride;
    }

    public void setAllowOsdialRecordingOverride(String allowOsdialRecordingOverride) {
        this.allowOsdialRecordingOverride = allowOsdialRecordingOverride;
    }

    public String isAllowAlterCustDataOverride() {
        return allowAlterCustDataOverride;
    }

    public void setAllowAlterCustDataOverride(String allowAlterCustDataOverride) {
        this.allowAlterCustDataOverride = allowAlterCustDataOverride;
    }

    public int getManualDialNewLimit() {
        return manualDialNewLimit;
    }

    public void setManualDialNewLimit(int manualDialNewLimit) {
        this.manualDialNewLimit = manualDialNewLimit;
    }

    public boolean isAllowManualDialSkip() {
        return allowManualDialSkip;
    }

    public void setAllowManualDialSkip(boolean allowManualDialSkip) {
        this.allowManualDialSkip = allowManualDialSkip;
    }

    public boolean isAllowExportLeads() {
        return allowExportLeads;
    }

    public void setAllowExportLeads(boolean allowExportLeads) {
        this.allowExportLeads = allowExportLeads;
    }

    public boolean isAdminAPIAccess() {
        return adminAPIAccess;
    }

    public void setAdminAPIAccess(boolean adminAPIAccess) {
        this.adminAPIAccess = adminAPIAccess;
    }

    public boolean isAgentAPIAccess() {
        return agentAPIAccess;
    }

    public void setAgentAPIAccess(boolean agentAPIAccess) {
        this.agentAPIAccess = agentAPIAccess;
    }

    public boolean isAllowXferAgent2Agent() {
        return allowXferAgent2Agent;
    }

    public void setAllowXferAgent2Agent(boolean allowXferAgent2Agent) {
        this.allowXferAgent2Agent = allowXferAgent2Agent;
    }

    public String getScriptOverride() {
        return scriptOverride;
    }

    public void setScriptOverride(String scriptOverride) {
        this.scriptOverride = scriptOverride;
    }

    public boolean isAllowLoadDnc() {
        return allowLoadDnc;
    }

    public void setAllowLoadDnc(boolean allowLoadDnc) {
        this.allowLoadDnc = allowLoadDnc;
    }

    public boolean isAllowExportDnc() {
        return allowExportDnc;
    }

    public void setAllowExportDnc(boolean allowExportDnc) {
        this.allowExportDnc = allowExportDnc;
    }

    public boolean isAllowDeleteDnc() {
        return allowDeleteDnc;
    }

    public void setAllowDeleteDnc(boolean allowDeleteDnc) {
        this.allowDeleteDnc = allowDeleteDnc;
    }

    public String getAccessCode() {
        return accessCode;
    }

    public void setAccessCode(String accessCode) {
        this.accessCode = accessCode;
    }

    public LocalDateTime getTokenExpire() {
        return tokenExpire;
    }

    public void setTokenExpire(LocalDateTime tokenExpire) {
        this.tokenExpire = tokenExpire;
    }

    public boolean isPermAdministrationMenu() {
        return permAdministrationMenu;
    }

    public void setPermAdministrationMenu(boolean permAdministrationMenu) {
        this.permAdministrationMenu = permAdministrationMenu;
    }

    public boolean isAllowDeleteIngroups() {
        return allowDeleteIngroups;
    }

    public void setAllowDeleteIngroups(boolean allowDeleteIngroups) {
        this.allowDeleteIngroups = allowDeleteIngroups;
    }

    public String getAllowOsdialRecordingOverride() {
        return allowOsdialRecordingOverride;
    }

    public String getAllowAlterCustDataOverride() {
        return allowAlterCustDataOverride;
    }

    public String getPasswordMd5() {
        return passwordMd5;
    }

    public void setPasswordMd5(String passwordMd5) {
        this.passwordMd5 = passwordMd5;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_ADMIN");
    }


}
