package ua.iptel.java.osdialapi.model;

import java.io.Serializable;

public class CallTime implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    private String comments;
    private Integer ctDefaultStart;
    private Integer ctDefaultStop;
    private Integer ctSundayStart;
    private Integer ctSundayStop;
    private Integer ctMondayStart;
    private Integer ctMondayStop;
    private Integer ctTuesdayStart;
    private Integer ctTuesdayStop;
    private Integer ctWednesdayStart;
    private Integer ctWednesdayStop;
    private Integer ctThursdayStart;
    private Integer ctThursdayStop;
    private Integer ctFridayStart;
    private Integer ctFridayStop;
    private Integer ctSaturdayStart;
    private Integer ctSaturdayStop;
    private String ctStateCallTimes;
    private boolean useRecycleGap;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Integer getCtDefaultStart() {
        return ctDefaultStart;
    }

    public void setCtDefaultStart(Integer ctDefaultStart) {
        this.ctDefaultStart = ctDefaultStart;
    }

    public Integer getCtDefaultStop() {
        return ctDefaultStop;
    }

    public void setCtDefaultStop(Integer ctDefaultStop) {
        this.ctDefaultStop = ctDefaultStop;
    }

    public Integer getCtSundayStart() {
        return ctSundayStart;
    }

    public void setCtSundayStart(Integer ctSundayStart) {
        this.ctSundayStart = ctSundayStart;
    }

    public Integer getCtSundayStop() {
        return ctSundayStop;
    }

    public void setCtSundayStop(Integer ctSundayStop) {
        this.ctSundayStop = ctSundayStop;
    }

    public Integer getCtMondayStart() {
        return ctMondayStart;
    }

    public void setCtMondayStart(Integer ctMondayStart) {
        this.ctMondayStart = ctMondayStart;
    }

    public Integer getCtMondayStop() {
        return ctMondayStop;
    }

    public void setCtMondayStop(Integer ctMondayStop) {
        this.ctMondayStop = ctMondayStop;
    }

    public Integer getCtTuesdayStart() {
        return ctTuesdayStart;
    }

    public void setCtTuesdayStart(Integer ctTuesdayStart) {
        this.ctTuesdayStart = ctTuesdayStart;
    }

    public Integer getCtTuesdayStop() {
        return ctTuesdayStop;
    }

    public void setCtTuesdayStop(Integer ctTuesdayStop) {
        this.ctTuesdayStop = ctTuesdayStop;
    }

    public Integer getCtWednesdayStart() {
        return ctWednesdayStart;
    }

    public void setCtWednesdayStart(Integer ctWednesdayStart) {
        this.ctWednesdayStart = ctWednesdayStart;
    }

    public Integer getCtWednesdayStop() {
        return ctWednesdayStop;
    }

    public void setCtWednesdayStop(Integer ctWednesdayStop) {
        this.ctWednesdayStop = ctWednesdayStop;
    }

    public Integer getCtThursdayStart() {
        return ctThursdayStart;
    }

    public void setCtThursdayStart(Integer ctThursdayStart) {
        this.ctThursdayStart = ctThursdayStart;
    }

    public Integer getCtThursdayStop() {
        return ctThursdayStop;
    }

    public void setCtThursdayStop(Integer ctThursdayStop) {
        this.ctThursdayStop = ctThursdayStop;
    }

    public Integer getCtFridayStart() {
        return ctFridayStart;
    }

    public void setCtFridayStart(Integer ctFridayStart) {
        this.ctFridayStart = ctFridayStart;
    }

    public Integer getCtFridayStop() {
        return ctFridayStop;
    }

    public void setCtFridayStop(Integer ctFridayStop) {
        this.ctFridayStop = ctFridayStop;
    }

    public Integer getCtSaturdayStart() {
        return ctSaturdayStart;
    }

    public void setCtSaturdayStart(Integer ctSaturdayStart) {
        this.ctSaturdayStart = ctSaturdayStart;
    }

    public Integer getCtSaturdayStop() {
        return ctSaturdayStop;
    }

    public void setCtSaturdayStop(Integer ctSaturdayStop) {
        this.ctSaturdayStop = ctSaturdayStop;
    }

    public String getCtStateCallTimes() {
        return ctStateCallTimes;
    }

    public void setCtStateCallTimes(String ctStateCallTimes) {
        this.ctStateCallTimes = ctStateCallTimes;
    }

    public boolean isUseRecycleGap() {
        return useRecycleGap;
    }

    public void setUseRecycleGap(boolean useRecycleGap) {
        this.useRecycleGap = useRecycleGap;
    }
}
