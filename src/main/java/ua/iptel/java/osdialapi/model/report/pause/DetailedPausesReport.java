package ua.iptel.java.osdialapi.model.report.pause;

import ua.iptel.java.osdialapi.model.User;
import ua.iptel.java.osdialapi.model.dto.CampaignDto;

import java.io.Serializable;
import java.time.LocalDateTime;

public class DetailedPausesReport implements Serializable {
    private static final long serialVersionUID = 1L;

    private User agent;
    private CampaignDto campaign;
    private String pauseCode;
    private LocalDateTime start;
    private LocalDateTime finish;
    private Integer totalTime;

    public User getAgent() {
        return agent;
    }

    public void setAgent(User agent) {
        this.agent = agent;
    }

    public CampaignDto getCampaign() {
        return campaign;
    }

    public void setCampaign(CampaignDto campaign) {
        this.campaign = campaign;
    }

    public String getPauseCode() {
        return pauseCode;
    }

    public void setPauseCode(String pauseCode) {
        this.pauseCode = pauseCode;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getFinish() {
        return finish;
    }

    public void setFinish(LocalDateTime finish) {
        this.finish = finish;
    }

    public Integer getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(Integer totalTime) {
        this.totalTime = totalTime;
    }
}
