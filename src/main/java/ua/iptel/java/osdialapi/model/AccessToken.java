package ua.iptel.java.osdialapi.model;

import java.time.LocalDateTime;

public class AccessToken {
    private Integer id;
    private String token;
    private User user;
    private LocalDateTime expire;

    protected AccessToken() {
        /* Reflection instantiation */
    }

    public AccessToken(User user, String token) {
        this.user = user;
        this.token = token;
    }

    public AccessToken(User user, String token, LocalDateTime expire)  {
        this(user, token);
        this.expire = expire;
    }

    public String getToken() {
        return this.token;
    }

    public User getUser() {
        return this.user;
    }

    public LocalDateTime getExpire() {
        return this.expire;
    }

    public void setExpire(LocalDateTime expire){
        this.expire = expire;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isExpired() {
        if (null == this.expire) {
            return false;
        }
            // logic when it's expire
        return true;
    }
}
