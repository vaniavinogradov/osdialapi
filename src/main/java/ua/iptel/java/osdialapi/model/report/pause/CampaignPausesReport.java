package ua.iptel.java.osdialapi.model.report.pause;

import ua.iptel.java.osdialapi.model.Campaign;
import ua.iptel.java.osdialapi.model.dto.CampaignDto;

import java.io.Serializable;

public class CampaignPausesReport implements Serializable {
    private static final long serialVersionUID = 1L;

    private CampaignDto campaign;
    private String pauseCode;
    private Integer count;
    private Integer totalTime;
    private Integer totalTimeAverage;

    public CampaignDto getCampaign() {
        return campaign;
    }

    public void setCampaign(CampaignDto campaign) {
        this.campaign = campaign;
    }

    public String getPauseCode() {
        return pauseCode;
    }

    public void setPauseCode(String pauseCode) {
        this.pauseCode = pauseCode;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(Integer totalTime) {
        this.totalTime = totalTime;
    }

    public Integer getTotalTimeAverage() {
        return totalTimeAverage;
    }

    public void setTotalTimeAverage(Integer totalTimeAverage) {
        this.totalTimeAverage = totalTimeAverage;
    }
}
