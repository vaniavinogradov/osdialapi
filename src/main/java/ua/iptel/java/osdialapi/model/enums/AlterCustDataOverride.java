package ua.iptel.java.osdialapi.model.enums;

public enum AlterCustDataOverride {
    NOT_ACTIVE,
    ALLOW_ALTER
}
