package ua.iptel.java.osdialapi.model;

import ua.iptel.java.osdialapi.model.validator.ValidateString;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Campaign implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;

    private String name;
    private boolean active;
    private String dialStatusA;
    private String dialStatusB;
    private String dialStatusC;
    private String dialStatusD;
    private String dialStatusE;
    private String leadOrder;
    private String parkExt;
    private String parkFileName;
    private String webFormAddress;
    private boolean allowClosers;
    private Integer hopper_level;
    private String autoDialLevel;
    @ValidateString(acceptedValues={"random", "oldest_call_start", "oldest_call_finish", "overall_user_level",
            "inbound_group_rank", "campaign_rank", "fewest_calls", "per_user"},
            message="Allowed field values: 'random', 'oldest_call_start', 'oldest_call_finish'," +
                    "'overall_user_level', 'inbound_group_rank', 'campaign_rank', 'fewest_calls', 'per_user'")
    private String nextAgentCall;
    private String localCallTime;
    private String voiceMailExtension;
    private Integer dialTimeout;
    private String dialPrefix;
    private String cid;
    private String vdadExtension;
    private String recordingExtension;
    private String recordingRule;
    @ValidateString(acceptedValues={"NEVER", "ONDEMAND", "ALLCALLS", "AllFORCE"},
            message="Allowed field values: 'NEVER', 'ONDEMAND', 'ALLCALLS', 'AllFORCE'")
    private String recording;
    private String recordingFileName;
    private String script;
    @ValidateString(acceptedValues={"NONE", "SCRIPT", "WEBFORM", "WEBFORM2"},
            message="Allowed field values: 'NONE', 'SCRIPT', 'WEBFORM', 'WEBFORM2'")
    private String getCallLaunch;
    private String amMessageExtension;
    @ValidateString(acceptedValues={"Y", "N", "CUSTOM1", "CUSTOM2"},
            message="Allowed field values: 'Y', 'N', 'CUSTOM1', 'CUSTOM2'")
    private String amdSendToVmx;
    private String xferConfADtmf;
    private String xferConfANumber;
    private String xferConfBDtmf;
    private String xferConfBNumber;
    private boolean altNumberDialing;
    private boolean allowScheduledCallBacks;
    private String leadFilterId;
    private Integer dropCallSeconds;
    private boolean allowSafeHarborMessage;
    private boolean allowSafeHarborExtension;
    private boolean allowDisplayDialableCount;
    private Integer wrapUpSeconds;
    private String wrapUpMessage;
    private String closerCampaigns;
    private boolean allowUseInternalDnc;
    private Integer allCallsDelay;
    private boolean omitPhoneCode;
    @ValidateString(acceptedValues={"MANUAL", "RATIO", "ADAPT_HARD_LIMIT", "ADAPT_TAPERED", "ADAPT_AVERAGE"},
            message="Allowed field values: 'MANUAL', 'RATIO', 'ADAPT_HARD_LIMIT', 'ADAPT_TAPERED', 'ADAPT_AVERAGE'")
    private String dialMethod;
    private boolean availableOnlyRatioTally;
    private Integer adaptiveDroppedPercentage;
    private String adaptiveMaximumLevel;
    private String adaptiveLatestServerTime;
    private String adaptiveIntensity;
    private Integer adaptiveDlDiffTarget;
    @ValidateString(acceptedValues={"AUTO", "1", "2", "3", "4", "5", "6", "7", "8", "9"},
            message="Allowed field values: 'AUTO', '1', '2', '3', '4', '5', '6', '7', '8', '9'")
    private String concurrentTransfers;
    @ValidateString(acceptedValues={"NONE", "ALT_ONLY", "ADDR3_ONLY", "ALT_AND_ADDR3", "ALT_ADDR3_AND_AFFAP"},
            message="Allowed field values: 'AUTO', 'NONE', 'ALT_ONLY', 'ADDR3_ONLY', 'ALT_AND_ADDR3', 'ALT_ADDR3_AND_AFFAP'")
    private String autoAltDial;
    private String AutoAltDialStatuses;
    private boolean agentPauseCodesActive;
    private String description;
    private LocalDateTime changeDateTime;
    private boolean statsRefresh;
    private LocalDateTime loginDateTime;
    private String dialStatuses;
    private boolean disableAlterCustData;
    private boolean noHopperLeadsLogins;
    private String listOrderMix;
    private boolean allowCampaignInbound;
    private Integer manualDialListId;
    private String defaultXferGroup;
    private String xferGroups;
    private String webFormAddress2;
    private boolean allowTabSwitch;
    private Integer answersPerHourLimit;
    private String campaignCallTime;
    private Integer previewForceDialTime;
    private boolean manualPreviewDefault;
    private boolean webFormExternalWindow;
    private boolean webForm2ExternalWindow;
    @ValidateString(acceptedValues={"NORMAL", "WEBFORM1", "WEBFORM2", "PASSBACK1", "PASSBACK2"},
            message="Allowed field values: 'NORMAL', 'WEBFORM1', 'WEBFORM2', 'PASSBACK1', 'PASSBACK2'")
    private String submitMethod;
    private boolean useCustom2CallierId;
    private LocalDateTime campaignLastCall;
    private String campaignCidName;
    @ValidateString(acceptedValues={"CAMPAIGN", "PHONE", "LEAD", "LEAD_CUSTOM2", "LEAD_CUSTOM1"},
            message="Allowed field values: 'CAMPAIGN', 'PHONE', 'LEAD', 'LEAD_CUSTOM2', 'LEAD_CUSTOM1'")
    private String xferCidMode;
    private boolean useCidAreaCodeMap;
    private Integer carrierId;
    private String emailTemplate;
    private boolean disableManualDial;
    private boolean hideXferLocalCloser;
    private boolean hideXferDialOverride;
    private boolean hideXferHangupXfer;
    private boolean hideXferLeave3way;
    private boolean hideXferDialWith;
    private boolean hideXferHangupBoth;
    private boolean hideXferBlindXfer;
    private boolean hideXferParkDial;
    private boolean hideXferBlindVmail;
    private boolean allowMdHopperList;
    private Integer ivrId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getDialStatusA() {
        return dialStatusA;
    }

    public void setDialStatusA(String dialStatusA) {
        this.dialStatusA = dialStatusA;
    }

    public String getDialStatusB() {
        return dialStatusB;
    }

    public void setDialStatusB(String dialStatusB) {
        this.dialStatusB = dialStatusB;
    }

    public String getDialStatusC() {
        return dialStatusC;
    }

    public void setDialStatusC(String dialStatusC) {
        this.dialStatusC = dialStatusC;
    }

    public String getDialStatusD() {
        return dialStatusD;
    }

    public void setDialStatusD(String dialStatusD) {
        this.dialStatusD = dialStatusD;
    }

    public String getDialStatusE() {
        return dialStatusE;
    }

    public void setDialStatusE(String dialStatusE) {
        this.dialStatusE = dialStatusE;
    }

    public String getLeadOrder() {
        return leadOrder;
    }

    public void setLeadOrder(String leadOrder) {
        this.leadOrder = leadOrder;
    }

    public String getParkExt() {
        return parkExt;
    }

    public void setParkExt(String parkExt) {
        this.parkExt = parkExt;
    }

    public String getParkFileName() {
        return parkFileName;
    }

    public void setParkFileName(String parkFileName) {
        this.parkFileName = parkFileName;
    }

    public String getWebFormAddress() {
        return webFormAddress;
    }

    public void setWebFormAddress(String webFormAddress) {
        this.webFormAddress = webFormAddress;
    }

    public boolean isAllowClosers() {
        return allowClosers;
    }

    public void setAllowClosers(boolean allowClosers) {
        this.allowClosers = allowClosers;
    }

    public Integer getHopper_level() {
        return hopper_level;
    }

    public void setHopper_level(Integer hopper_level) {
        this.hopper_level = hopper_level;
    }

    public String getAutoDialLevel() {
        return autoDialLevel;
    }

    public void setAutoDialLevel(String autoDialLevel) {
        this.autoDialLevel = autoDialLevel;
    }

    public String getNextAgentCall() {
        return nextAgentCall;
    }

    public void setNextAgentCall(String nextAgentCall) {
        this.nextAgentCall = nextAgentCall;
    }

    public String getLocalCallTime() {
        return localCallTime;
    }

    public void setLocalCallTime(String localCallTime) {
        this.localCallTime = localCallTime;
    }

    public String getVoiceMailExtension() {
        return voiceMailExtension;
    }

    public void setVoiceMailExtension(String voiceMailExtension) {
        this.voiceMailExtension = voiceMailExtension;
    }

    public Integer getDialTimeout() {
        return dialTimeout;
    }

    public void setDialTimeout(Integer dialTimeout) {
        this.dialTimeout = dialTimeout;
    }

    public String getDialPrefix() {
        return dialPrefix;
    }

    public void setDialPrefix(String dialPrefix) {
        this.dialPrefix = dialPrefix;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getVdadExtension() {
        return vdadExtension;
    }

    public void setVdadExtension(String vdadExtension) {
        this.vdadExtension = vdadExtension;
    }

    public String getRecordingExtension() {
        return recordingExtension;
    }

    public void setRecordingExtension(String recordingExtension) {
        this.recordingExtension = recordingExtension;
    }

    public String getRecordingRule() {
        return recordingRule;
    }

    public void setRecordingRule(String recordingRule) {
        this.recordingRule = recordingRule;
    }

    public String getRecording() {
        return recording;
    }

    public void setRecording(String recording) {
        this.recording = recording;
    }

    public String getRecordingFileName() {
        return recordingFileName;
    }

    public void setRecordingFileName(String recordingFileName) {
        this.recordingFileName = recordingFileName;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public String getGetCallLaunch() {
        return getCallLaunch;
    }

    public void setGetCallLaunch(String getCallLaunch) {
        this.getCallLaunch = getCallLaunch;
    }

    public String getAmMessageExtension() {
        return amMessageExtension;
    }

    public void setAmMessageExtension(String amMessageExtension) {
        this.amMessageExtension = amMessageExtension;
    }

    public String getAmdSendToVmx() {
        return amdSendToVmx;
    }

    public void setAmdSendToVmx(String amdSendToVmx) {
        this.amdSendToVmx = amdSendToVmx;
    }

    public String getXferConfADtmf() {
        return xferConfADtmf;
    }

    public void setXferConfADtmf(String xferConfADtmf) {
        this.xferConfADtmf = xferConfADtmf;
    }

    public String getXferConfANumber() {
        return xferConfANumber;
    }

    public void setXferConfANumber(String xferConfANumber) {
        this.xferConfANumber = xferConfANumber;
    }

    public String getXferConfBDtmf() {
        return xferConfBDtmf;
    }

    public void setXferConfBDtmf(String xferConfBDtmf) {
        this.xferConfBDtmf = xferConfBDtmf;
    }

    public String getXferConfBNumber() {
        return xferConfBNumber;
    }

    public void setXferConfBNumber(String xferConfBNumber) {
        this.xferConfBNumber = xferConfBNumber;
    }

    public boolean isAltNumberDialing() {
        return altNumberDialing;
    }

    public void setAltNumberDialing(boolean altNumberDialing) {
        this.altNumberDialing = altNumberDialing;
    }

    public boolean isAllowScheduledCallBacks() {
        return allowScheduledCallBacks;
    }

    public void setAllowScheduledCallBacks(boolean allowScheduledCallBacks) {
        this.allowScheduledCallBacks = allowScheduledCallBacks;
    }

    public String getLeadFilterId() {
        return leadFilterId;
    }

    public void setLeadFilterId(String leadFilterId) {
        this.leadFilterId = leadFilterId;
    }

    public Integer getDropCallSeconds() {
        return dropCallSeconds;
    }

    public void setDropCallSeconds(Integer dropCallSeconds) {
        this.dropCallSeconds = dropCallSeconds;
    }

    public boolean isAllowSafeHarborMessage() {
        return allowSafeHarborMessage;
    }

    public void setAllowSafeHarborMessage(boolean allowSafeHarborMessage) {
        this.allowSafeHarborMessage = allowSafeHarborMessage;
    }

    public boolean isAllowSafeHarborExtension() {
        return allowSafeHarborExtension;
    }

    public void setAllowSafeHarborExtension(boolean allowSafeHarborExtension) {
        this.allowSafeHarborExtension = allowSafeHarborExtension;
    }

    public boolean isAllowDisplayDialableCount() {
        return allowDisplayDialableCount;
    }

    public void setAllowDisplayDialableCount(boolean allowDisplayDialableCount) {
        this.allowDisplayDialableCount = allowDisplayDialableCount;
    }

    public Integer getWrapUpSeconds() {
        return wrapUpSeconds;
    }

    public void setWrapUpSeconds(Integer wrapUpSeconds) {
        this.wrapUpSeconds = wrapUpSeconds;
    }

    public String getWrapUpMessage() {
        return wrapUpMessage;
    }

    public void setWrapUpMessage(String wrapUpMessage) {
        this.wrapUpMessage = wrapUpMessage;
    }

    public String getCloserCampaigns() {
        return closerCampaigns;
    }

    public void setCloserCampaigns(String closerCampaigns) {
        this.closerCampaigns = closerCampaigns;
    }

    public boolean isAllowUseInternalDnc() {
        return allowUseInternalDnc;
    }

    public void setAllowUseInternalDnc(boolean allowUseInternalDnc) {
        this.allowUseInternalDnc = allowUseInternalDnc;
    }

    public Integer getAllCallsDelay() {
        return allCallsDelay;
    }

    public void setAllCallsDelay(Integer allCallsDelay) {
        this.allCallsDelay = allCallsDelay;
    }

    public boolean isOmitPhoneCode() {
        return omitPhoneCode;
    }

    public void setOmitPhoneCode(boolean omitPhoneCode) {
        this.omitPhoneCode = omitPhoneCode;
    }

    public String getDialMethod() {
        return dialMethod;
    }

    public void setDialMethod(String dialMethod) {
        this.dialMethod = dialMethod;
    }

    public boolean isAvailableOnlyRatioTally() {
        return availableOnlyRatioTally;
    }

    public void setAvailableOnlyRatioTally(boolean availableOnlyRatioTally) {
        this.availableOnlyRatioTally = availableOnlyRatioTally;
    }

    public Integer getAdaptiveDroppedPercentage() {
        return adaptiveDroppedPercentage;
    }

    public void setAdaptiveDroppedPercentage(Integer adaptiveDroppedPercentage) {
        this.adaptiveDroppedPercentage = adaptiveDroppedPercentage;
    }

    public String getAdaptiveMaximumLevel() {
        return adaptiveMaximumLevel;
    }

    public void setAdaptiveMaximumLevel(String adaptiveMaximumLevel) {
        this.adaptiveMaximumLevel = adaptiveMaximumLevel;
    }

    public String getAdaptiveLatestServerTime() {
        return adaptiveLatestServerTime;
    }

    public void setAdaptiveLatestServerTime(String adaptiveLatestServerTime) {
        this.adaptiveLatestServerTime = adaptiveLatestServerTime;
    }

    public String getAdaptiveIntensity() {
        return adaptiveIntensity;
    }

    public void setAdaptiveIntensity(String adaptiveIntensity) {
        this.adaptiveIntensity = adaptiveIntensity;
    }

    public Integer getAdaptiveDlDiffTarget() {
        return adaptiveDlDiffTarget;
    }

    public void setAdaptiveDlDiffTarget(Integer adaptiveDlDiffTarget) {
        this.adaptiveDlDiffTarget = adaptiveDlDiffTarget;
    }

    public String getConcurrentTransfers() {
        return concurrentTransfers;
    }

    public void setConcurrentTransfers(String concurrentTransfers) {
        this.concurrentTransfers = concurrentTransfers;
    }

    public String getAutoAltDial() {
        return autoAltDial;
    }

    public void setAutoAltDial(String autoAltDial) {
        this.autoAltDial = autoAltDial;
    }

    public String getAutoAltDialStatuses() {
        return AutoAltDialStatuses;
    }

    public void setAutoAltDialStatuses(String autoAltDialStatuses) {
        AutoAltDialStatuses = autoAltDialStatuses;
    }

    public boolean isAgentPauseCodesActive() {
        return agentPauseCodesActive;
    }

    public void setAgentPauseCodesActive(boolean agentPauseCodesActive) {
        this.agentPauseCodesActive = agentPauseCodesActive;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getChangeDateTime() {
        return changeDateTime;
    }

    public void setChangeDateTime(LocalDateTime changeDateTime) {
        this.changeDateTime = changeDateTime;
    }

    public boolean isStatsRefresh() {
        return statsRefresh;
    }

    public void setStatsRefresh(boolean statsRefresh) {
        this.statsRefresh = statsRefresh;
    }

    public LocalDateTime getLoginDateTime() {
        return loginDateTime;
    }

    public void setLoginDateTime(LocalDateTime loginDateTime) {
        this.loginDateTime = loginDateTime;
    }

    public String getDialStatuses() {
        return dialStatuses;
    }

    public void setDialStatuses(String dialStatuses) {
        this.dialStatuses = dialStatuses;
    }

    public boolean isDisableAlterCustData() {
        return disableAlterCustData;
    }

    public void setDisableAlterCustData(boolean disableAlterCustData) {
        this.disableAlterCustData = disableAlterCustData;
    }

    public boolean isNoHopperLeadsLogins() {
        return noHopperLeadsLogins;
    }

    public void setNoHopperLeadsLogins(boolean noHopperLeadsLogins) {
        this.noHopperLeadsLogins = noHopperLeadsLogins;
    }

    public String getListOrderMix() {
        return listOrderMix;
    }

    public void setListOrderMix(String listOrderMix) {
        this.listOrderMix = listOrderMix;
    }

    public boolean isAllowCampaignInbound() {
        return allowCampaignInbound;
    }

    public void setAllowCampaignInbound(boolean allowCampaignInbound) {
        this.allowCampaignInbound = allowCampaignInbound;
    }

    public Integer getManualDialListId() {
        return manualDialListId;
    }

    public void setManualDialListId(Integer manualDialListId) {
        this.manualDialListId = manualDialListId;
    }

    public String getDefaultXferGroup() {
        return defaultXferGroup;
    }

    public void setDefaultXferGroup(String defaultXferGroup) {
        this.defaultXferGroup = defaultXferGroup;
    }

    public String getXferGroups() {
        return xferGroups;
    }

    public void setXferGroups(String xferGroups) {
        this.xferGroups = xferGroups;
    }

    public String getWebFormAddress2() {
        return webFormAddress2;
    }

    public void setWebFormAddress2(String webFormAddress2) {
        this.webFormAddress2 = webFormAddress2;
    }

    public boolean isAllowTabSwitch() {
        return allowTabSwitch;
    }

    public void setAllowTabSwitch(boolean allowTabSwitch) {
        this.allowTabSwitch = allowTabSwitch;
    }

    public Integer getAnswersPerHourLimit() {
        return answersPerHourLimit;
    }

    public void setAnswersPerHourLimit(Integer answersPerHourLimit) {
        this.answersPerHourLimit = answersPerHourLimit;
    }

    public String getCampaignCallTime() {
        return campaignCallTime;
    }

    public void setCampaignCallTime(String campaignCallTime) {
        this.campaignCallTime = campaignCallTime;
    }

    public Integer getPreviewForceDialTime() {
        return previewForceDialTime;
    }

    public void setPreviewForceDialTime(Integer previewForceDialTime) {
        this.previewForceDialTime = previewForceDialTime;
    }

    public boolean isManualPreviewDefault() {
        return manualPreviewDefault;
    }

    public void setManualPreviewDefault(boolean manualPreviewDefault) {
        this.manualPreviewDefault = manualPreviewDefault;
    }

    public boolean isWebFormExternalWindow() {
        return webFormExternalWindow;
    }

    public void setWebFormExternalWindow(boolean webFormExternalWindow) {
        this.webFormExternalWindow = webFormExternalWindow;
    }

    public boolean isWebForm2ExternalWindow() {
        return webForm2ExternalWindow;
    }

    public void setWebForm2ExternalWindow(boolean webForm2ExternalWindow) {
        this.webForm2ExternalWindow = webForm2ExternalWindow;
    }

    public String getSubmitMethod() {
        return submitMethod;
    }

    public void setSubmitMethod(String submitMethod) {
        this.submitMethod = submitMethod;
    }

    public boolean isUseCustom2CallierId() {
        return useCustom2CallierId;
    }

    public void setUseCustom2CallierId(boolean useCustom2CallierId) {
        this.useCustom2CallierId = useCustom2CallierId;
    }

    public LocalDateTime getCampaignLastCall() {
        return campaignLastCall;
    }

    public void setCampaignLastCall(LocalDateTime campaignLastCall) {
        this.campaignLastCall = campaignLastCall;
    }

    public String getCampaignCidName() {
        return campaignCidName;
    }

    public void setCampaignCidName(String campaignCidName) {
        this.campaignCidName = campaignCidName;
    }

    public String getXferCidMode() {
        return xferCidMode;
    }

    public void setXferCidMode(String xferCidMode) {
        this.xferCidMode = xferCidMode;
    }

    public boolean isUseCidAreaCodeMap() {
        return useCidAreaCodeMap;
    }

    public void setUseCidAreaCodeMap(boolean useCidAreaCodeMap) {
        this.useCidAreaCodeMap = useCidAreaCodeMap;
    }

    public Integer getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(Integer carrierId) {
        this.carrierId = carrierId;
    }

    public String getEmailTemplate() {
        return emailTemplate;
    }

    public void setEmailTemplate(String emailTemplate) {
        this.emailTemplate = emailTemplate;
    }

    public boolean isDisableManualDial() {
        return disableManualDial;
    }

    public void setDisableManualDial(boolean disableManualDial) {
        this.disableManualDial = disableManualDial;
    }

    public boolean isHideXferLocalCloser() {
        return hideXferLocalCloser;
    }

    public void setHideXferLocalCloser(boolean hideXferLocalCloser) {
        this.hideXferLocalCloser = hideXferLocalCloser;
    }

    public boolean isHideXferDialOverride() {
        return hideXferDialOverride;
    }

    public void setHideXferDialOverride(boolean hideXferDialOverride) {
        this.hideXferDialOverride = hideXferDialOverride;
    }

    public boolean isHideXferHangupXfer() {
        return hideXferHangupXfer;
    }

    public void setHideXferHangupXfer(boolean hideXferHangupXfer) {
        this.hideXferHangupXfer = hideXferHangupXfer;
    }

    public boolean isHideXferLeave3way() {
        return hideXferLeave3way;
    }

    public void setHideXferLeave3way(boolean hideXferLeave3way) {
        this.hideXferLeave3way = hideXferLeave3way;
    }

    public boolean isHideXferDialWith() {
        return hideXferDialWith;
    }

    public void setHideXferDialWith(boolean hideXferDialWith) {
        this.hideXferDialWith = hideXferDialWith;
    }

    public boolean isHideXferHangupBoth() {
        return hideXferHangupBoth;
    }

    public void setHideXferHangupBoth(boolean hideXferHangupBoth) {
        this.hideXferHangupBoth = hideXferHangupBoth;
    }

    public boolean isHideXferBlindXfer() {
        return hideXferBlindXfer;
    }

    public void setHideXferBlindXfer(boolean hideXferBlindXfer) {
        this.hideXferBlindXfer = hideXferBlindXfer;
    }

    public boolean isHideXferParkDial() {
        return hideXferParkDial;
    }

    public void setHideXferParkDial(boolean hideXferParkDial) {
        this.hideXferParkDial = hideXferParkDial;
    }

    public boolean isHideXferBlindVmail() {
        return hideXferBlindVmail;
    }

    public void setHideXferBlindVmail(boolean hideXferBlindVmail) {
        this.hideXferBlindVmail = hideXferBlindVmail;
    }

    public boolean isAllowMdHopperList() {
        return allowMdHopperList;
    }

    public void setAllowMdHopperList(boolean allowMdHopperList) {
        this.allowMdHopperList = allowMdHopperList;
    }

    public Integer getIvrId() {
        return ivrId;
    }

    public void setIvrId(Integer ivrId) {
        this.ivrId = ivrId;
    }
}
