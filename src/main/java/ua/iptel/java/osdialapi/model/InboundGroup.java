package ua.iptel.java.osdialapi.model;

import ua.iptel.java.osdialapi.model.validator.ValidateString;

import java.io.Serializable;

public class InboundGroup implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    private String color;
    private boolean active;
    private String webFormAddress;
    private String voicemailExtension;
    @ValidateString(acceptedValues={"random", "oldest_call_start", "oldest_call_finish", "overall_user_level",
                                    "inbound_group_rank", "campaign_rank", "fewest_calls", "per_user"},
            message="Allowed field values: 'random', 'oldest_call_start', 'oldest_call_finish'," +
                    "'overall_user_level', 'inbound_group_rank', 'campaign_rank', 'fewest_calls', 'per_user'")
    private String nextAgentCall;
    private boolean allowFronterDisplay;
    private String inboundScript;
    @ValidateString(acceptedValues={"NONE", "SCRIPT", "WEBFORM", "WEBFORM2"},
            message="Allowed field values: 'NONE', 'SCRIPT', 'WEBFORM', 'WEBFORM2'")
    private String getCallLaunch;
    private String xferConfADtmf;
    private String xferConfANumber;
    private String xferConfBDtmf;
    private String xferConfBNumber;
    private Integer dropCallSeconds;
    @ValidateString(acceptedValues={"HANGUP", "MESSAGE", "EXTENSION", "VOICEMAIL", "CALlBACK"},
            message="Allowed field values: 'HANGUP', 'MESSAGE', 'EXTENSION', 'VOICEMAIL', 'CALlBACK'")
    private String dropAction;
    private String dropExtension;
    private Long callTimeId;
    @ValidateString(acceptedValues={"HANGUP", "MESSAGE", "EXTENSION", "VOICEMAIL", "CALlBACK"},
            message="Allowed field values: 'HANGUP', 'MESSAGE', 'EXTENSION', 'VOICEMAIL', 'CALlBACK'")
    private String afterHoursAction;
    private String afterHoursMessageFilename;
    private String afterHoursExtension;
    private String afterHoursVoiceMail;
    private String welcomeMessageFilename;
    private String mohContext;
    private String onHoldPromptFilename;
    private Integer promptInterval;
    private String agentAlertExtension;
    private Integer agentAlertDelay;
    private String defaultXferGroup;
    private String webFormAddress2;
    private boolean allowTabSwitch;
    private boolean allowWebFormExternalWindow;
    private boolean allowWebForm2ExternalWindow;
    @ValidateString(acceptedValues={"CALL_SECONDS_TIMEOUT", "NO_AGENTS_CONNECTED", "NO_AGENTS_AVAILABLE"},
            message="Allowed field values: 'CALL_SECONDS_TIMEOUT', 'NO_AGENTS_CONNECTED', 'NO_AGENTS_AVAILABLE'")
    private String dropTrigger;
    private boolean allowMultiCall;
    private Integer placementInterval;
    private Integer placementMaxRepeat;
    private Integer queueTimeInterval;
    private Integer queueMaxRepeat;
    private String backgroundMusicFilename;
    private String dropMessageFilename;
    private Integer callbackInterval;
    private String callbackIntervalKey;
    private Integer onHoldStartDelay;
    private Integer callbackStartDelay;
    private Integer placementStartDelay;
    private Integer queueTimeStartDelay;
    private String promptLanguage;
    private String welcomeMessageMinPlayTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getWebFormAddress() {
        return webFormAddress;
    }

    public void setWebFormAddress(String webFormAddress) {
        this.webFormAddress = webFormAddress;
    }

    public String getVoicemailExtension() {
        return voicemailExtension;
    }

    public void setVoicemailExtension(String voicemailExtension) {
        this.voicemailExtension = voicemailExtension;
    }

    public String getNextAgentCall() {
        return nextAgentCall;
    }

    public void setNextAgentCall(String nextAgentCall) {
        this.nextAgentCall = nextAgentCall;
    }

    public boolean isAllowFronterDisplay() {
        return allowFronterDisplay;
    }

    public void setAllowFronterDisplay(boolean allowFronterDisplay) {
        this.allowFronterDisplay = allowFronterDisplay;
    }

    public String getInboundScript() {
        return inboundScript;
    }

    public void setInboundScript(String inboundScript) {
        this.inboundScript = inboundScript;
    }

    public String getGetCallLaunch() {
        return getCallLaunch;
    }

    public void setGetCallLaunch(String getCallLaunch) {
        this.getCallLaunch = getCallLaunch;
    }

    public String getXferConfADtmf() {
        return xferConfADtmf;
    }

    public void setXferConfADtmf(String xferConfADtmf) {
        this.xferConfADtmf = xferConfADtmf;
    }

    public String getXferConfANumber() {
        return xferConfANumber;
    }

    public void setXferConfANumber(String xferConfANumber) {
        this.xferConfANumber = xferConfANumber;
    }

    public String getXferConfBDtmf() {
        return xferConfBDtmf;
    }

    public void setXferConfBDtmf(String xferConfBDtmf) {
        this.xferConfBDtmf = xferConfBDtmf;
    }

    public String getXferConfBNumber() {
        return xferConfBNumber;
    }

    public void setXferConfBNumber(String xferConfBNumber) {
        this.xferConfBNumber = xferConfBNumber;
    }

    public Integer getDropCallSeconds() {
        return dropCallSeconds;
    }

    public void setDropCallSeconds(Integer dropCallSeconds) {
        this.dropCallSeconds = dropCallSeconds;
    }

    public String getDropAction() {
        return dropAction;
    }

    public void setDropAction(String dropAction) {
        this.dropAction = dropAction;
    }

    public String getDropExtension() {
        return dropExtension;
    }

    public void setDropExtension(String dropExtension) {
        this.dropExtension = dropExtension;
    }

    public Long getCallTimeId() {
        return callTimeId;
    }

    public void setCallTimeId(Long callTimeId) {
        this.callTimeId = callTimeId;
    }

    public String getAfterHoursAction() {
        return afterHoursAction;
    }

    public void setAfterHoursAction(String afterHoursAction) {
        this.afterHoursAction = afterHoursAction;
    }

    public String getAfterHoursMessageFilename() {
        return afterHoursMessageFilename;
    }

    public void setAfterHoursMessageFilename(String afterHoursMessageFilename) {
        this.afterHoursMessageFilename = afterHoursMessageFilename;
    }

    public String getAfterHoursExtension() {
        return afterHoursExtension;
    }

    public void setAfterHoursExtension(String afterHoursExtension) {
        this.afterHoursExtension = afterHoursExtension;
    }

    public String getAfterHoursVoiceMail() {
        return afterHoursVoiceMail;
    }

    public void setAfterHoursVoiceMail(String afterHoursVoiceMail) {
        this.afterHoursVoiceMail = afterHoursVoiceMail;
    }

    public String getWelcomeMessageFilename() {
        return welcomeMessageFilename;
    }

    public void setWelcomeMessageFilename(String welcomeMessageFilename) {
        this.welcomeMessageFilename = welcomeMessageFilename;
    }

    public String getMohContext() {
        return mohContext;
    }

    public void setMohContext(String mohContext) {
        this.mohContext = mohContext;
    }

    public String getOnHoldPromptFilename() {
        return onHoldPromptFilename;
    }

    public void setOnHoldPromptFilename(String onHoldPromptFilename) {
        this.onHoldPromptFilename = onHoldPromptFilename;
    }

    public Integer getPromptInterval() {
        return promptInterval;
    }

    public void setPromptInterval(Integer promptInterval) {
        this.promptInterval = promptInterval;
    }

    public String getAgentAlertExtension() {
        return agentAlertExtension;
    }

    public void setAgentAlertExtension(String agentAlertExtension) {
        this.agentAlertExtension = agentAlertExtension;
    }

    public Integer getAgentAlertDelay() {
        return agentAlertDelay;
    }

    public void setAgentAlertDelay(Integer agentAlertDelay) {
        this.agentAlertDelay = agentAlertDelay;
    }

    public String getDefaultXferGroup() {
        return defaultXferGroup;
    }

    public void setDefaultXferGroup(String defaultXferGroup) {
        this.defaultXferGroup = defaultXferGroup;
    }

    public String getWebFormAddress2() {
        return webFormAddress2;
    }

    public void setWebFormAddress2(String webFormAddress2) {
        this.webFormAddress2 = webFormAddress2;
    }

    public boolean isAllowTabSwitch() {
        return allowTabSwitch;
    }

    public void setAllowTabSwitch(boolean allowTabSwitch) {
        this.allowTabSwitch = allowTabSwitch;
    }

    public boolean isAllowWebFormExternalWindow() {
        return allowWebFormExternalWindow;
    }

    public void setAllowWebFormExternalWindow(boolean allowWebFormExternalWindow) {
        this.allowWebFormExternalWindow = allowWebFormExternalWindow;
    }

    public boolean isAllowWebForm2ExternalWindow() {
        return allowWebForm2ExternalWindow;
    }

    public void setAllowWebForm2ExternalWindow(boolean allowWebForm2ExternalWindow) {
        this.allowWebForm2ExternalWindow = allowWebForm2ExternalWindow;
    }

    public String getDropTrigger() {
        return dropTrigger;
    }

    public void setDropTrigger(String dropTrigger) {
        this.dropTrigger = dropTrigger;
    }

    public boolean isAllowMultiCall() {
        return allowMultiCall;
    }

    public void setAllowMultiCall(boolean allowMultiCall) {
        this.allowMultiCall = allowMultiCall;
    }

    public Integer getPlacementInterval() {
        return placementInterval;
    }

    public void setPlacementInterval(Integer placementInterval) {
        this.placementInterval = placementInterval;
    }

    public Integer getPlacementMaxRepeat() {
        return placementMaxRepeat;
    }

    public void setPlacementMaxRepeat(Integer placementMaxRepeat) {
        this.placementMaxRepeat = placementMaxRepeat;
    }

    public Integer getQueueTimeInterval() {
        return queueTimeInterval;
    }

    public void setQueueTimeInterval(Integer queueTimeInterval) {
        this.queueTimeInterval = queueTimeInterval;
    }

    public Integer getQueueMaxRepeat() {
        return queueMaxRepeat;
    }

    public void setQueueMaxRepeat(Integer queueMaxRepeat) {
        this.queueMaxRepeat = queueMaxRepeat;
    }

    public String getBackgroundMusicFilename() {
        return backgroundMusicFilename;
    }

    public void setBackgroundMusicFilename(String backgroundMusicFilename) {
        this.backgroundMusicFilename = backgroundMusicFilename;
    }

    public String getDropMessageFilename() {
        return dropMessageFilename;
    }

    public void setDropMessageFilename(String dropMessageFilename) {
        this.dropMessageFilename = dropMessageFilename;
    }

    public Integer getCallbackInterval() {
        return callbackInterval;
    }

    public void setCallbackInterval(Integer callbackInterval) {
        this.callbackInterval = callbackInterval;
    }

    public String getCallbackIntervalKey() {
        return callbackIntervalKey;
    }

    public void setCallbackIntervalKey(String callbackIntervalKey) {
        this.callbackIntervalKey = callbackIntervalKey;
    }

    public Integer getOnHoldStartDelay() {
        return onHoldStartDelay;
    }

    public void setOnHoldStartDelay(Integer onHoldStartDelay) {
        this.onHoldStartDelay = onHoldStartDelay;
    }

    public Integer getCallbackStartDelay() {
        return callbackStartDelay;
    }

    public void setCallbackStartDelay(Integer callbackStartDelay) {
        this.callbackStartDelay = callbackStartDelay;
    }

    public Integer getPlacementStartDelay() {
        return placementStartDelay;
    }

    public void setPlacementStartDelay(Integer placementStartDelay) {
        this.placementStartDelay = placementStartDelay;
    }

    public Integer getQueueTimeStartDelay() {
        return queueTimeStartDelay;
    }

    public void setQueueTimeStartDelay(Integer queueTimeStartDelay) {
        this.queueTimeStartDelay = queueTimeStartDelay;
    }

    public String getPromptLanguage() {
        return promptLanguage;
    }

    public void setPromptLanguage(String promptLanguage) {
        this.promptLanguage = promptLanguage;
    }

    public String getWelcomeMessageMinPlayTime() {
        return welcomeMessageMinPlayTime;
    }

    public void setWelcomeMessageMinPlayTime(String welcomeMessageMinPlayTime) {
        this.welcomeMessageMinPlayTime = welcomeMessageMinPlayTime;
    }
}
