package ua.iptel.java.osdialapi.model.report.agent;

import io.swagger.models.auth.In;
import ua.iptel.java.osdialapi.model.dto.UserDto;

import java.io.Serializable;

public class AgentCallsDetailedReport implements Serializable {
    private static final long serialVersionUID = 1L;

    private UserDto user;
    private Integer callsCount;
    private Integer totalTime;
    private Integer pauseTotalTime;
    private Integer pauseAverageTime;
    private Integer waitTotalTime;
    private Integer averageWaitTime;
    private Integer callDurationTime;
    private Integer callDurationAverageTime;
    private Integer dispoTime;
    private Integer averageDispoTime;

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    public Integer getCallsCount() {
        return callsCount;
    }

    public void setCallsCount(Integer callsCount) {
        this.callsCount = callsCount;
    }

    public Integer getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(Integer totalTime) {
        this.totalTime = totalTime;
    }

    public Integer getPauseTotalTime() {
        return pauseTotalTime;
    }

    public void setPauseTotalTime(Integer pauseTotalTime) {
        this.pauseTotalTime = pauseTotalTime;
    }

    public Integer getPauseAverageTime() {
        return pauseAverageTime;
    }

    public void setPauseAverageTime(Integer pauseAverageTime) {
        this.pauseAverageTime = pauseAverageTime;
    }

    public Integer getWaitTotalTime() {
        return waitTotalTime;
    }

    public void setWaitTotalTime(Integer waitTotalTime) {
        this.waitTotalTime = waitTotalTime;
    }

    public Integer getAverageWaitTime() {
        return averageWaitTime;
    }

    public void setAverageWaitTime(Integer averageWaitTime) {
        this.averageWaitTime = averageWaitTime;
    }

    public Integer getCallDurationTime() {
        return callDurationTime;
    }

    public void setCallDurationTime(Integer callDurationTime) {
        this.callDurationTime = callDurationTime;
    }

    public Integer getCallDurationAverageTime() {
        return callDurationAverageTime;
    }

    public void setCallDurationAverageTime(Integer callDurationAverageTime) {
        this.callDurationAverageTime = callDurationAverageTime;
    }

    public Integer getDispoTime() {
        return dispoTime;
    }

    public void setDispoTime(Integer dispoTime) {
        this.dispoTime = dispoTime;
    }

    public Integer getAverageDispoTime() {
        return averageDispoTime;
    }

    public void setAverageDispoTime(Integer averageDispoTime) {
        this.averageDispoTime = averageDispoTime;
    }
}
