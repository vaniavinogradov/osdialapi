package ua.iptel.java.osdialapi.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class CallLog implements Serializable {
    protected Long id;
    protected String uniqueId;
    protected Long leadId;
    protected Long listId;
    protected Long campaingId;
    protected LocalDateTime callDateTime;
    protected Integer length;
    protected String status;
    protected String phoneCode;
    protected String phoneNumber;
    protected String user;
    protected String comments;
    protected String userGroup;
    protected String termReason;
    protected String callerId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Long getLeadId() {
        return leadId;
    }

    public void setLeadId(Long leadId) {
        this.leadId = leadId;
    }

    public Long getListId() {
        return listId;
    }

    public void setListId(Long listId) {
        this.listId = listId;
    }

    public Long getCampaingId() {
        return campaingId;
    }

    public void setCampaingId(Long campaingId) {
        this.campaingId = campaingId;
    }

    public LocalDateTime getCallDateTime() {
        return callDateTime;
    }

    public void setCallDateTime(LocalDateTime callDateTime) {
        this.callDateTime = callDateTime;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(String userGroup) {
        this.userGroup = userGroup;
    }

    public String getTermReason() {
        return termReason;
    }

    public void setTermReason(String termReason) {
        this.termReason = termReason;
    }

    public String getCallerId() {
        return callerId;
    }

    public void setCallerId(String callerId) {
        this.callerId = callerId;
    }
}
