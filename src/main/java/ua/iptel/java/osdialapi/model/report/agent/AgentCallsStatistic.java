package ua.iptel.java.osdialapi.model.report.agent;

import ua.iptel.java.osdialapi.model.dto.CampaignDto;
import ua.iptel.java.osdialapi.model.dto.LeadDto;
import ua.iptel.java.osdialapi.model.dto.ListDto;

import java.io.Serializable;
import java.time.LocalDateTime;

public class AgentCallsStatistic implements Serializable {
    private static final long serialVersionUID = 1L;

    private LocalDateTime callDateTime;
    private Integer waitTime;
    private Integer callDurationTime;
    private Integer dispoTime;
    private Integer pauseTime;
    private String status;
    private String phoneNumber;
    private CampaignDto campaign;
    private ListDto list;
    private LeadDto lead;

    public LocalDateTime getCallDateTime() {
        return callDateTime;
    }

    public void setCallDateTime(LocalDateTime callDateTime) {
        this.callDateTime = callDateTime;
    }

    public Integer getWaitTime() {
        return waitTime;
    }

    public void setWaitTime(Integer waitTime) {
        this.waitTime = waitTime;
    }

    public Integer getCallDurationTime() {
        return callDurationTime;
    }

    public void setCallDurationTime(Integer callDurationTime) {
        this.callDurationTime = callDurationTime;
    }

    public Integer getDispoTime() {
        return dispoTime;
    }

    public void setDispoTime(Integer dispoTime) {
        this.dispoTime = dispoTime;
    }

    public Integer getPauseTime() {
        return pauseTime;
    }

    public void setPauseTime(Integer pauseTime) {
        this.pauseTime = pauseTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public CampaignDto getCampaign() {
        return campaign;
    }

    public void setCampaign(CampaignDto campaign) {
        this.campaign = campaign;
    }

    public ListDto getList() {
        return list;
    }

    public void setList(ListDto list) {
        this.list = list;
    }

    public LeadDto getLead() {
        return lead;
    }

    public void setLead(LeadDto lead) {
        this.lead = lead;
    }
}
