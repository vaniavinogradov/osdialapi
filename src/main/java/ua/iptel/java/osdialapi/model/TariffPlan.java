package ua.iptel.java.osdialapi.model;

import java.time.LocalDateTime;

public class TariffPlan {
    private Integer id;
    private String name;
    private Integer agentsLimit;
    private Integer virtualAgentsLimit;
    private Integer leadsLimit;
    private String defaultLanguage;
    private LocalDateTime paymentDateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAgentsLimit() {
        return agentsLimit;
    }

    public void setAgentsLimit(Integer agentsLimit) {
        this.agentsLimit = agentsLimit;
    }

    public Integer getVirtualAgentsLimit() {
        return virtualAgentsLimit;
    }

    public void setVirtualAgentsLimit(Integer virtualAgentsLimit) {
        this.virtualAgentsLimit = virtualAgentsLimit;
    }

    public Integer getLeadsLimit() {
        return leadsLimit;
    }

    public void setLeadsLimit(Integer leadsLimit) {
        this.leadsLimit = leadsLimit;
    }

    public String getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(String defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

    public LocalDateTime getPaymentDateTime() {
        return paymentDateTime;
    }

    public void setPaymentDateTime(LocalDateTime paymentDateTime) {
        this.paymentDateTime = paymentDateTime;
    }
}
