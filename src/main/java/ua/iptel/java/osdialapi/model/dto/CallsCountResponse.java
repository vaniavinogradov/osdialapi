package ua.iptel.java.osdialapi.model.dto;

public class CallsCountResponse {
    private String callStatus;
    private Integer calledCallsCount;
    private Integer notCalledCallsCount;

    public String getCallStatus() {
        return callStatus;
    }

    public void setCallStatus(String callStatus) {
        this.callStatus = callStatus;
    }

    public Integer getCalledCallsCount() {
        return calledCallsCount;
    }

    public void setCalledCallsCount(Integer calledCallsCount) {
        this.calledCallsCount = calledCallsCount;
    }

    public Integer getNotCalledCallsCount() {
        return notCalledCallsCount;
    }

    public void setNotCalledCallsCount(Integer notCalledCallsCount) {
        this.notCalledCallsCount = notCalledCallsCount;
    }
}
