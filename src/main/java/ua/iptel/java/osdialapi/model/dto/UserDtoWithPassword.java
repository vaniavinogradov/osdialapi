package ua.iptel.java.osdialapi.model.dto;

public class UserDtoWithPassword extends UserDto {
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
