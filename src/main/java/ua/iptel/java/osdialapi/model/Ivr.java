package ua.iptel.java.osdialapi.model;

import io.swagger.models.auth.In;

import java.io.Serializable;

public class Ivr implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    private String announcement;
    private Integer repeatLoops;
    private Integer waitLoops;
    private Integer waitTimeout;
    private String answeredStatus;
    private Integer virtualAgents;
    private String status;
    private String timeoutAction;
    private Integer reserveAgents;
    private boolean allowInbound;
    private boolean allowAgentExtensions;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(String announcement) {
        this.announcement = announcement;
    }

    public Integer getRepeatLoops() {
        return repeatLoops;
    }

    public void setRepeatLoops(Integer repeatLoops) {
        this.repeatLoops = repeatLoops;
    }

    public Integer getWaitLoops() {
        return waitLoops;
    }

    public void setWaitLoops(Integer waitLoops) {
        this.waitLoops = waitLoops;
    }

    public Integer getWaitTimeout() {
        return waitTimeout;
    }

    public void setWaitTimeout(Integer waitTimeout) {
        this.waitTimeout = waitTimeout;
    }

    public String getAnsweredStatus() {
        return answeredStatus;
    }

    public void setAnsweredStatus(String answeredStatus) {
        this.answeredStatus = answeredStatus;
    }

    public Integer getVirtualAgents() {
        return virtualAgents;
    }

    public void setVirtualAgents(Integer virtualAgents) {
        this.virtualAgents = virtualAgents;
    }

    public String getTimeoutAction() {
        return timeoutAction;
    }

    public void setTimeoutAction(String timeoutAction) {
        this.timeoutAction = timeoutAction;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getReserveAgents() {
        return reserveAgents;
    }

    public void setReserveAgents(Integer reserveAgents) {
        this.reserveAgents = reserveAgents;
    }

    public boolean isAllowInbound() {
        return allowInbound;
    }

    public void setAllowInbound(boolean allowInbound) {
        this.allowInbound = allowInbound;
    }

    public boolean isAllowAgentExtensions() {
        return allowAgentExtensions;
    }

    public void setAllowAgentExtensions(boolean allowAgentExtensions) {
        this.allowAgentExtensions = allowAgentExtensions;
    }
}
