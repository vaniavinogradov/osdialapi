package ua.iptel.java.osdialapi.model.report.pause;

import java.io.Serializable;

public class GeneralPausesReport implements Serializable {
    private static final long serialVersionUID = 1L;

    private String codePause;
    private Integer count;
    private Integer totalSeconds;
    private Integer totalTimeAverage;

    public String getCodePause() {
        return codePause;
    }

    public void setCodePause(String codePause) {
        this.codePause = codePause;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getTotalSeconds() {
        return totalSeconds;
    }

    public void setTotalSeconds(Integer totalSeconds) {
        this.totalSeconds = totalSeconds;
    }

    public Integer getTotalTimeAverage() {
        return totalTimeAverage;
    }

    public void setTotalTimeAverage(Integer totalTimeAverage) {
        this.totalTimeAverage = totalTimeAverage;
    }
}
