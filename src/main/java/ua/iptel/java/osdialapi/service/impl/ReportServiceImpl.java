package ua.iptel.java.osdialapi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.iptel.java.osdialapi.dao.osdial.ReportDao;
import ua.iptel.java.osdialapi.model.CallLog;
import ua.iptel.java.osdialapi.model.Recording;
import ua.iptel.java.osdialapi.model.dto.CallLogExtended;
import ua.iptel.java.osdialapi.model.report.agent.AgentCallsDetailedReport;
import ua.iptel.java.osdialapi.model.report.agent.AgentCallsStatistic;
import ua.iptel.java.osdialapi.model.report.agent.AgentStatusesDetailedReport;
import ua.iptel.java.osdialapi.model.report.pause.AgentPausesReport;
import ua.iptel.java.osdialapi.model.report.pause.CampaignPausesReport;
import ua.iptel.java.osdialapi.model.report.pause.DetailedPausesReport;
import ua.iptel.java.osdialapi.model.report.pause.GeneralPausesReport;
import ua.iptel.java.osdialapi.service.ReportService;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class ReportServiceImpl implements ReportService {
    @Autowired
    private ReportDao reportDao;


    @Override
    public List<GeneralPausesReport> getGeneralPausesReport(LocalDateTime from, LocalDateTime to, String agent, String campaigns) {
        return reportDao.getGeneralPausesReport(from, to, agent, campaigns);
    }

    @Override
    public List<CampaignPausesReport> getCampaignPausesReport(LocalDateTime from, LocalDateTime to, String campaigns) {
        return reportDao.getCampaignPausesReport(from, to, campaigns);
    }

    @Override
    public List<AgentPausesReport> getAgentPausesReport(LocalDateTime from, LocalDateTime to, String agent, String campaigns) {
        return reportDao.getAgentPausesReport(from, to, agent, campaigns);
    }

    @Override
    public List<DetailedPausesReport> getDetailedPausesReport(LocalDateTime from, LocalDateTime to, String agent, String campaigns) {
        return reportDao.getDetailedPausesReport(from,to, agent, campaigns);
    }

    @Override
    public List<AgentCallsDetailedReport> getAgentCallsDetailedReport(LocalDateTime from, LocalDateTime to, String agent, String campaign, String userGroup) {
        return reportDao.getAgentCallsDetailedReport(from, to, agent, campaign, userGroup);
    }

    @Override
    public List<AgentStatusesDetailedReport> getAgentStatusesDetailedReport(LocalDateTime from, LocalDateTime to, String agent, String campaign, String userGroup) {
        return reportDao.getAgentStatusesDetailedReport(from, to, agent, campaign, userGroup);
    }

    @Override
    public List<AgentCallsStatistic> getAgentCallsStatistic(LocalDateTime from, LocalDateTime to, String agent, String type) {
        return reportDao.getAgentCallsStatistic(from, to, agent, type);
    }

    @Override
    public List<Recording> getAllRecordings() {
        return reportDao.getAllRecordings();
    }

    @Override
    public List<Recording> getRecordingByLeadId(Long leadId) {
        return reportDao.getRecordingByLeadId(leadId);
    }

    @Override
    public List<Recording> getRecordingsByPeriod(LocalDateTime from, LocalDateTime to) {
        return reportDao.getRecordingsByPeriod(from, to);
    }

    @Override
    public List<Recording> getRecordingsByPeriodAndUser(LocalDateTime from, LocalDateTime to, String user) {
        return reportDao.getRecordingsByPeriodAndUser(from, to, user);
    }

    @Override
    public List<CallLog> getCallsLog() {
        return reportDao.getCallsLog();
    }

    @Override
    public List<CallLogExtended> getCallsLogWitchLeadName() {
        return reportDao.getCallsLogWitchLeadName();
    }

    @Override
    public List<CallLog> getCallsLogByLeadId(Long leadId) {
        return reportDao.getCallsLogByLeadId(leadId);
    }

    @Override
    public List<CallLogExtended> getCallsLogByLeadIdWithLeadName(Long leadId) {
        return reportDao.getCallsLogByLeadIdWithLeadName(leadId);
    }

    @Override
    public List<CallLog> getCallLogByCampaignAndDateTimeRange(LocalDateTime from, LocalDateTime to, Integer campaignId) {
        return reportDao.getCallLogByCampaignAndDateTimeRange(from, to, campaignId);
    }

    @Override
    public List<CallLog> getCallLogByListId(Integer listId) {
        return reportDao.getCallLogByListId(listId);
    }
}
