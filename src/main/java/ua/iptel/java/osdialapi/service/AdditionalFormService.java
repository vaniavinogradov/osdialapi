package ua.iptel.java.osdialapi.service;

import ua.iptel.java.osdialapi.model.AdditionalForm;

import java.util.List;

public interface AdditionalFormService {
    List<AdditionalForm> getAll();
    AdditionalForm getById(Long id);
    AdditionalForm create(AdditionalForm additionalForm);
    AdditionalForm update(AdditionalForm additionalForm);
    Long delete(Long id);
}
