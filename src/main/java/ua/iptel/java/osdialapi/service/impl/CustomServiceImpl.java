package ua.iptel.java.osdialapi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.iptel.java.osdialapi.dao.CustomDao;
import ua.iptel.java.osdialapi.dao.exception.PersistException;
import ua.iptel.java.osdialapi.model.CallNotFoundException;
import ua.iptel.java.osdialapi.model.dto.CustomNumberResponse;
import ua.iptel.java.osdialapi.service.CustomService;

@Repository
public class CustomServiceImpl implements CustomService {

  @Autowired
  private CustomDao customDao;

  @Override
  public CustomNumberResponse getAgentCurrentConnectedPhoneNumber(Long agentInternalPhoneNumber)
      throws PersistException, CallNotFoundException {
    CustomNumberResponse customNumberResponse = customDao.getAgentCurrentConnectedPhoneNumber(
        agentInternalPhoneNumber);
    if (customNumberResponse == null) {
      throw new CallNotFoundException();
    }
    if (customNumberResponse.getPhoneNumber() == null) {
      throw new CallNotFoundException();
    }
    return customDao.getAgentCurrentConnectedPhoneNumber(agentInternalPhoneNumber);
  }
}
