package ua.iptel.java.osdialapi.service;

import ua.iptel.java.osdialapi.model.DialList;
import ua.iptel.java.osdialapi.model.dto.CallsCountResponse;

import java.util.List;

public interface DialListService {
    List<DialList> getAll();
    List<DialList> getAllByUserGroup(String userGroup);
    DialList getById(Long id);
    DialList create(DialList dialList);
    DialList update(DialList dialList);
    Long delete(Long id);
    List<CallsCountResponse> getCallsCount();
    List<CallsCountResponse> getCallsCountByListId(Long listId);
}
