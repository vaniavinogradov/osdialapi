package ua.iptel.java.osdialapi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.iptel.java.osdialapi.dao.osdial.UserGroupDao;
import ua.iptel.java.osdialapi.model.UserGroup;
import ua.iptel.java.osdialapi.service.UserGroupService;

import java.util.List;

@Service
public class UserGroupServiceImpl implements UserGroupService {
    @Autowired
    private UserGroupDao userGroupDao;


    @Override
    public List<UserGroup> getAll() {
        return userGroupDao.getAll();
    }

    @Override
    public UserGroup getById(Long id) {
        return userGroupDao.getByPK(id);
    }

    @Override
    public UserGroup getByName(String userGroupName) {
        return userGroupDao.getByName(userGroupName);
    }

    @Override
    public UserGroup create(UserGroup userGroup) {
        return userGroupDao.create(userGroup);
    }

    @Override
    public UserGroup update(UserGroup userGroup) {
        return userGroupDao.update(userGroup);
    }

    @Override
    public Long delete(Long id) {
        return userGroupDao.delete(id);
    }
}
