package ua.iptel.java.osdialapi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.iptel.java.osdialapi.dao.osdial.LeadDao;
import ua.iptel.java.osdialapi.model.Lead;
import ua.iptel.java.osdialapi.model.dto.LeadSimpleDto;
import ua.iptel.java.osdialapi.service.LeadService;

import java.util.List;


@Service
public class LeadServiceImpl implements LeadService {
    @Autowired
    private LeadDao leadDao;


    @Override
    public List<Lead> getAll() {
        return leadDao.getAll();
    }

    @Override
    public List<Lead> getByListId(Long id) {
        return leadDao.getByListId(id);
    }

    @Override
    public List<Lead> getByStatuses(List<String> statuses) {
        return leadDao.getByStatuses(statuses);
    }

    @Override
    public Lead getById(Long id) {
        return leadDao.getByPK(id);
    }

    @Override
    public Lead getByExternalKey(Long externalKey) {
        return leadDao.getByExternalKey(externalKey);
    }

    @Override
    public Lead create(Lead lead) {
        return leadDao.create(lead);
    }

    @Override
    public List<Lead> insertBatchLeads(List<Lead> leads) {
        return leadDao.insertBatchLeads(leads);
    }

    @Override
    public Lead update(Lead lead) {
        return leadDao.update(lead);
    }

    @Override
    public Long delete(Long id) {
        return leadDao.delete(id);
    }

    @Override
    public Integer addOnlyUniqueueLeads(List<LeadSimpleDto> leads) {
        return leadDao.addOnlyUniqueueLeads(leads);
    }

    @Override
    public void deleteLeadsFromPoolByListId(Long listId) {
        leadDao.deleteLeadsFromPoolByListId(listId);
    }

    @Override
    public void deleteLeadsFromPoolByCampaignId(Long campaignId) {
        leadDao.deleteLeadsFromPoolByCampaignId(campaignId);
    }
}
