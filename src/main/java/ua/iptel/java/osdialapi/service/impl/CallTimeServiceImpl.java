package ua.iptel.java.osdialapi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.iptel.java.osdialapi.dao.osdial.CallTimeDao;
import ua.iptel.java.osdialapi.model.CallTime;
import ua.iptel.java.osdialapi.service.CallTimeService;

import java.util.List;

@Service
public class CallTimeServiceImpl implements CallTimeService {
    @Autowired
    private CallTimeDao callTimeDao;

    @Override
    public List<CallTime> getAll() {
        return callTimeDao.getAll();
    }

    @Override
    public CallTime getById(Long id) {
        return callTimeDao.getByPK(id);
    }

    @Override
    public CallTime create(CallTime callTime) {
        return callTimeDao.create(callTime);
    }

    @Override
    public CallTime update(CallTime callTime) {
        return callTimeDao.update(callTime);
    }

    @Override
    public Long delete(Long id) {
        return callTimeDao.delete(id);
    }
}
