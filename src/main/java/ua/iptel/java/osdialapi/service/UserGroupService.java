package ua.iptel.java.osdialapi.service;

import ua.iptel.java.osdialapi.model.UserGroup;

import java.util.List;

public interface UserGroupService {
    List<UserGroup> getAll();
    UserGroup getById(Long id);
    UserGroup getByName(String userGroupName);
    UserGroup create(UserGroup userGroup);
    UserGroup update(UserGroup userGroup);
    Long delete(Long id);
}
