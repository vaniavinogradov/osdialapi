package ua.iptel.java.osdialapi.service;

import ua.iptel.java.osdialapi.model.MediaFile;

import java.util.List;

public interface MediaFileService {
    List<MediaFile> getAll();
    MediaFile getById(Long id);
    MediaFile create(MediaFile mediaFile);
    MediaFile update(MediaFile mediaFile);
    Long delete(Long id);
}
