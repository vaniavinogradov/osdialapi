package ua.iptel.java.osdialapi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.iptel.java.osdialapi.dao.osdial.CampaignDao;
import ua.iptel.java.osdialapi.model.Campaign;
import ua.iptel.java.osdialapi.service.CampaignService;

import java.util.List;

@Service
public class CampaignServiceImpl implements CampaignService {
    @Autowired
    private CampaignDao campaignDao;

    @Override
    public List<Campaign> getAll() {
        return campaignDao.getAll();
    }

    @Override
    public Campaign getById(Long id) {
        return campaignDao.getByPK(id);
    }

    @Override
    public Campaign create(Campaign campaign) {
        return campaignDao.create(campaign);
    }

    @Override
    public Campaign update(Campaign campaign) {
        return campaignDao.update(campaign);
    }

    @Override
    public Long delete(Long id) {
        return campaignDao.delete(id);
    }
}
