package ua.iptel.java.osdialapi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.iptel.java.osdialapi.dao.osdial.MediaFileDao;
import ua.iptel.java.osdialapi.model.MediaFile;
import ua.iptel.java.osdialapi.service.MediaFileService;

import java.util.List;

@Service
public class MediaFileServiceImpl implements MediaFileService {
    @Autowired
    private MediaFileDao mediaFileDao;


    @Override
    public List<MediaFile> getAll() {
        return mediaFileDao.getAll();
    }

    @Override
    public MediaFile getById(Long id) {
        return mediaFileDao.getByPK(id);
    }

    @Override
    public MediaFile create(MediaFile mediaFile) {
        return mediaFileDao.create(mediaFile);
    }

    @Override
    public MediaFile update(MediaFile mediaFile) {
        return mediaFileDao.update(mediaFile);
    }

    @Override
    public Long delete(Long id) {
        return mediaFileDao.delete(id);
    }
}
