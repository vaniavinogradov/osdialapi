package ua.iptel.java.osdialapi.service;

import ua.iptel.java.osdialapi.model.Lead;
import ua.iptel.java.osdialapi.model.dto.LeadSimpleDto;

import java.util.List;

public interface LeadService {
    List<Lead> getAll();
    List<Lead> getByListId(Long id);
    List<Lead> getByStatuses(List<String> statuses);
    Lead getById(Long id);
    Lead getByExternalKey(Long externalKey);
    Lead create(Lead lead);
    List<Lead> insertBatchLeads(List<Lead> leads);
    Lead update(Lead lead);
    Long delete(Long id);
    Integer addOnlyUniqueueLeads(List<LeadSimpleDto> leads);
    void deleteLeadsFromPoolByListId(Long listId);
    void deleteLeadsFromPoolByCampaignId(Long campaignId);
}
