package ua.iptel.java.osdialapi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.iptel.java.osdialapi.dao.osdial.ScriptDao;
import ua.iptel.java.osdialapi.model.Script;
import ua.iptel.java.osdialapi.service.ScriptService;

import java.util.List;

@Service
public class ScriptServiceImpl implements ScriptService {
    @Autowired
    private ScriptDao scriptDao;

    @Override
    public List<Script> getAll() {
        return scriptDao.getAll();
    }

    @Override
    public Script getById(Long id) {
        return scriptDao.getByPK(id);
    }

    @Override
    public Script create(Script script) {
        return scriptDao.create(script);
    }

    @Override
    public Script update(Script script) {
        return scriptDao.update(script);
    }

    @Override
    public Long delete(Long id) {
        return scriptDao.delete(id);
    }
}
