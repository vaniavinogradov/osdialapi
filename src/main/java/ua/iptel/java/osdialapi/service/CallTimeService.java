package ua.iptel.java.osdialapi.service;

import ua.iptel.java.osdialapi.model.CallTime;

import java.util.List;

public interface CallTimeService {
    List<CallTime> getAll();
    CallTime getById(Long id);
    CallTime create(CallTime callTime);
    CallTime update(CallTime callTime);
    Long delete(Long id);
}
