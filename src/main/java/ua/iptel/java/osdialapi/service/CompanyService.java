package ua.iptel.java.osdialapi.service;

import ua.iptel.java.osdialapi.model.Company;

public interface CompanyService {
    Company get();
}
