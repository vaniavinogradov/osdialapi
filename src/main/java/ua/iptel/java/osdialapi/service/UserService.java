package ua.iptel.java.osdialapi.service;

import ua.iptel.java.osdialapi.model.User;

import java.util.List;

/**
 * Created by ipvinner on 12.10.2016.
 */

public interface UserService {
    List<User> getAll();
    User getById(Long id);
    User getByUserName(String username);
    User create(User user);
    User update(User user);
    Long delete(Long id);
}
