package ua.iptel.java.osdialapi.service;

import ua.iptel.java.osdialapi.model.Script;

import java.util.List;

public interface ScriptService {
    List<Script> getAll();
    Script getById(Long id);
    Script create(Script script);
    Script update(Script script);
    Long delete(Long id);
}
