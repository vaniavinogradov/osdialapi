package ua.iptel.java.osdialapi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.iptel.java.osdialapi.dao.osdial.IvrDao;
import ua.iptel.java.osdialapi.model.Ivr;
import ua.iptel.java.osdialapi.model.IvrOption;
import ua.iptel.java.osdialapi.service.IvrService;

import java.util.List;
@Service
public class IvrServiceImpl implements IvrService {
    @Autowired
    private IvrDao ivrDao;

    @Override
    public List<Ivr> getAll() {
        return ivrDao.getAll();
    }

    @Override
    public Ivr getById(Long id) {
        return ivrDao.getByPK(id);
    }

    @Override
    public Ivr create(Ivr ivr) {
        return ivrDao.create(ivr);
    }

    @Override
    public Ivr update(Ivr ivr) {
        return ivrDao.update(ivr);
    }

    @Override
    public Long delete(Long id) {
        return ivrDao.delete(id);
    }

    @Override
    public List<IvrOption> getAllIvrOptions() {
        return ivrDao.getAllIvrOptions();
    }

    @Override
    public IvrOption getIvrOptionByPk(Long id) {
        return ivrDao.getIvrOptionByPk(id);
    }

    @Override
    public IvrOption createIvrOption(IvrOption ivrOption) {
        return ivrDao.createIvrOption(ivrOption);
    }

    @Override
    public IvrOption updateIvrOption(IvrOption ivrOption) {
        return ivrDao.updateIvrOption(ivrOption);
    }

    @Override
    public Long deleteIvrOption(Long id) {
        return ivrDao.deleteIvrOption(id);
    }
}
