package ua.iptel.java.osdialapi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ua.iptel.java.osdialapi.dao.osdial.AccessTokenDao;
import ua.iptel.java.osdialapi.dao.osdial.UserDao;
import ua.iptel.java.osdialapi.model.AccessToken;
import ua.iptel.java.osdialapi.model.User;
import ua.iptel.java.osdialapi.service.UserService;
import ua.iptel.java.osdialapi.util.JwtUtil;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class UserServiceImpl implements UserService, UserDetailsService {
    @Autowired
    private UserDao userDao;

    @Autowired
    private AccessTokenDao accessTokenDao;

    @Override
    public List<User> getAll() {
        return userDao.getAll();
    }

    @Override
    public User getById(Long id) {
        return userDao.getByPK(id);
    }

    @Override
    public User getByUserName(String username) {
        return userDao.getByUserName(username);
    }

    @Override
    public User create(User user) {
        return userDao.create(user);
    }

    @Override
    public User update(User user) {
        return userDao.update(user);
    }

    @Override
    public Long delete(Long id) {
        return userDao.delete(id);
    }

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        User user = userDao.getByUserName(name);
        if (user == null) {
            return null;
        } else {
            return user;
        }
    }

    public AccessToken getToken(User user) {
        AccessToken accessToken = accessTokenDao.findByUserId(user.getId());
        if(accessToken == null) {
            accessToken = new AccessToken(user, generateToken(user));
            return accessTokenDao.create(accessToken);
        }else {
            if(accessToken.getExpire().isBefore(LocalDateTime.now())) {
                return accessTokenDao.update(accessToken);
            }else {
                return accessToken;
            }
        }
    }

    private String generateToken(User user){
        return JwtUtil.generateToken(user);
    }
}
