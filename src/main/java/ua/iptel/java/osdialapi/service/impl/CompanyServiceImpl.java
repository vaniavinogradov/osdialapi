package ua.iptel.java.osdialapi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.iptel.java.osdialapi.dao.osdial.CompanyDao;
import ua.iptel.java.osdialapi.model.Company;
import ua.iptel.java.osdialapi.service.CompanyService;

@Service
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyDao companyDao;
    @Override
    public Company get() {
        return companyDao.get();
    }
}
