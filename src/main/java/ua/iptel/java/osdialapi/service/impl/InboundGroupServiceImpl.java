package ua.iptel.java.osdialapi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.iptel.java.osdialapi.dao.osdial.InboundGroupDao;
import ua.iptel.java.osdialapi.model.InboundGroup;
import ua.iptel.java.osdialapi.service.InboundGroupService;

import java.util.List;

@Service
public class InboundGroupServiceImpl implements InboundGroupService {
    @Autowired
    private InboundGroupDao inboundGroupDao;

    @Override
    public List<InboundGroup> getAll() {
        return inboundGroupDao.getAll();
    }

    @Override
    public InboundGroup getById(Long id) {
        return inboundGroupDao.getByPK(id);
    }

    @Override
    public InboundGroup create(InboundGroup inboundGroup) {
        return inboundGroupDao.create(inboundGroup);
    }

    @Override
    public InboundGroup update(InboundGroup inboundGroup) {
        return inboundGroupDao.update(inboundGroup);
    }

    @Override
    public Long delete(Long id) {
        return inboundGroupDao.delete(id);
    }
}
