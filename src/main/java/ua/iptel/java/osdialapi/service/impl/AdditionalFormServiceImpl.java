package ua.iptel.java.osdialapi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.iptel.java.osdialapi.dao.osdial.AdditionalFormDao;
import ua.iptel.java.osdialapi.model.AdditionalForm;
import ua.iptel.java.osdialapi.service.AdditionalFormService;

import java.util.List;

@Service
public class AdditionalFormServiceImpl implements AdditionalFormService {
    @Autowired
    private AdditionalFormDao additionalFormDao;

    @Override
    public List<AdditionalForm> getAll() {
        return additionalFormDao.getAll();
    }

    @Override
    public AdditionalForm getById(Long id) {
        return additionalFormDao.getByPK(id);
    }

    @Override
    public AdditionalForm create(AdditionalForm additionalForm) {
        return additionalFormDao.create(additionalForm);
    }

    @Override
    public AdditionalForm update(AdditionalForm additionalForm) {
        return additionalFormDao.update(additionalForm);
    }

    @Override
    public Long delete(Long id) {
        return additionalFormDao.delete(id);
    }
}
