package ua.iptel.java.osdialapi.service;

import ua.iptel.java.osdialapi.model.Campaign;

import java.util.List;


public interface CampaignService {
    List<Campaign> getAll();
    Campaign getById(Long id);
    Campaign create(Campaign campaign);
    Campaign update(Campaign campaign);
    Long delete(Long id);
}
