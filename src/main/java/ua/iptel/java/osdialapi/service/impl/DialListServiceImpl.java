package ua.iptel.java.osdialapi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.iptel.java.osdialapi.dao.osdial.ListDao;
import ua.iptel.java.osdialapi.model.DialList;
import ua.iptel.java.osdialapi.model.dto.CallsCountResponse;
import ua.iptel.java.osdialapi.service.DialListService;

import java.util.List;

@Service
public class DialListServiceImpl implements DialListService {
    @Autowired
    private ListDao listDao;

    @Override
    public List<DialList> getAll() {
        return listDao.getAll();
    }

    @Override
    public List<DialList> getAllByUserGroup(String userGroup) {
        return listDao.getAllByUserGroup(userGroup);
    }

    @Override
    public DialList getById(Long id) {
        return listDao.getByPK(id);
    }

    @Override
    public DialList create(DialList dialList) {
        return listDao.create(dialList);
    }

    @Override
    public DialList update(DialList dialList) {
        return listDao.update(dialList);
    }

    @Override
    public Long delete(Long id) {
        return listDao.delete(id);
    }

    @Override
    public List<CallsCountResponse> getCallsCount() {
        return listDao.getCallsCount();
    }

    @Override
    public List<CallsCountResponse> getCallsCountByListId(Long listId) {
        return listDao.getCallsCountByListId(listId);
    }
}
