package ua.iptel.java.osdialapi.service;

import ua.iptel.java.osdialapi.model.InboundGroup;

import java.util.List;

public interface InboundGroupService {
    List<InboundGroup> getAll();
    InboundGroup getById(Long id);
    InboundGroup create(InboundGroup inboundGroup);
    InboundGroup update(InboundGroup inboundGroup);
    Long delete(Long id);
}
