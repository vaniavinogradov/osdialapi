package ua.iptel.java.osdialapi.service;

import ua.iptel.java.osdialapi.dao.exception.PersistException;
import ua.iptel.java.osdialapi.model.CallNotFoundException;
import ua.iptel.java.osdialapi.model.dto.CustomNumberResponse;

public interface CustomService {
  CustomNumberResponse getAgentCurrentConnectedPhoneNumber(Long agentInternalPhoneNumber) throws PersistException, CallNotFoundException;
}
