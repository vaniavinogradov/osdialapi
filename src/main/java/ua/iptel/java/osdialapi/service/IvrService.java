package ua.iptel.java.osdialapi.service;

import ua.iptel.java.osdialapi.model.Ivr;
import ua.iptel.java.osdialapi.model.IvrOption;

import java.util.List;

public interface IvrService {
    List<Ivr> getAll();
    Ivr getById(Long id);
    Ivr create(Ivr ivr);
    Ivr update(Ivr ivr);
    Long delete(Long id);

    List<IvrOption> getAllIvrOptions();
    IvrOption getIvrOptionByPk(Long id);
    IvrOption createIvrOption(IvrOption ivrOption);
    IvrOption updateIvrOption(IvrOption ivrOption);
    Long deleteIvrOption(Long id);
}
