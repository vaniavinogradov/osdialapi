package ua.iptel.java.osdialapi.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.iptel.java.osdialapi.model.Campaign;
import ua.iptel.java.osdialapi.service.CampaignService;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("rest/admin/campaigns")
public class CampaignController {
    @Autowired
    private CampaignService service;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Campaign> getAll() {
        List<Campaign> result = service.getAll();
        return result;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Campaign> get(@PathVariable("id") Long id){
        Campaign campaign = service.getById(id);
        if(campaign == null){
            return new ResponseEntity("No Campaign found for ID " + id, HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity<Campaign>(campaign, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable("id") Long id) {
        if (0 == service.delete(id)) {
            return new ResponseEntity("No Campaign found for ID " + id, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(id, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Campaign> create(@RequestBody Campaign campaign){
        Campaign newCampaign = service.create(campaign);
        return new ResponseEntity<Campaign>(newCampaign, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.PUT)
    public ResponseEntity<Campaign> update(@PathVariable("id") int id, @RequestBody Campaign campaign){
        Campaign updatedCampaign = service.update(campaign);
        if (null == updatedCampaign) {
            return new ResponseEntity("No Campaign found for ID " + id, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Campaign>(updatedCampaign, HttpStatus.OK);
    }

}
