package ua.iptel.java.osdialapi.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.iptel.java.osdialapi.model.Ivr;
import ua.iptel.java.osdialapi.model.IvrOption;
import ua.iptel.java.osdialapi.service.IvrService;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("rest/admin/ivrs")
public class IvrController {
    @Autowired
    private IvrService service;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Ivr> getAll() {
        List<Ivr> result = service.getAll();
        return result;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Ivr> get(@PathVariable("id") Long id){
        Ivr ivr = service.getById(id);
        if(ivr == null){
            return new ResponseEntity("No Ivr found for ID " + id, HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity<Ivr>(ivr, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/options", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<IvrOption> getAllIvrOptions() {
        List<IvrOption> result = service.getAllIvrOptions();
        return result;
    }

    @RequestMapping(value = "/options/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<IvrOption> getIvrOption(@PathVariable("id") Long id){
        IvrOption ivrOption = service.getIvrOptionByPk(id);
        if(ivrOption == null){
            return new ResponseEntity("No IvrOption found for ID " + id, HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity<IvrOption>(ivrOption, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/options", method = RequestMethod.POST)
    public ResponseEntity<IvrOption> createOption(@RequestBody IvrOption ivrOption){
        IvrOption newIvrOption = service.createIvrOption(ivrOption);
        return new ResponseEntity<IvrOption>(ivrOption, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/options/{id}",method = RequestMethod.PUT)
    public ResponseEntity<IvrOption> updateOption(@PathVariable("id") int id, @RequestBody IvrOption ivrOption){
        IvrOption updatedIvrOption = service.updateIvrOption(ivrOption);
        if (null == updatedIvrOption) {
            return new ResponseEntity("No IvrOption found for ID " + id, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<IvrOption>(updatedIvrOption, HttpStatus.OK);
    }

//    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
//    public ResponseEntity delete(@PathVariable("id") Long id) {
//        if (0 == service.delete(id)) {
//            return new ResponseEntity("No Ivr found for ID " + id, HttpStatus.NOT_FOUND);
//        }
//        return new ResponseEntity(id, HttpStatus.OK);
//    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Ivr> create(@RequestBody Ivr ivr){
        Ivr newIvr = service.create(ivr);
        return new ResponseEntity<Ivr>(newIvr, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.PUT)
    public ResponseEntity<Ivr> update(@PathVariable("id") int id, @RequestBody Ivr ivr){
        Ivr updatedIvr = service.update(ivr);
        if (null == updatedIvr) {
            return new ResponseEntity("No Ivr found for ID " + id, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Ivr>(updatedIvr, HttpStatus.OK);
    }

}
