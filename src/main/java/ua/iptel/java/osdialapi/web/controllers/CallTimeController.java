package ua.iptel.java.osdialapi.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.iptel.java.osdialapi.model.CallTime;
import ua.iptel.java.osdialapi.service.CallTimeService;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("rest/admin/callTimes")
public class CallTimeController {
    @Autowired
    private CallTimeService service;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<CallTime> getAll() {
        List<CallTime> result = service.getAll();
        return result;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CallTime> get(@PathVariable("id") Long id){
        CallTime callTime = service.getById(id);
        if(callTime == null){
            return new ResponseEntity("No CallTime found for ID " + id, HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity<CallTime>(callTime, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable("id") Long id) {
        if (0 == service.delete(id)) {
            return new ResponseEntity("No CallTime found for ID " + id, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(id, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<CallTime> create(@RequestBody CallTime callTime){
        CallTime newCallTime = service.create(callTime);
        return new ResponseEntity<CallTime>(newCallTime, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.PUT)
    public ResponseEntity<CallTime> update(@PathVariable("id") int id, @RequestBody CallTime callTime){
        CallTime updatedCallTime = service.update(callTime);
        if (null == updatedCallTime) {
            return new ResponseEntity("No CallTime found for ID " + id, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<CallTime>(updatedCallTime, HttpStatus.OK);
    }

}
