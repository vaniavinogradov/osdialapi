package ua.iptel.java.osdialapi.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ua.iptel.java.osdialapi.model.DialList;
import ua.iptel.java.osdialapi.model.dto.CallsCountResponse;
import ua.iptel.java.osdialapi.service.DialListService;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("rest/admin/lists")
public class ListController {
    @Autowired
    private DialListService service;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<DialList> getAll() {
        List<DialList> result = service.getAll();
        return result;
    }

    @RequestMapping(value = "/getCallsCount", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<CallsCountResponse> getCallsCount() {
        List<CallsCountResponse> callsCountResponse = service.getCallsCount();
        return callsCountResponse;
    }

    @RequestMapping(value = "/getCallsCountByListId/{listId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<CallsCountResponse> getCallsCountByListId(@PathVariable("listId") Long listId) {
        List<CallsCountResponse> callsCountResponse = service.getCallsCountByListId(listId);
        return callsCountResponse;
    }



    @RequestMapping(value = "/getByUserGroup/{userGroup}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<DialList> getAllByUserGroup(@PathVariable("userGroup") String userGroup) {
        List<DialList> result = service.getAllByUserGroup(userGroup);
        return result;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DialList> get(@PathVariable("id") Long id){
        DialList dialList = service.getById(id);
        if(dialList == null){
            return new ResponseEntity("No List found for ID " + id, HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity<DialList>(dialList, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable("id") Long id) {
        if (0 == service.delete(id)) {
            return new ResponseEntity("No List found for ID " + id, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(id, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<DialList> create(@Valid @RequestBody DialList dialList){
        DialList dialListNew = service.create(dialList);
        return new ResponseEntity<DialList>(dialListNew, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.PUT)
    public ResponseEntity<DialList> update(@PathVariable("id") int id, @RequestBody DialList dialList){
        DialList dialListUpdated = service.update(dialList);
        if (null == dialListUpdated) {
            return new ResponseEntity("No List found for ID " + id, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<DialList>(dialListUpdated, HttpStatus.OK);
    }
}
