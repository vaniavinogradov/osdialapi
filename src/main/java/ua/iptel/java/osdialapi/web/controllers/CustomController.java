package ua.iptel.java.osdialapi.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ua.iptel.java.osdialapi.model.CallNotFoundException;
import ua.iptel.java.osdialapi.model.dto.CustomNumberResponse;
import ua.iptel.java.osdialapi.service.CustomService;

@RestController
@RequestMapping("rest/admin/custom")
public class CustomController {
  @Autowired
  private CustomService customService;


  @RequestMapping(value = "/get-agent-current-connecte-phone/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity getByListId(@PathVariable("id") Long id){
    try {
      CustomNumberResponse customNumberResponse = customService.getAgentCurrentConnectedPhoneNumber(id);
      return new ResponseEntity(customNumberResponse, HttpStatus.OK);
    }catch (CallNotFoundException callNotFoundException){
      return new ResponseEntity("Call not found.",
          HttpStatus.NOT_FOUND);
    }catch (Exception exception){
      return new ResponseEntity("Server error. Please contact with administrator.",
          HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
