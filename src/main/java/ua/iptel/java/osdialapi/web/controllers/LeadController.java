package ua.iptel.java.osdialapi.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ua.iptel.java.osdialapi.model.Lead;
import ua.iptel.java.osdialapi.model.dto.LeadSimpleDto;
import ua.iptel.java.osdialapi.service.LeadService;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

@Validated
@RestController
@RequestMapping("rest/admin/leads")
public class LeadController {

    @Autowired
    private LeadService service;

//    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//    public Collection<Lead> getAll() {
//        List<Lead> result = service.getAll();
//        return result;
//    }

    @RequestMapping(value = "/getByListId/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Lead> getByListId(@PathVariable("id") Long id){
        List<Lead> result = service.getByListId(id);
        return result;
    }

    @RequestMapping(value = "/getByExternalKey/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Lead getByExternalKey(@PathVariable("id") Long externalKey){
        Lead result = service.getByExternalKey(externalKey);
        return result;
    }

    @RequestMapping(value = "/getByStatuses", method = RequestMethod.POST)
    public Collection<Lead> getByStatuses(@RequestBody List<String> statuses){
        List<Lead> result = service.getByStatuses(statuses);
        return result;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Lead> get(@PathVariable("id") Long id){
        Lead lead = service.getById(id);
        if(lead == null){
            return new ResponseEntity("No Lead found for ID " + id, HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity<Lead>(lead, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable("id") Long id) {
        if (0 == service.delete(id)) {
            return new ResponseEntity("No Lead found for ID " + id, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(id, HttpStatus.OK);
    }

    @RequestMapping(value = "/pool/campaign/{campaignId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteLeadsFromPoolByCampaignId(@PathVariable("campaignId") Long campaignId) {
        service.deleteLeadsFromPoolByCampaignId(campaignId);
        return new ResponseEntity(campaignId, HttpStatus.OK);
    }

    @RequestMapping(value = "/pool/list/{listId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteLeadsFromPoolByListId(@PathVariable("listId") Long listId) {
        service.deleteLeadsFromPoolByListId(listId);
        return new ResponseEntity(listId, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Lead> create(@Valid @RequestBody Lead Lead){
        Lead newLead = service.create(Lead);
        return new ResponseEntity<Lead>(newLead, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/fastcreate", method = RequestMethod.POST)
    public ResponseEntity<Lead> fastCreate(@Valid @RequestBody LeadSimpleDto newLead){
        Lead createdLead = service.create(convertDtoToLead(newLead));
        return new ResponseEntity<Lead>(createdLead, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/addOnlyUniqueLeads", method = RequestMethod.POST)
    public ResponseEntity<Integer> addOnlyUniqueueLeads(@RequestBody List<@Valid LeadSimpleDto> leads){
        //Lead createdLead = service.create(convertDtoToLead(newLead));
        Integer countOfAddedLeads = service.addOnlyUniqueueLeads(leads);
        return new ResponseEntity<Integer>(countOfAddedLeads, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/batchInsert", method = RequestMethod.POST)
    public ResponseEntity<List<Lead>> batchInsert(@RequestBody List<Lead> leads){
        List<Lead> insertedLeads = service.insertBatchLeads(leads);
        return new ResponseEntity<List<Lead>>(leads, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/fastBatchInsert", method = RequestMethod.POST)
    public ResponseEntity<List<Lead>> fastBatchInsert(@RequestBody List<LeadSimpleDto> leads){
        List<Lead> convertedList = new LinkedList<>();
        for (int i = 0; i < leads.size(); i++) {
            convertedList.add(convertDtoToLead(leads.get(i)));
        }
        List<Lead> insertedLeads = service.insertBatchLeads(convertedList);
        return new ResponseEntity<List<Lead>>(insertedLeads, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.PUT)
    public ResponseEntity<Lead> update(@PathVariable("id") int id, @RequestBody Lead lead){
        Lead leadUpdated = service.update(lead);
        if (null == leadUpdated) {
            return new ResponseEntity("No Lead found for ID " + id, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Lead>(leadUpdated, HttpStatus.OK);
    }

    private Lead convertDtoToLead(LeadSimpleDto leadSimpleDto){
        Lead lead = new Lead();
        lead.setEntryDate(LocalDateTime.now());
        lead.setModifyDateTime(LocalDateTime.now());
        lead.setStatus("NEW");
        lead.setUser("");
        lead.setVendorLeadCode("");
        lead.setSourceId("");
        lead.setListId(leadSimpleDto.getListId());
        lead.setGmtOffsetNow(0.0f);
        lead.setCalledSinceLastReset("N");
        lead.setPhoneCode(leadSimpleDto.getPhoneCode());
        lead.setPhoneNumber(leadSimpleDto.getPhoneNumber());
        lead.setTitle("");
        lead.setFirstName(leadSimpleDto.getFirstName());
        lead.setMiddleInitial("");
        lead.setLastName(leadSimpleDto.getLastName());
        lead.setAddress1(leadSimpleDto.getAddress1());
        lead.setAddress2("");
        lead.setAddress3("");
        lead.setCity(leadSimpleDto.getCity());
        lead.setState("");
        lead.setProvince("");
        lead.setCountryCode(leadSimpleDto.getCountryCode());
        lead.setGender("");
        lead.setAltPhone(leadSimpleDto.getAltPhone());
        lead.setEmail(leadSimpleDto.getEmail());
        lead.setCustom1(leadSimpleDto.getCustom1());
        lead.setCustom2(leadSimpleDto.getCustom2());
        lead.setComments(leadSimpleDto.getComments());
        lead.setExternalKey(leadSimpleDto.getExternalKey());
        lead.setOrganization(leadSimpleDto.getOrganization());
        lead.setCost(0.0f);
        lead.setPostalCode("");
        lead.setOrganizationTitle("");
        lead.setStatusExtended("");
        return lead;
    }
}
