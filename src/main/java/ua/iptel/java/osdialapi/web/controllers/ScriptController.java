package ua.iptel.java.osdialapi.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.iptel.java.osdialapi.model.Script;
import ua.iptel.java.osdialapi.service.ScriptService;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("rest/admin/scripts")
public class ScriptController {
    @Autowired
    private ScriptService service;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Script> getAll() {
        List<Script> result = service.getAll();
        return result;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Script> get(@PathVariable("id") Long id){
        Script script = service.getById(id);
        if(script == null){
            return new ResponseEntity("No Script found for ID " + id, HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity<Script>(script, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable("id") Long id) {
        if (0 == service.delete(id)) {
            return new ResponseEntity("No Script found for ID " + id, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(id, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Script> create(@Valid @RequestBody Script script){
        Script scriptNew = service.create(script);
        return new ResponseEntity<Script>(scriptNew, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.PUT)
    public ResponseEntity<Script> update(@PathVariable("id") int id, @RequestBody Script script){
        Script scriptUpdated = service.update(script);
        if (null == scriptUpdated) {
            return new ResponseEntity("No Script found for ID " + id, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Script>(scriptUpdated, HttpStatus.OK);
    }
    
}
