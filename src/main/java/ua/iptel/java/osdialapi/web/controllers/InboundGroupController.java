package ua.iptel.java.osdialapi.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.iptel.java.osdialapi.model.InboundGroup;
import ua.iptel.java.osdialapi.service.InboundGroupService;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("rest/admin/inboundGroups")
public class InboundGroupController {
    @Autowired
    private InboundGroupService service;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<InboundGroup> getAll() {
        List<InboundGroup> result = service.getAll();
        return result;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<InboundGroup> get(@PathVariable("id") Long id){
        InboundGroup inboundGroup = service.getById(id);
        if(inboundGroup == null){
            return new ResponseEntity("No InboundGroup found for ID " + id, HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity<InboundGroup>(inboundGroup, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable("id") Long id) {
        if (0 == service.delete(id)) {
            return new ResponseEntity("No InboundGroup found for ID " + id, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(id, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<InboundGroup> create(@Valid @RequestBody InboundGroup inboundGroup){
        InboundGroup inboundGroupNew = service.create(inboundGroup);
        return new ResponseEntity<InboundGroup>(inboundGroupNew, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.PUT)
    public ResponseEntity<InboundGroup> update(@PathVariable("id") int id, @RequestBody InboundGroup inboundGroup){
        InboundGroup inboundGroupUpdated = service.update(inboundGroup);
        if (null == inboundGroupUpdated) {
            return new ResponseEntity("No InboundGroup found for ID " + id, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<InboundGroup>(inboundGroupUpdated, HttpStatus.OK);
    }
}
