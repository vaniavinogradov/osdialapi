package ua.iptel.java.osdialapi.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.iptel.java.osdialapi.model.User;
import ua.iptel.java.osdialapi.service.UserService;
import ua.iptel.java.osdialapi.util.PasswordUtil;


import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

/**
 * Created by ipvinner on 12.10.2016.
 */
@RestController
@RequestMapping("rest/admin/users")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<User> getAll() {
        List<User> result = userService.getAll();
        return result;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> get(@PathVariable("id") Long id){
        User user = userService.getById(id);
        if(user == null){
            return new ResponseEntity("No User found for ID " + id, HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity<User>(user, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable("id") Long id) {
        if (0 == userService.delete(id)) {
            return new ResponseEntity("No User found for ID " + id, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(id, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<User> create(@RequestBody User user){
        // check if user with that username exists
        if(userService.getByUserName(user.getUsername()) != null){
            return new ResponseEntity("User with this username: " + user.getUsername() + "already exists", HttpStatus.CONFLICT);
        }else {
            user.setPasswordMd5(PasswordUtil.encode(user.getPassword()));
            User newUser = userService.create(user);
            return new ResponseEntity<User>(newUser, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.PUT)
    public ResponseEntity<User> update(@PathVariable("id") int id, @RequestBody User user){
        User updatedUser = userService.update(user);
        if (null == updatedUser) {
            return new ResponseEntity("No User found for ID " + id, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<User>(updatedUser, HttpStatus.OK);
    }
}
