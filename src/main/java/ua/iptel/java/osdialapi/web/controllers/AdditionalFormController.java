package ua.iptel.java.osdialapi.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.iptel.java.osdialapi.model.AdditionalForm;
import ua.iptel.java.osdialapi.service.AdditionalFormService;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("rest/admin/additionalForms")
public class AdditionalFormController {
    @Autowired
    private AdditionalFormService service;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<AdditionalForm> getAll() {
        List<AdditionalForm> result = service.getAll();
        return result;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AdditionalForm> get(@PathVariable("id") Long id){
        AdditionalForm additionalForm = service.getById(id);
        if(additionalForm == null){
            return new ResponseEntity("No AdditionalForm found for ID " + id, HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity<AdditionalForm>(additionalForm, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable("id") Long id) {
        if (0 == service.delete(id)) {
            return new ResponseEntity("No AdditionalForm found for ID " + id, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(id, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<AdditionalForm> create(@RequestBody AdditionalForm additionalForm){
        AdditionalForm newAdditionalForm = service.create(additionalForm);
        return new ResponseEntity<AdditionalForm>(newAdditionalForm, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.PUT)
    public ResponseEntity<AdditionalForm> update(@PathVariable("id") int id, @RequestBody AdditionalForm additionalForm){
        AdditionalForm updatedAdditionalForm = service.update(additionalForm);
        if (null == updatedAdditionalForm) {
            return new ResponseEntity("No AdditionalForm found for ID " + id, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<AdditionalForm>(updatedAdditionalForm, HttpStatus.OK);
    }

}
