package ua.iptel.java.osdialapi.web.controllers.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ua.iptel.java.osdialapi.model.AccessToken;
import ua.iptel.java.osdialapi.model.User;
import ua.iptel.java.osdialapi.model.dto.UserDtoWithPassword;
import ua.iptel.java.osdialapi.model.dto.UserSimpleDto;
import ua.iptel.java.osdialapi.service.impl.UserServiceImpl;
import ua.iptel.java.osdialapi.util.PasswordUtil;


@RestController
@RequestMapping("rest/login")
public class LoginController {
    @Autowired
    UserServiceImpl service;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity create(@RequestBody UserSimpleDto user){
        User loggedInUser = (User) service.loadUserByUsername(user.getUsername());
        if(loggedInUser == null) {
            return new ResponseEntity("User with name: " + user.getUsername() + " NOT found.", HttpStatus.NOT_FOUND);
        }else {
            if(PasswordUtil.isMatch(user.getPassword(), loggedInUser.getPasswordMd5())){
                return new ResponseEntity<AccessToken>(service.getToken(loggedInUser), HttpStatus.OK);
            }else {
                return new ResponseEntity("Wrong password." , HttpStatus.UNAUTHORIZED);
            }
        }
    }
}
