package ua.iptel.java.osdialapi.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ua.iptel.java.osdialapi.model.Company;
import ua.iptel.java.osdialapi.service.CompanyService;

@RestController
@RequestMapping("rest/admin/settings")
public class SettingsController {
    @Autowired
    CompanyService companyService;

    @RequestMapping(value = "/getCompanyInfo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Company getCompanyInfo() {
        return companyService.get();
    }
}
