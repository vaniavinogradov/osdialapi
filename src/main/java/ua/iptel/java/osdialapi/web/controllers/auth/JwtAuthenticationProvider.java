package ua.iptel.java.osdialapi.web.controllers.auth;


import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import ua.iptel.java.osdialapi.model.User;
import ua.iptel.java.osdialapi.util.JwtUtil;


import java.util.LinkedList;
import java.util.List;

public class JwtAuthenticationProvider implements AuthenticationProvider {
    @Override
    public boolean supports(Class<?> authentication) {
        return JwtAuthentication.class.equals(authentication);
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        JwtAuthentication jwtAuthenticationToken = (JwtAuthentication) authentication;
        String token = jwtAuthenticationToken.getToken();

        User parsedUser = JwtUtil.parseToken(token);


        if (parsedUser == null) {
            throw new AuthenticationServiceException("Token corrupted");
        }

        List<GrantedAuthority> authorityList = AuthorityUtils.commaSeparatedStringToAuthorityList(generateAuthorityListBasedOnUserPermissions(parsedUser));

        return new JwtAuthentication(token, authorityList, true, parsedUser);
    }

    private String generateAuthorityListBasedOnUserPermissions(User user) {
        List<String> roles = new LinkedList<>();
        if(user.isAllowModifyUsers()) {
            roles.add("ROLE_MODIFY_USERS");
        }
        if(user.isAllowDeleteUsers()){
            roles.add("ROLE_DELETE_USERS");
        }
        if(user.isAllowModifyCampaigns()){
            roles.add("ROLE_MODIFY_CAMPAIGNS");
        }
        if (user.isAllowDeleteCampaigns()) {
            roles.add("ROLE_DELETE_CAMPAIGNS");
        }
        if (user.isAllowModifyInGroups()){
            roles.add("ROLE_MODIFY_INGROUPS");
        }
        if (user.isAllowDeleteIngroups()) {
            roles.add("ROLE_DELETE_INGROUPS");
        }
        if(user.isAllowModifyLists()) {
            roles.add("ROLE_MODIFY_LISTS");
        }
        if (user.isAllowDeleteLists()) {
            roles.add("ROLE_DELETE_LISTS");
        }
        if (user.isAllowModifyLeads()) {
            roles.add("ROLE_MODIFY_LEADS");
        }
        if (user.isAllowViewReports()) {
            roles.add("ROLE_VIEW_REPORTS");
        }
        if (user.isAllowModifyScripts()) {
            roles.add("ROLE_MODIFY_SCRIPTS");
        }
        if (user.isAllowDeleteScripts()) {
            roles.add("ROLE_DELETE_SCRIPTS");
        }

        return StringUtils.join(roles, ",");
    }

}
