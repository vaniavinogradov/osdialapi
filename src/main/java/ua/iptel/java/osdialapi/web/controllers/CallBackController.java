package ua.iptel.java.osdialapi.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.iptel.java.osdialapi.dao.osdial.CallBackDao;
import ua.iptel.java.osdialapi.model.CallBack;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("rest/admin/callbacks")
public class CallBackController {
    @Autowired
    private CallBackDao callBackDao;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<CallBack>> getAll() {
        List<CallBack> callBacks = this.callBackDao.getAll();
        if(callBacks == null){
            return new ResponseEntity("Server error. Please contact with administrator.",
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }else {
            return new ResponseEntity(callBacks, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CallBack> getByPk(@PathVariable("id") Long id){
        CallBack CallBack = this.callBackDao.getByPK(id);
        if(CallBack == null){
            return new ResponseEntity("Server error. Please contact with administrator.",
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }else {
            return new ResponseEntity(CallBack, HttpStatus.OK);
        }
    }

    @RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public ResponseEntity<CallBack> create(@RequestBody CallBack callBack){
        callBack.setModifyDate(LocalDate.now());
        CallBack createdCallBack = this.callBackDao.create(callBack);
        if(createdCallBack == null){
            return new ResponseEntity("Server error. Please contact with administrator.",
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }else{
            return new ResponseEntity<CallBack>(createdCallBack, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<CallBack> update(@PathVariable("id") int id, @RequestBody CallBack callBack){
        callBack.setModifyDate(LocalDate.now());
        CallBack updatedCallBack = this.callBackDao.update(callBack);
        if (null == updatedCallBack) {
            return new ResponseEntity("Server error. Please contact with administrator. " + id,
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }else {
            return new ResponseEntity(updatedCallBack, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable("id") Long id) {
        Long result = this.callBackDao.delete(id);
        if(result == null){
            return new ResponseEntity("Please contact with administrator", HttpStatus.INTERNAL_SERVER_ERROR);
        }else {
            return new ResponseEntity(id, HttpStatus.OK);
        }
    }
}
