package ua.iptel.java.osdialapi.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.iptel.java.osdialapi.model.MediaFile;
import ua.iptel.java.osdialapi.service.MediaFileService;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("rest/admin/mediaFiles")
public class MediaFileController {
    @Autowired
    private MediaFileService service;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<MediaFile> getAll() {
        List<MediaFile> result = service.getAll();
        return result;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MediaFile> get(@PathVariable("id") Long id){
        MediaFile mediaFile = service.getById(id);
        if(mediaFile == null){
            return new ResponseEntity("No MediaFile found for ID " + id, HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity<MediaFile>(mediaFile, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable("id") Long id) {
        if (0 == service.delete(id)) {
            return new ResponseEntity("No MediaFile found for ID " + id, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(id, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<MediaFile> create(@Valid @RequestBody MediaFile mediaFile){
        MediaFile mediaFileNew = service.create(mediaFile);
        return new ResponseEntity<MediaFile>(mediaFileNew, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.PUT)
    public ResponseEntity<MediaFile> update(@PathVariable("id") int id, @RequestBody MediaFile mediaFile){
        MediaFile mediaFileUpdated = service.update(mediaFile);
        if (null == mediaFileUpdated) {
            return new ResponseEntity("No MediaFile found for ID " + id, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<MediaFile>(mediaFileUpdated, HttpStatus.OK);
    }
}
