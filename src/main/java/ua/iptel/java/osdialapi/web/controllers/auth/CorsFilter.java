package ua.iptel.java.osdialapi.web.controllers.auth;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

//public class CorsFilter implements Filter {
//    @Override
//    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
//            throws IOException, ServletException {
//        HttpServletResponse httpResponse = (HttpServletResponse) response;
//        httpResponse.setHeader("Access-Control-Allow-Origin", "*");
//        httpResponse.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
//        httpResponse.setHeader("Access-Control-Allow-Headers", "Content-Type,X-Requested-With,accept,Origin,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,Access-Control-Allow-Origin");
//        httpResponse.setHeader("Access-Control-Expose-Headers", "Access-Control-Allow-Credentials");
//        httpResponse.setHeader("Access-Control-Allow-Credentials", "true");
//        httpResponse.setHeader("Access-Control-Max-Age", "10");
//        System.out.println("---CORS Configuration Completed---");
//        chain.doFilter(request, response);
//    }
//    @Override
//    public void init(FilterConfig filterConfig) throws ServletException {
//    }
//    @Override
//    public void destroy() {
//    }
//}

public class CorsFilter implements Filter {
    private final List<String> allowedOrigins = Arrays.asList("http://localhost:4200");
    public CorsFilter() {
        // TODO Auto-generated constructor stub
    }

    /**
     * @see Filter#destroy()
     */
    public void destroy() {
        // TODO Auto-generated method stub
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;

        // Authorize (allow) all domains to consume the content
        // For websocket
//        String origin = request.getHeader("Origin");
//        ((HttpServletResponse) servletResponse).addHeader("Access-Control-Allow-Origin", allowedOrigins.contains(origin) ? origin : "");
//        ((HttpServletResponse) servletResponse).addHeader("Vary", "Origin");
        ((HttpServletResponse) servletResponse).addHeader("Access-Control-Allow-Origin", getOrigin());
        ((HttpServletResponse) servletResponse).addHeader("Access-Control-Allow-Credentials", "true");
        ((HttpServletResponse) servletResponse).addHeader("Access-Control-Allow-Methods","GET, OPTIONS, HEAD, PUT, POST, DELETE");
        ((HttpServletResponse) servletResponse).addHeader("Access-Control-Allow-Headers","Authorization");
        ((HttpServletResponse) servletResponse).addHeader("Access-Control-Allow-Headers","Content-Type");

        HttpServletResponse resp = (HttpServletResponse) servletResponse;

        // For HTTP OPTIONS verb/method reply with ACCEPTED status code -- per CORS handshake
        if (request.getMethod().equals("OPTIONS")) {
            resp.setStatus(HttpServletResponse.SC_ACCEPTED);
            return;
        }

        // pass the request along the filter chain
        chain.doFilter(request, servletResponse);
    }

    public void init(FilterConfig fConfig) throws ServletException {
        // TODO Auto-generated method stub
    }

    private String getOrigin(){
        String origin = "";
        String path = System.getenv("OSDIAL_API") + "/config/osdialapi.properties";
        Properties prop = new Properties();
        InputStream input = null;

        try {
            input = new FileInputStream(path);
            // load a properties file
            prop.load(input);

            // get the property value and print it out
            origin = prop.getProperty("origin.url");
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return origin;
    }

}