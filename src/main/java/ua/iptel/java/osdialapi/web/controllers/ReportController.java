package ua.iptel.java.osdialapi.web.controllers;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.iptel.java.osdialapi.model.CallLog;
import ua.iptel.java.osdialapi.model.Recording;
import ua.iptel.java.osdialapi.model.dto.CallLogExtended;
import ua.iptel.java.osdialapi.model.report.agent.AgentCallsDetailedReport;
import ua.iptel.java.osdialapi.model.report.agent.AgentCallsStatistic;
import ua.iptel.java.osdialapi.model.report.agent.AgentStatusesDetailedReport;
import ua.iptel.java.osdialapi.model.report.pause.AgentPausesReport;
import ua.iptel.java.osdialapi.model.report.pause.CampaignPausesReport;
import ua.iptel.java.osdialapi.model.report.pause.DetailedPausesReport;
import ua.iptel.java.osdialapi.model.report.pause.GeneralPausesReport;
import ua.iptel.java.osdialapi.service.ReportService;
import ua.iptel.java.osdialapi.util.TimeUtil;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("rest/admin/reports")
public class ReportController {

    @Autowired
    private ReportService service;

    @RequestMapping(value = "/pauses/general", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<GeneralPausesReport> getGeneralPausesReport(@RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime from,
                                                  @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime to,
                                                  @RequestParam("agent") String agent,
                                                  @RequestParam("campaigns") String campaigns) {

        List<GeneralPausesReport> result = service.getGeneralPausesReport(from, to, agent, campaigns);
        return result;
    }

    @RequestMapping(value = "/pauses/campaigns", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<CampaignPausesReport> getCampaignPauseReport(@RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime from,
                                                   @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime to,
                                                   @RequestParam("campaigns") String campaigns) {

        List<CampaignPausesReport> result = service.getCampaignPausesReport(from, to, campaigns);
        return result;
    }

    @RequestMapping(value = "/pauses/agents", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<AgentPausesReport> getAgentPauseReport(@RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime from,
                                                                 @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime to,
                                                                 @RequestParam("agent") String agent,
                                                                 @RequestParam("campaigns") String campaigns) {

        List<AgentPausesReport> result = service.getAgentPausesReport(from, to, agent, campaigns);
        return result;
    }

    @RequestMapping(value = "/pauses/detailed", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<DetailedPausesReport> getDetailedPauseReport(@RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime from,
                                                                   @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime to,
                                                                   @RequestParam("agent") String agent,
                                                                   @RequestParam("campaigns") String campaigns) {

        List<DetailedPausesReport> result = service.getDetailedPausesReport(from, to, agent, campaigns);
        return result;
    }

    @RequestMapping(value = "/agent/calls", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<AgentCallsDetailedReport> getAgentCallsDetailedPauseReport(@RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime from,
                                                                       @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime to,
                                                                       @RequestParam("agent") String agent,
                                                                       @RequestParam("campaigns") String campaigns,
                                                                       @RequestParam("userGroup") String userGroup) {

        List<AgentCallsDetailedReport> result = service.getAgentCallsDetailedReport(from, to, agent, campaigns, userGroup);
        return result;
    }

    @RequestMapping(value = "/agent/statuses", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<AgentStatusesDetailedReport> getAgentStatusesDetailedPauseReport(@RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime from,
                                                                                    @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime to,
                                                                                    @RequestParam("agent") String agent,
                                                                                    @RequestParam("campaigns") String campaigns,
                                                                                    @RequestParam("userGroup") String userGroup) {

        List<AgentStatusesDetailedReport> result = service.getAgentStatusesDetailedReport(from, to, agent, campaigns, userGroup);
        return result;
    }

    @RequestMapping(value = "/recordings", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Recording> getAllRecordings() {
        return service.getAllRecordings();
    }

    @RequestMapping(value = "/recordings/{leadId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Recording> getAllRecordingsByLeadId(@PathVariable("leadId") Long leadId) {
        return service.getRecordingByLeadId(leadId);
    }

    @RequestMapping(value = "/recordings/byDatetime", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Recording> getRecordingsByDateTime(@RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime from,
                                                         @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime to){
        List<Recording> result = service.getRecordingsByPeriod(from, to);
        return result;
    }

    @RequestMapping(value = "/recordings/byDatetimeAndUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Recording> getRecordingsByDateTimeAndUser(@RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime from,
                                                         @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime to,
                                                                @RequestParam("agent") String agent){
        List<Recording> result = service.getRecordingsByPeriodAndUser(from, to, agent);
        return result;
    }

    @RequestMapping(value = "/callLog", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<CallLog> getCallsLog() {
        return service.getCallsLog();
    }

    @RequestMapping(value = "/callLog/{leadId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<CallLog> getCallsLogByLeadId(@PathVariable("leadId") Long leadId) {
        return service.getCallsLogByLeadId(leadId);
    }

    @RequestMapping(value = "/bitrix/callLog", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<CallLogExtended> getCallsLogBitrix() {
        return service.getCallsLogWitchLeadName();
    }

    @RequestMapping(value = "/bitrix/callLog/{listId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<CallLogExtended> getCallsLogBitrixByLeadId(@PathVariable("listId") Long listId) {
        return service.getCallsLogByLeadIdWithLeadName(listId);
    }

    @RequestMapping(value = "/callLog/byCampaignIdAndDateTimeRange", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<CallLog>>
    getCallsByCampaignIdAndDateTimeRange(@RequestParam("from") String from,
                                         @RequestParam("to") String to,
                                         @RequestParam("campaignId") Integer campaingId) {
        try {
            LocalDateTime fromTime = TimeUtil.parseLocalDateTime(from);
            LocalDateTime toTime =  TimeUtil.parseLocalDateTime(to);
            List<CallLog> result = service.getCallLogByCampaignAndDateTimeRange(fromTime, toTime, campaingId);
            ResponseEntity<Collection<CallLog>> responseEntity = new ResponseEntity<>(result, HttpStatus.OK);
            return responseEntity;
        }catch (Exception ex){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/callLog/byListId/{listId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<CallLog>>
    getCallsByListId(@PathVariable("listId") Integer listId) {
        try{
            List<CallLog> result = service.getCallLogByListId(listId);
            ResponseEntity<Collection<CallLog>> responseEntity = new ResponseEntity<>(result, HttpStatus.OK);
            return responseEntity;
        }catch (Exception ex){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }



    @RequestMapping(value = "/recfile/{filename:.+}", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<byte[]> getRecord(@PathVariable("filename") String filename) throws IOException {
        // example filename: 1002_1009_20180909-190054_0487002006-all.mp3
        String[] splitRecordingFileName = filename.split("_");
        String dateSubstring = splitRecordingFileName[2];
        String recordingFolder = dateSubstring.substring(0, 4) + "-" + dateSubstring.substring(4, 6) + "-" + dateSubstring.substring(6, 8);
        String recordFileName = filename + "-all.mp3";
        String osdialApi = System.getenv("OSDIAL_API");
        File initialFile = new File(osdialApi + "/recordings/" + splitRecordingFileName[0] + "/" + recordingFolder + "/"  + recordFileName);
        InputStream in = FileUtils.openInputStream(initialFile);
        byte[] media = IOUtils.toByteArray(in);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentLength(media.length);
        headers.setContentDispositionFormData("attachment", recordFileName);
        ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(media, headers, HttpStatus.OK);
        return responseEntity;
    }

}
