package ua.iptel.java.osdialapi.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.iptel.java.osdialapi.model.UserGroup;
import ua.iptel.java.osdialapi.service.UserGroupService;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("rest/admin/userGroups")
public class UserGroupController {
    @Autowired
    private UserGroupService service;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<UserGroup> getAll() {
        List<UserGroup> result = service.getAll();
        return result;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserGroup> get(@PathVariable("id") Long id){
        UserGroup userGroup = service.getById(id);
        if(userGroup == null){
            return new ResponseEntity("No UserGroup found for ID " + id, HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity<UserGroup>(userGroup, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable("id") Long id) {
        if (0 == service.delete(id)) {
            return new ResponseEntity("No UserGroup found for ID " + id, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(id, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<UserGroup> create(@Valid @RequestBody UserGroup userGroup){
        UserGroup userGroupNew = service.create(userGroup);
        return new ResponseEntity<UserGroup>(userGroupNew, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.PUT)
    public ResponseEntity<UserGroup> update(@PathVariable("id") int id, @RequestBody UserGroup userGroup){
        UserGroup userGroupUpdated = service.update(userGroup);
        if (null == userGroupUpdated) {
            return new ResponseEntity("No UserGroup found for ID " + id, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<UserGroup>(userGroupUpdated, HttpStatus.OK);
    }
}
