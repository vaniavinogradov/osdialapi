package ua.iptel.java.osdialapi.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.iptel.java.osdialapi.dao.osdial.Bitrix24Dao;
import ua.iptel.java.osdialapi.model.DialList;
import ua.iptel.java.osdialapi.model.dto.Bitrix24Settings;

@RestController
@RequestMapping("rest/admin/bitrix24")
public class Bitrix24Controller {
    @Autowired
    private Bitrix24Dao bitrix24Dao;

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<Bitrix24Settings> update(@RequestBody Bitrix24Settings bitrix24Settings){
        if(bitrix24Settings != null && bitrix24Settings.getWebHookUrl() != ""){
            Bitrix24Settings updateSettings = bitrix24Dao.update(bitrix24Settings);
            if (null == updateSettings) {
                return new ResponseEntity("didnt update" , HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<Bitrix24Settings>(updateSettings, HttpStatus.OK);
        }else {
            return new ResponseEntity<Bitrix24Settings>(bitrix24Settings, HttpStatus.BAD_REQUEST);
        }

    }
}
